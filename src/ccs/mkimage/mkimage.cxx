/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <sys/types.h>
#include <sys/stat.h>

#include <stdint.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <fstream>
#include <getopt.h>
#include <langinfo.h>

#include <iostream>
#include <sstream>
#include <algorithm>

#include <boost/filesystem.hpp>
#include <boost/filesystem/convenience.hpp>

#include <libsherpa/UExcept.hxx>

#include "Version.hxx"
#include "AST.hxx"
#include "Environment.hxx"
#include "UocInfo.hxx"
#include "Options.hxx"

using namespace std;
using namespace boost;
using namespace sherpa;

bool Options::showLex = false;
bool Options::showParse = false;
bool Options::useStdInc = true;
bool Options::useStdLib = true;
bool Options::showPasses = false;
bool Options::showDiag = false;
bool Options::useFileNames = false;
bool Options::ppFQNS = false;
bool Options::emitDeps = false;
boost::filesystem::path Options::depFile;
boost::filesystem::path Options::outputFileName = "mkimage.out";
boost::filesystem::path Options::headerDir;
std::vector< boost::filesystem::path > Options::libPath;
std::string Options::targetArchName;
  
#define LOPT_SHOWLEX      257   /* Show tokens */
#define LOPT_SHOWPARSE    258   /* Show parse */
#define LOPT_SHOWPASSES   259   /* Show processing passes */
#define LOPT_DUMPAFTER    260   /* PP after this pass */
#define LOPT_NOSTDINC     261   /* Do not append std search paths */
#define LOPT_NOSTDLIB     262   /* Do not append std lib paths */
#define LOPT_PPFQNS       263   /* Show FQNs when pretty printing */
#define LOPT_MDEPEND      264   /* Write dependencies too */
#define LOPT_MDEPFILE     265   /* Output file name for dependencies */
#define LOPT_DIAG         266   /* Set opts.diag to true */

struct option longopts[] = {
  { "MD",                   0,  0, LOPT_MDEPEND },
  { "MF",                   1,  0, LOPT_MDEPFILE },
  { "diag",                 0,  0, LOPT_DIAG },
  { "dumpafter",            1,  0, LOPT_DUMPAFTER },
  { "headers",              1,  0, 'H' },
  { "help",                 0,  0, 'h' },
  { "nostdinc",             0,  0, LOPT_NOSTDINC },
  { "nostdlib",             0,  0, LOPT_NOSTDLIB },
  { "ppfqns",               0,  0, LOPT_PPFQNS },
  { "showlex",              0,  0, LOPT_SHOWLEX },
  { "showparse",            0,  0, LOPT_SHOWPARSE },
  { "showpasses",           0,  0, LOPT_SHOWPASSES },
  { "target",               1,  0, 't' },
  { "version",              0,  0, 'V' },
  {0,                       0,  0, 0}
};

/// @brief Search for an executable using the library path
boost::filesystem::path
Options::resolveLibraryPath(std::string exec)
{
  boost::filesystem::path ifPath(exec);

  if (ifPath.empty())
    return ifPath;

  // absolute paths don't get expanded.
  if (ifPath.is_complete())
    return ifPath;

  for (size_t i = 0; i < Options::libPath.size(); i++) {
    boost::filesystem::path testPath(Options::libPath[i]);
    testPath /= ifPath;
    
    if (boost::filesystem::exists(testPath) && boost::filesystem::is_regular(testPath))
      return testPath;
  }

  return boost::filesystem::path();
}

boost::filesystem::path 
FindCoyotosRoot()
{
  const char *root_dir = getenv("COYOTOS_ROOT");
  if (root_dir)
    return boost::filesystem::path(root_dir);

  // See if we can find it the hard way:
  boost::filesystem::path rootDir = boost::filesystem::current_path();

  bool found_src = false;

  while(!rootDir.empty()) {
    if (found_src && rootDir.leaf() == "coyotos")
      break;

    found_src = (rootDir.leaf() == "src");

    rootDir = rootDir.branch_path();
  }

  return rootDir;
}

void
emit_deps_to(const boost::filesystem::path& targetName, DepSet& deps, 
	     std::ostream& s)
{
  s << targetName
    << ':';
  for (DepSet::iterator itr = deps.begin();
       itr != deps.end(); ++itr)
    s << ' ' << (*itr);

  s << '\n';
}

bool 
emitUocDeps(boost::shared_ptr<UocInfo::UocListEntry> le,
	    std::ostream *pos)
{
  boost::shared_ptr<UocInfo> uoc = le->uoc;

  // Cannot use change_extension here, because uocName contains dots!
  std::string targetName = (uoc->uocName + ".h");

  boost::filesystem::path targetPath = 
    Options::headerDir / targetName;

  if (pos) {
    emit_deps_to(targetPath, uoc->uocDeps, *pos);
    return true;
  }

  /* If pos==0, you specified --MD but not --MT. In that case, we
     stick the output into

     $(headerDir)/.mumble.m,

     where .mumble.m is derived from the UOC name. 
     
     This is almost always the right default, and it eliminates a
     lot of redundant input in the makefiles. */

  if (pos == 0) {
    boost::filesystem::path depFile = 
      Options::headerDir / ("." + uoc->uocName + ".m");

    ofstream ofs(depFile.string().c_str(), 
		 ofstream::out | ofstream::trunc);
    if (ofs.fail()) {
      boost::filesystem::remove(depFile);
      std::cerr << "Could not write dependency file \""
		<< depFile << "\"\n";
      THROW(excpt::IoError, "Depend write failure");
    }

    emit_deps_to(targetPath, uoc->uocDeps, ofs);
  }

  return true;
}

void
help()
{
  std::cerr 
    << "Usage:" << endl
    << "  mkimage -t targetArch [-o outfile] [-I moddir] modname\n"
    << "      Interprets the module \"modname\"\n"
    << "  mkimage -t targetArch [-o outfile] [-I moddir] [-H hdrdir] modname\n"
    << "      Generates an export header file for enums in \"modname\"\n"
    << "  mkimage -V|--version\n"
    << "  mkimage -h|--help\n"
    << "If option -f is used, arguments are files rather than module names.\n"
    << "Debugging options:\n"
    << "  --showlex --showparse\n" 
    << flush;
}
 
void
fatal()
{
  cerr << "Confused due to previous errors, bailing."
       << endl;
  exit(1);
}


int
main(int argc, char *argv[]) 
{
  int c;
  extern int optind;
  int opterr = 0;

  while ((c = getopt_long(argc, argv, 
			  "o:H:VhI:L:t:fM:",
			  longopts, 0
		     )) != -1) {
    switch(c) {
    case 'V':
      cerr << "mkimage Version: " << MKIMAGE_VERSION << endl;
      exit(0);
      break;

    case LOPT_NOSTDINC:
      Options::useStdInc = false;
      break;

    case LOPT_NOSTDLIB:
      Options::useStdLib = false;
      break;

    case LOPT_SHOWPARSE:
      Options::showParse = true;
      break;

    case LOPT_SHOWLEX:
      Options::showLex = true;
      break;

    case LOPT_SHOWPASSES:
      Options::showPasses = true;
      break;

    case LOPT_DIAG:
      Options::showDiag = true;
      break;

    case LOPT_DUMPAFTER:
      {
	for (size_t i = (size_t)pn_none+1; i < (size_t) pn_npass; i++) {
	  if (strcmp(UocInfo::passInfo[i].name, optarg) == 0 ||
	      strcmp("ALL", optarg) == 0)
	    UocInfo::passInfo[i].printAfter = true;
	}

	break;
      }

    case LOPT_PPFQNS:
      Options::ppFQNS = true;
      break;

    case LOPT_MDEPEND:
      Options::emitDeps = true;
      break;

    case LOPT_MDEPFILE:
      Options::depFile = optarg;
      break;

    case 'h': 
      {
	help();
	exit(0);
      }

    case 'o':
      Options::outputFileName = optarg;
      break;

    case 'f':
      Options::useFileNames = true;
      break;

    case 'H':
      Options::headerDir = optarg;
      break;

    case 'I':
      UocInfo::searchPath.push_back(boost::filesystem::path(optarg));
      break;

    case 'L':
      Options::libPath.push_back(boost::filesystem::path(optarg));
      break;

    case 't':
      Options::targetArchName = optarg;
      break;

    default:
      opterr++;
      break;
    }
  }
  
  boost::filesystem::path root_dir = FindCoyotosRoot();

  if (!root_dir.empty()) {
    boost::filesystem::path incpath = root_dir / "/usr/include/mki";
    boost::filesystem::path libpath = root_dir / "/usr/domain";

    if (Options::useStdInc)
      UocInfo::searchPath.push_back(incpath);
    if (Options::useStdLib)
      Options::libPath.push_back(libpath);
  }

  const char *xenv_dir = getenv("COYOTOS_XENV");
  if (xenv_dir) {
    boost::filesystem::path incpath = 
      boost::filesystem::path(xenv_dir) / "/usr/include/mki";
    boost::filesystem::path libpath =
      boost::filesystem::path(xenv_dir) / "/usr/domain";

    if (Options::useStdInc)
      UocInfo::searchPath.push_back(incpath);
    if (Options::useStdLib)
      Options::libPath.push_back(libpath);
  }

  argc -= optind;
  argv += optind;
  
  if (argc == 0)
    opterr++;

  if (opterr) {
    std::cerr << "Usage: Try mkimage --help" << std::endl;
    exit(1);
  }

  try {
    boost::shared_ptr<CoyImage> ci(new CoyImage(Options::targetArchName));

    uint32_t compileVariant = 
      Options::headerDir.empty() ? CV_INTERP : CV_CONSTDEF;

    // Vector to use for dependency tracking:
    DepSet allDeps;

    // Compile everything on the command line:
    LexLoc cmdLoc("<command line>", 0, 0);
    for(int i = 0; i < argc; i++) {
      std::string fileName;
      std::string modName = argv[i];

      DepSet deps;

      if (Options::useFileNames) {
	fileName = argv[i];
	modName = 
	  UocInfo::extractModuleName(std::cerr, std::string(argv[i]));

	UocInfo::UocList::iterator it = UocInfo::uocList.find(modName);

	if (it != UocInfo::uocList.end()) {
	  std::cerr << "File \"" << fileName << "\" provides module "
		    << modName << ", which is already provided by \""
		    << it->second->fileName << "\"\n";
	  exit(1);
	}

	boost::shared_ptr<UocInfo::UocListEntry>
	  ule(new UocInfo::UocListEntry(fileName));
	UocInfo::uocList[modName] = ule;
      }

      boost::shared_ptr<UocInfo> uoc =
	 UocInfo::importModule(std::cerr, cmdLoc, ci, modName, 
			       deps,
			       compileVariant);
      uoc->isCmdLine = true;

      for (DepSet::iterator itr = deps.begin();
	   itr != deps.end(); ++itr) {
	if(!contains(allDeps, *itr))
	  allDeps.insert(*itr);
      }
    }

    if (Options::headerDir.empty()) {
      ci->ToFile(Options::outputFileName);

      if (Options::emitDeps) {
	if (!Options::depFile.empty()) {
	  ofstream ofs(Options::depFile.string().c_str(), 
		       ofstream::out | ofstream::trunc);
	  if (ofs.fail()) {
	    boost::filesystem::remove(Options::outputFileName);
	    std::cerr << "Could not write dependency file \""
		      << Options::depFile << "\"\n";
	    THROW(excpt::IoError, "Depend write failure");
	  }
	  emit_deps_to(Options::outputFileName, allDeps, ofs);
	}
	else
	  emit_deps_to(Options::outputFileName, allDeps, std::cerr);
      }
    }
    else if (Options::emitDeps) { /* ~headerDir.size() */
      /* This is a bit tricky. If you specify --MD but not --MT,
	 we pass NULL for the output stream pointer. The iterator
	 function will then stick the output into 
	    $(headerDir)/.mumble.m,
	 where .mumble.m is derived from the UOC name. 

	 This is almost always the right default, and it eliminates a
	 lot of redundant input in the makefiles. */

      if (!Options::depFile.empty()) {
	ofstream ofs(Options::depFile.string().c_str(), 
		     ofstream::out | ofstream::trunc);
	if (ofs.fail()) {
	  if (!Options::outputFileName.empty())
	    boost::filesystem::remove(Options::outputFileName);
	  std::cerr << "Could not write dependency file \""
		    << Options::depFile << "\"\n";
	  THROW(excpt::IoError, "Depend write failure");
	}

	for (UocInfo::UocList::iterator it = UocInfo::uocList.begin();
	     it != UocInfo::uocList.end();
	     it++) 
	  emitUocDeps(it->second, &ofs);
      }
      else {
	for (UocInfo::UocList::iterator it = UocInfo::uocList.begin();
	     it != UocInfo::uocList.end();
	     it++) 
	  emitUocDeps(it->second, 0);
      }
    }

  }
  catch ( const sherpa::UExceptionBase& ex ) {
    std::cerr << "Exception "
	      << ex.et->name
	      << " at "
	      << ex.file
	      << ":"
	      << ex.line
	      << ": "
	      << ex.msg
	      << std::endl;
    exit(1);
  }

  exit(0);
}
