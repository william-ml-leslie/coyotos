/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <string>

#include <libsherpa/utf8.hxx>

#include "Environment.hxx"
#include "Value.hxx"
#include "AST.hxx"

using namespace boost;
using namespace sherpa;

static boost::shared_ptr<Environment<Value> > endpointEnv;

// Set field values:
static void 
EndpointSet(PrimFieldValue& pfv,
	    const boost::shared_ptr<Value> & val)
{
  boost::shared_ptr<CapValue> thisv =  dynamic_pointer_cast<CapValue>(pfv.thisValue);
  boost::shared_ptr<CoyImage> ci = thisv->ci;

  boost::shared_ptr<CiEndpoint> ob = ci->GetEndpoint(thisv->cap);

  if (pfv.ident == "protPayload") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.protPayload = dynamic_pointer_cast<IntValue>(val)->bn.as_uint32();
    return;
  }

  if (pfv.ident == "endpointID") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.endpointID = dynamic_pointer_cast<IntValue>(val)->bn.as_uint64();
    return;
  }

  if (pfv.ident == "recipient") {
    if (val->kind != Value::vk_cap)
      throw excpt::BadValue;

    ob->v.recipient = dynamic_pointer_cast<CapValue>(val)->cap;
    return;
  }

  if (pfv.ident == "pm") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.pm = (dynamic_pointer_cast<IntValue>(val)->bn == 0) ? 0 : 1;
    return;
  }

  assert(false && "Missing or unknown field name");
}

// Get field values:
static boost::shared_ptr<Value>
EndpointGet(PrimFieldValue& pfv)
{
  // Only field is "cap", so no need to bother with dispatching:
  
  boost::shared_ptr<CapValue> thisv =  dynamic_pointer_cast<CapValue>(pfv.thisValue);
  boost::shared_ptr<CoyImage> ci = thisv->ci;

  boost::shared_ptr<CiEndpoint> ob = ci->GetEndpoint(thisv->cap);

  if (pfv.ident == "protPayload")
    return IntValue::make(ob->v.protPayload);

  if (pfv.ident == "endpointID")
    return IntValue::make(ob->v.endpointID);

  if (pfv.ident == "recipient")
    return CapValue::make(ci, ob->v.recipient);

  if (pfv.ident == "pm")
    return IntValue::make(ob->v.pm);

  assert(false && "Missing or unknown field name");
}

#if 0
// Endpoint methods:
static boost::shared_ptr<Value>
EndpointMethod(PrimMethodValue& pmv,
	       std::ostream&,
	       sherpa::shared_ptr<AST> callSite,
	       std::vector<sherpa::shared_ptr<Value> >& args)
{
  // Only field is "cap", so no need to bother with dispatching:
  
  boost::shared_ptr<CapValue> thisv =  pmv.dynamic_pointer_cast<CapValue>(thisValue);
  boost::shared_ptr<CoyImage> ci = thisv->ci;

  boost::shared_ptr<CiEndpoint> ob = ci->GetEndpoint(thisv->cap);

  if (pmv.nm == "setProcess") {
    boost::shared_ptr<Value> val = args[0];

    if (val->kind != Value::vk_cap)
      throw excpt::BadValue;

    ob->v.recipient = dynamic_pointer_cast<CapValue>(val)->cap;
    return &TheUnitValue;
  }

  if (pmv.nm == "setPayloadMatch") {
    boost::shared_ptr<Value> val = args[0];

    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.pm = (dynamic_pointer_cast<IntValue>(val)->bn == 0) ? 0 : 1;
    return &TheUnitValue;
  }

  if (pmv.nm == "setSqueak") {
    boost::shared_ptr<Value> val = args[0];

    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.sq = (dynamic_pointer_cast<IntValue>(val)->bn == 0) ? 0 : 1;
    return &TheUnitValue;
  }

  if (pmv.nm == "setEndpointID") {
    boost::shared_ptr<Value> val = args[0];

    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.endpointID = dynamic_pointer_cast<IntValue>(val)->bn.as_uint64();
    return &TheUnitValue;
  }

  if (pmv.nm == "makeSendCap") {
    boost::shared_ptr<CapValue> cv = CapValue::make(ci, thisv->cap);

    cv->cap.type = ct_EndpointSend;

    return cv;
  }

  assert(false && "Missing or unknown field name");
}
#endif

boost::shared_ptr<Environment<Value> >
CapValue::getEndpointEnvironment()
{
  if (! endpointEnv) {
    endpointEnv = Environment<Value>::make();

    endpointEnv->addBinding("protPayload", 
			    PrimFieldValue::make("protPayload", false,
					      EndpointSet, 
					      EndpointGet));
    endpointEnv->addBinding("endpointID", 
			   PrimFieldValue::make("endpointID", false,
					      EndpointSet, 
					      EndpointGet));

    endpointEnv->addBinding("recipient", 
			   PrimFieldValue::make("recipient", false,
					      EndpointSet, 
					      EndpointGet));

    endpointEnv->addBinding("pm", 
			   PrimFieldValue::make("pm", false,
					      EndpointSet, 
					      EndpointGet));

    endpointEnv->addBinding("sq", 
			   PrimFieldValue::make("sq", false,
					      EndpointSet, 
					      EndpointGet));

#if 0
    endpointEnv->addBinding("setProcess", 
			    PrimMethodValue::make("setProcess", 1, 1,
					       EndpointMethod));

    endpointEnv->addBinding("setPayloadMatch", 
			    PrimMethodValue::make("setPayloadMatch", 1, 1,
					       EndpointMethod));

    endpointEnv->addBinding("setSqueak", 
			    PrimMethodValue::make("setSqueak", 1, 1,
					       EndpointMethod));
    endpointEnv->addBinding("setEndpointID", 
			    PrimMethodValue::make("setEndpointID", 1, 1,
					       EndpointMethod));
    endpointEnv->addBinding("makeSendCap", 
			    PrimMethodValue::make("makeSendCap", 0, 0,
					       EndpointMethod));
#endif
  }

  return endpointEnv;
}
