/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <string>

#include <boost/shared_ptr.hpp>
#include <libsherpa/utf8.hxx>

#include "Environment.hxx"
#include "Value.hxx"
#include "AST.hxx"

using namespace boost;
using namespace sherpa;

static boost::shared_ptr<Environment<Value> > gptEnv;

// Set field values:
static void 
GptSet(PrimFieldValue& pfv,
       const boost::shared_ptr<Value> & val)
{
  boost::shared_ptr<CapValue> thisv =  dynamic_pointer_cast<CapValue>(pfv.thisValue);
  boost::shared_ptr<CoyImage> ci = thisv->ci;

  boost::shared_ptr<CiGPT> ob = ci->GetGPT(thisv->cap);

#if 0
  if (pfv.ident == "recipient") {
    if (val->kind != Value::vk_cap)
      throw excpt::BadValue;

    ob->v.state.recipient = dynamic_pointer_cast<CapValue>(val)->cap;
    return;
  }
#endif

  if (pfv.ident == "l2v") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.l2v = dynamic_pointer_cast<IntValue>(val)->bn.as_uint32();
    return;
  }

  if (pfv.ident == "bg") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.bg = (dynamic_pointer_cast<IntValue>(val)->bn == 0) ? 0 : 1;
    return;
  }

  if (pfv.ident == "ha") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.ha = (dynamic_pointer_cast<IntValue>(val)->bn == 0) ? 0 : 1;
    return;
  }

  if (pfv.ident == "cap") {
    if (pfv.ndx == ~0u || pfv.ndx >= NUM_GPT_SLOTS) {
      // Cannot set() an un-dereferenced vector or exceed the bound
      throw excpt::BadValue;
    }

    ob->v.cap[pfv.ndx] = dynamic_pointer_cast<CapValue>(val)->cap;
    return;
  }

  assert(false && "Missing or unknown field name");
}

// Get field values:
static boost::shared_ptr<Value>
GptGet(PrimFieldValue& pfv)
{
  // Only field is "cap", so no need to bother with dispatching:
  
  boost::shared_ptr<CapValue> thisv =  dynamic_pointer_cast<CapValue>(pfv.thisValue);
  boost::shared_ptr<CoyImage> ci = thisv->ci;

  boost::shared_ptr<CiGPT> ob = ci->GetGPT(thisv->cap);

#if 0
  if (pfv.ident == "recipient")
    return CapValue::make(ci, ob->v.recipient);
#endif

  if (pfv.ident == "l2v")
    return IntValue::make(ob->v.l2v);

  if (pfv.ident == "ha")
    return IntValue::make(ob->v.ha);

  if (pfv.ident == "bg")
    return IntValue::make(ob->v.bg);

  if (pfv.ident == "cap") {
    if (pfv.ndx == ~0u || pfv.ndx >= NUM_GPT_SLOTS) {
      // Cannot get() an un-dereferenced vector or exceed the bound
      throw excpt::BadValue;
    }

    return CapValue::make(ci, ob->v.cap[pfv.ndx]);
  }

  assert(false && "Missing or unknown field name");
}

boost::shared_ptr<Environment<Value> >
CapValue::getGptEnvironment()
{
  if (! gptEnv) {
    gptEnv = Environment<Value>::make();

#if 0
    gptEnv->addBinding("endpointID", 
			   PrimFieldValue::make("endpointID", false,
					      GptSet, 
					      GptGet));

    gptEnv->addBinding("recipient", 
			   PrimFieldValue::make("recipient", false,
					      GptSet, 
					      GptGet));
#endif

    gptEnv->addBinding("cap", 
			   PrimFieldValue::make("cap", false,
					      GptSet, 
					      GptGet,
					      true));

    gptEnv->addBinding("bg", 
			   PrimFieldValue::make("bg", false,
					      GptSet, 
					      GptGet));

    gptEnv->addBinding("ha", 
			   PrimFieldValue::make("ha", false,
					      GptSet, 
					      GptGet));
    gptEnv->addBinding("l2v", 
			   PrimFieldValue::make("l2v", false,
					      GptSet, 
					      GptGet));
  }

  return gptEnv;
}
