/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <string>

#include <boost/shared_ptr.hpp>
#include <libsherpa/utf8.hxx>

#include "Environment.hxx"
#include "Value.hxx"
#include "AST.hxx"

using namespace boost;
using namespace sherpa;

static boost::shared_ptr<Environment<Value> > processEnv;

// Set field values:
static void 
ProcessSet(PrimFieldValue& pfv,
	   const boost::shared_ptr<Value> & val)
{
  boost::shared_ptr<CapValue> thisv =  dynamic_pointer_cast<CapValue>(pfv.thisValue);
  boost::shared_ptr<CoyImage> ci = thisv->ci;

  boost::shared_ptr<CiProcess> ob = ci->GetProcess(thisv->cap);

  if (pfv.ident == "runState") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    uint32_t rs = ob->v.runState = dynamic_pointer_cast<IntValue>(val)->bn.as_uint32();

    switch(rs) {
    case PRS_RUNNING:
    case PRS_RECEIVING:
    case PRS_FAULTED:
      ob->v.runState = dynamic_pointer_cast<IntValue>(val)->bn.as_uint32();
      break;
    default:
      throw excpt::BoundsError;
    }
    return;
  }

  if (pfv.ident == "flags") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.flags = dynamic_pointer_cast<IntValue>(val)->bn.as_uint32();
    return;
  }

  if (pfv.ident == "notices") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.notices = dynamic_pointer_cast<IntValue>(val)->bn.as_uint32();
    return;
  }

  if (pfv.ident == "faultCode") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.faultCode = dynamic_pointer_cast<IntValue>(val)->bn.as_uint32();
    return;
  }

  if (pfv.ident == "faultInfo") {
    if (val->kind != Value::vk_int)
      throw excpt::BadValue;

    ob->v.faultInfo = dynamic_pointer_cast<IntValue>(val)->bn.as_uint64();
    return;
  }

  if (pfv.ident == "schedule") {
    if (val->kind != Value::vk_cap)
      throw excpt::BadValue;

    ob->v.schedule = dynamic_pointer_cast<CapValue>(val)->cap;
    return;
  }

  if (pfv.ident == "addrSpace") {
    if (val->kind != Value::vk_cap)
      throw excpt::BadValue;

    ob->v.addrSpace = dynamic_pointer_cast<CapValue>(val)->cap;
    return;
  }

  if (pfv.ident == "brand") {
    if (val->kind != Value::vk_cap)
      throw excpt::BadValue;

    ob->v.brand = dynamic_pointer_cast<CapValue>(val)->cap;
    return;
  }

  if (pfv.ident == "cohort") {
    if (val->kind != Value::vk_cap)
      throw excpt::BadValue;

    ob->v.cohort = dynamic_pointer_cast<CapValue>(val)->cap;
    return;
  }

  if (pfv.ident == "ioSpace") {
    if (val->kind != Value::vk_cap)
      throw excpt::BadValue;

    ob->v.ioSpace = dynamic_pointer_cast<CapValue>(val)->cap;
    return;
  }

  if (pfv.ident == "handler") {
    if (val->kind != Value::vk_cap)
      throw excpt::BadValue;

    ob->v.handler = dynamic_pointer_cast<CapValue>(val)->cap;
    return;
  }

  if (pfv.ident == "capReg") {
    if (pfv.ndx == ~0u || pfv.ndx >= NUM_CAP_REGS || pfv.ndx == 0) {
      // Cannot set() an un-dereferenced vector or exceed the bound.
      // capReg 0 must remain null.
      throw excpt::BadValue;
    }

    ob->v.capReg[pfv.ndx] = dynamic_pointer_cast<CapValue>(val)->cap;
    return;
  }

  THROW(excpt::BadValue, "Unknown field name");
}

// Get field values:
static boost::shared_ptr<Value>
ProcessGet(PrimFieldValue& pfv)
{
  // Only field is "cap", so no need to bother with dispatching:
  
  boost::shared_ptr<CapValue> thisv =  dynamic_pointer_cast<CapValue>(pfv.thisValue);
  boost::shared_ptr<CoyImage> ci = thisv->ci;

  boost::shared_ptr<CiProcess> ob = ci->GetProcess(thisv->cap);

  if (pfv.ident == "flags")
    return IntValue::make(ob->v.flags);
  if (pfv.ident == "runState")
    return IntValue::make(ob->v.runState);
  if (pfv.ident == "notices")
    return IntValue::make(ob->v.notices);
  if (pfv.ident == "faultCode")
    return IntValue::make(ob->v.faultCode);
  if (pfv.ident == "faultInfo")
    return IntValue::make(ob->v.faultInfo);

  if (pfv.ident == "schedule")
    return CapValue::make(ci, ob->v.schedule);
  if (pfv.ident == "addrSpace")
    return CapValue::make(ci, ob->v.addrSpace);
  if (pfv.ident == "brand")
    return CapValue::make(ci, ob->v.brand);
  if (pfv.ident == "cohort")
    return CapValue::make(ci, ob->v.cohort);
  if (pfv.ident == "ioSpace")
    return CapValue::make(ci, ob->v.ioSpace);
  if (pfv.ident == "handler")
    return CapValue::make(ci, ob->v.handler);

  if (pfv.ident == "capReg") {
    if (pfv.ndx == ~0u || pfv.ndx >= NUM_CAP_REGS) {
      // Cannot get() an un-dereferenced vector or exceed the bound
      throw excpt::BadValue;
    }

    return CapValue::make(ci, ob->v.capReg[pfv.ndx]);
  }

  THROW(excpt::BadValue, "Unknown field name");
}

boost::shared_ptr<Environment<Value> >
CapValue::getProcessEnvironment()
{
  if (!processEnv) {
    processEnv = Environment<Value>::make();

    processEnv->addBinding("runState", 
			   PrimFieldValue::make("runState", false,
					      ProcessSet, 
					      ProcessGet));

    processEnv->addBinding("flags", 
			   PrimFieldValue::make("flags", false,
					      ProcessSet, 
					      ProcessGet));

    processEnv->addBinding("faultCode", 
			   PrimFieldValue::make("faultCode", false,
					      ProcessSet, 
					      ProcessGet));

    processEnv->addBinding("faultInfo", 
			   PrimFieldValue::make("faultInfo", false,
					      ProcessSet, 
					      ProcessGet));
    processEnv->addBinding("schedule", 
			   PrimFieldValue::make("schedule", false,
					      ProcessSet, 
					      ProcessGet));
    processEnv->addBinding("addrSpace", 
			   PrimFieldValue::make("addrSpace", false,
					      ProcessSet, 
					      ProcessGet));
    processEnv->addBinding("brand", 
			   PrimFieldValue::make("brand", false,
					      ProcessSet, 
					      ProcessGet));
    processEnv->addBinding("cohort", 
			   PrimFieldValue::make("cohort", false,
					      ProcessSet, 
					      ProcessGet));
    processEnv->addBinding("ioSpace", 
			   PrimFieldValue::make("ioSpace", false,
					      ProcessSet, 
					      ProcessGet));
    processEnv->addBinding("handler", 
			   PrimFieldValue::make("handler", false,
					      ProcessSet, 
					      ProcessGet));
    processEnv->addBinding("upcb", 
			   PrimFieldValue::make("upcb", false,
					      ProcessSet, 
					      ProcessGet));
    processEnv->addBinding("capReg", 
			   PrimFieldValue::make("capReg", false,
					      ProcessSet, 
					      ProcessGet,
					      true));

  }

  return processEnv;
}
