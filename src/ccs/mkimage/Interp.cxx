/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <stddef.h>
#include <vector>
#include <string>
#include <iomanip>

#include <boost/shared_ptr.hpp>
#include <libsherpa/ByteString.hxx>
#include <libcoyimage/ExecImage.hxx>

#include "UocInfo.hxx"
#include "builtin.hxx"

// #define DEBUG

using namespace std;
using namespace boost;
using namespace sherpa;

struct FnReturn {
  boost::shared_ptr<Value> val;

  FnReturn(boost::shared_ptr<Value> _val)
  {
    val = _val;
  }

  virtual ~FnReturn();
};

FnReturn::~FnReturn()
{
}

static bool
isValue(boost::shared_ptr<Value> v)
{
  if (v->kind == Value::vk_unit)
    return false;
  else if (v->kind == Value::vk_primField) {
    boost::shared_ptr<PrimFieldValue> pfv =
      dynamic_pointer_cast<PrimFieldValue>(v);
    if (pfv->isVector && pfv->ndx == ~0u)
      return false;
  }

  return true;
}

static bool
canDereference(boost::shared_ptr<Value> v)
{
  if (v->kind == Value::vk_primField) {
    boost::shared_ptr<PrimFieldValue> pfv =
      dynamic_pointer_cast<PrimFieldValue>(v);

    if (!pfv->isVector)
      return false;

    if (pfv->ndx != ~0u)		// already dereferenced
      return false;

    return true;
  }

  if (v->kind == Value::vk_cap) {
    boost::shared_ptr<CapValue> cv = dynamic_pointer_cast<CapValue>(v);
    if (cv->cap.type == ct_CapPage || cv->cap.type == ct_Page)
      return true;
  }

  return false;
}

bool
InterpState::value_is_true_expr(boost::shared_ptr<Value> vcond)
{
  if (vcond->kind == Value::vk_int) {
    boost::shared_ptr<IntValue> a = dynamic_pointer_cast<IntValue>(vcond);
    return (a->bn != 0);
  }
  if (vcond->kind == Value::vk_bool) {
    boost::shared_ptr<BoolValue> a = dynamic_pointer_cast<BoolValue>(vcond);
    return (a->b);
  }
  else if (vcond->kind == Value::vk_float) {
    boost::shared_ptr<FloatValue> a = dynamic_pointer_cast<FloatValue>(vcond);
    return (a->d != 0.0);
  }
  else if (vcond->kind == Value::vk_string) {
    return true;
  }
  else {
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

bool 
InterpState::conditional_expr_is_true(boost::shared_ptr<AST> ast)
{
  boost::shared_ptr<Value> vcond = ast->interp(*this)->get();

  try {
    return value_is_true_expr(vcond);
  }
  catch (...) {
    errStream << ast->loc << " "
	      << "Inappropriate dynamic type for boolean-valued expression\n";
    backtrace();
    throw;
  }
}

boost::shared_ptr<Value>
AST::interp(InterpState& incomingIS)
{
  InterpState is(incomingIS);
  is.curAST = shared_from_this();

  switch(astType) {
  case at_intLiteral:
  case at_stringLiteral:
    return litValue;

  case at_uoc:
    {
      boost::shared_ptr<Value> v = Value::Unit;

      // We may be executing in pure mode. If so, the only children we
      // should process are imports and enum ASTs. The rest should be
      // skipped. In future we may add puredef, so use an accessor on
      // the AST to determine what is pure.
      if (is.pureMode) {
	for(size_t c = 1; c < children.size(); c++)
	  if (child(c)->isPureAST())
	    v = child(c)->interp(is);
      }
      else {
	for(size_t c = 1; c < children.size(); c++)
	    v = child(c)->interp(is);
      }
      return v;
    }

  case at_s_import:
    {
      std::string ident = child(0)->s;
      std::string modName = child(1)->s;

      boost::shared_ptr<Binding<Value> > b = is.env->getLocalBinding(ident);
      if (b) {
	is.errStream << loc << " "
		  << "Symbol \""
		  << ident << "\" is already bound.\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      // It seems regrettable that this step abandons the interpreter
      // stack, because we cannot, in consequence, print the chain of
      // imports when it turns out to be circular. In the main, we
      // abandon the interpreter stack because to do otherwise we
      // would need to invade the UocInfo::compile() logic rather
      // deeply.
      //
      // For the moment, I don't think that it is worthwhile to do
      // that. It is easier to maintain the separate stack of
      // import records for that purpose if we decided that we need
      // to, and that is what I have actually done.
      boost::shared_ptr<UocInfo> uoc =
	UocInfo::importModule(is.errStream, loc, is.ci, modName, 
			      is.deps,
			      is.pureMode ? CV_CONSTDEF : CV_INTERP);

      for (DepSet::iterator itr = uoc->uocDeps.begin();
	   itr != uoc->uocDeps.end(); ++itr) {
	if (!contains(is.curUOC->uocDeps, *itr))
	  is.curUOC->uocDeps.insert(*itr);
      }

      is.env->addBinding(ident, EnvValue::make(uoc->getExportedEnvironment()));

      return Value::Unit;
    }

  case at_ifelse:
    {
      if (is.conditional_expr_is_true(child(0)))
	return child(1)->interp(is);

      // Else process 'else' clause if present:
      if (children.size() == 3)
	return child(2)->interp(is);

      // Else return unit:
      return Value::Unit;
    }

  case at_s_do:
    {
      boost::shared_ptr<Value> val = Value::Unit;
      do {
	val = child(0)->interp(is);
      } while (is.conditional_expr_is_true(child(1)));

      return val;
    }

  case at_s_while:
    {
      boost::shared_ptr<Value> val = Value::Unit;
      while (is.conditional_expr_is_true(child(0)))
	val = child(1)->interp(is);

      return val;
    }

  case at_fncall:
    {
      is.fnSym = child(0)->interp(is)->get();

      /* We may be in pure mode if we are on the RHS of an enum
	 definition. In that case, only pure vk_primfn's are currently
	 permitted. */
      if (is.fnSym->kind == Value::vk_function) {
	boost::shared_ptr<FnValue> fnv = dynamic_pointer_cast<FnValue>(is.fnSym);

	boost::shared_ptr<AST> args = fnv->args;
	boost::shared_ptr<AST> body = fnv->body;

	if (args->children.size() + 1 != children.size()) {
	  is.errStream << loc << " "
		    << "Wrong number of arguments to \""
		    << fnv->nm << "\"\n";
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}

	boost::shared_ptr<Environment<Value> > closedEnv = fnv->closedEnv;
	boost::shared_ptr<Environment<Value> > argEnv =
	  Environment<Value>::make(closedEnv);
	boost::shared_ptr<Environment<Value> > bodyEnv =
	  Environment<Value>::make(argEnv);

	for (size_t c = 0; c < args->children.size(); c++) {
	  std::string argName = args->child(c)->s;
	  boost::shared_ptr<Value> v = child(c+1)->interp(is);
	  boost::shared_ptr<Value> vGet = v->get();
	  is.actuals.push_back(vGet);
	  argEnv->addBinding(argName, vGet);
	}
      
	boost::shared_ptr<Value> result;

	try {
	  InterpState bodyState(is);
	  bodyState.env = bodyEnv;
	  result = body->interp(bodyState);
	}
	catch (FnReturn &ir) {
	  result = ir.val;
	}
	catch (...) {
	  throw;
	}

	return result->get();
      }
      else if (is.fnSym->kind == Value::vk_primfn) {
	boost::shared_ptr<PrimFnValue> fnv =
	  dynamic_pointer_cast<PrimFnValue>(is.fnSym);

	size_t nArgs = children.size() - 1;
	if (nArgs < fnv->minArgs) {
	  is.errStream << loc << " "
		    << "Not enough arguments to \""
		    << fnv->nm << "\"\n";
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}
	if (fnv->maxArgs && nArgs > fnv->maxArgs) {
	  is.errStream << loc << " "
		    << "Too many arguments to \""
		    << fnv->nm << "\"\n";
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}

	for (size_t c = 1; c < children.size(); c++) {
	  boost::shared_ptr<Value> v = child(c)->interp(is);
	  is.actuals.push_back(v->get());
	}
      
	boost::shared_ptr<Value> result = fnv->fn(*fnv, is, is.actuals);
	return result->get();
      }
      else if (is.fnSym->kind == Value::vk_primMethod) {
	boost::shared_ptr<PrimMethodValue> pmv =
	  dynamic_pointer_cast<PrimMethodValue>(is.fnSym);

	if (is.pureMode) {
	  is.errStream << loc << " " 
		       << "Illegal call to non-pure method \""
		       << pmv->nm << "\" on RHS of enum definition.\n";
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}

	size_t nArgs = children.size() - 1;
	if (nArgs < pmv->minArgs) {
	  is.errStream << loc << " "
		    << "Not enough arguments to \""
		    << pmv->nm << "\"\n";
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}
	if (pmv->maxArgs && nArgs > pmv->maxArgs) {
	  is.errStream << loc << " "
		    << "Too many arguments to \""
		    << pmv->nm << "\"\n";
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}

	for (size_t c = 1; c < children.size(); c++) {
	  boost::shared_ptr<Value> v = child(c)->interp(is);
	  is.actuals.push_back(v->get());
	}
      
	boost::shared_ptr<Value> result = pmv->fn(*pmv, is, is.actuals);
	return result->get();
      }
      else {
	is.errStream << loc << " "
		  << "Call using non-function value.\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }
    }

#if 0
  case at_s_progdef:
    {
      // A program definition defines a new first-class environment

      // No need to check types on the first two, because parser
      // ensures they are right:
      std::string ident = child(0)->s;
      std::string exeName = child(1)->s;

      boost::shared_ptr<Binding<Value> > b = is.env->getLocalBinding(ident);
      if (b) {
	is.errStream << loc << " "
		  << "Identifier \""
		  << ident << "\" is already bound.\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      ExecImage ei(exeName);

      boost::shared_ptr<Environment<Value> > progEnv = new Environment<Value>();
      boost::shared_ptr<Environment<Value> > toolNdxEnv = new Environment<Value>();
      boost::shared_ptr<CapValue> toolPageCap =
	new CapValue(is.ci, is.ci->AllocCapPage(is.curBank));
      boost::shared_ptr<CiCapPage> toolPage = is.ci->GetCapPage(toolPageCap->cap);

      boost::shared_ptr<EnvValue> progEnvValue = new EnvValue(progEnv);

      is.env->addBinding(ident, progEnvValue);
      progEnv->addBinding("pc", new IntValue(ei.entryPoint));
      progEnv->addBinding("tool", new EnvValue(toolNdxEnv));
      progEnv->addBinding("toolPage", toolPageCap);

      progEnv->setFlags("pc", BF_CONSTANT);
      progEnv->setFlags("tool", BF_CONSTANT);
      progEnv->setFlags("toolPage", BF_CONSTANT);

      capability addrSpace = is.ci->CiCap();

      for (size_t i = 0; i < ei.region.size(); i++) {
	ExecRegion& er = ei.region[i];

	for (uint64_t pos = 0; pos < er.filesz; pos += is.ci->target.pageSize) {
	  size_t nBytes = min(er.filesz - pos, is.ci->target.pageSize);

	  capability pgCap = is.ci->AllocPage(is.curBank);
	  boost::shared_ptr<CiPage> pg = is.ci->GetPage(pgCap);
	  memcpy(pg->data, ei.image.substr(er.offset + pos, nBytes).c_str(), nBytes);

	  if ((er.perm & ER_X) == 0)
	    pgCap.nx = 1;
	  if ((er.perm & ER_W) == 0)
	    pgCap.ro = 1;

#ifdef DEBUG
	  std::ios_base::fmtflags flgs = std::cerr.flags();

	  std::cerr << "Calling addSubspace at " 
		    << std::hex
		    << std::showbase
		    << er.vaddr + pos
		    << std::dec
		    << std::endl;

	  std::cerr.flags(flgs);

	  std::cerr << "  Space    = " << addrSpace << endl;
	  std::cerr << "  SubSpace = " << pgCap << endl;
#endif

	  addrSpace = 
	    is.ci->AddSubSpaceToSpace(bank, addrSpace, er.vaddr + pos, pgCap);
#ifdef DEBUG
	  std::cerr << "  Result   = " << addrSpace << endl;
#endif
	}
      }

      progEnv->addBinding("space", new CapValue(is.ci, addrSpace));
      progEnv->setFlags("space", BF_CONSTANT);


      for (size_t ndx = 2; ndx < children.size(); ndx++) {
	boost::shared_ptr<AST> toolAST = child(ndx);
	
	std::string toolIdent = toolAST->child(0)->s;
	boost::shared_ptr<Value> toolNdx = toolAST->child(1)->interp(is)->get();
	boost::shared_ptr<Value> toolVal = toolAST->child(2)->interp(is)->get();

	if (toolNdx->kind != Value::vk_int) {
	  is.errStream << toolAST->child(1)->loc << " " << 
	    "Tool indices must be integers." << '\n';
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}
	if (toolVal->kind != Value::vk_cap) {
	  is.errStream << toolAST->child(2)->loc << " " << 
	    "Tool values must be capabilities." << '\n';
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}

	size_t nSlots = toolPage->pgSize/sizeof(capability);

	if (dynamic_pointer_cast<IntValue>(toolNdx)()->bn >= nSlots) {
	  is.errStream << toolAST->child(1)->loc << " " << 
	    "Tool index must be less than number of CapPage slots ("
		       << toolPage->pgSize
		       << ")\n";
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}

	size_t capPgNdx = dynamic_pointer_cast<IntValue>(toolNdx)()->bn.as_uint32();

	toolNdxEnv->addBinding(toolIdent, toolNdx);
	toolNdxEnv->setFlags(toolIdent, BF_CONSTANT);
	toolPage->data[capPgNdx] = dynamic_pointer_cast<CapValue>(toolVal)()->cap;
      }

      return progEnvValue;
    }
#endif

  case at_s_assign:
    {
      boost::shared_ptr<Value> vdest = child(0)->interp(is);
      boost::shared_ptr<Value> v = child(1)->interp(is)->get();

      if (!isValue(v)) {
	is.errStream << loc << " " << 
	  "Right hand side of assignment is not a value." << '\n';
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
      }

      try {
	vdest->set(v->get());
      }
#if 1
      catch (const UException<&excpt::BoundsError>& bv) {
	is.errStream << loc << " "
		     << "Illegal assignment (array bounds violation)\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }
      catch (const UException<&excpt::BadValue>& bv) {
	is.errStream << loc << " "
		     << "Illegal assignment (bad RHS type)\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }
      catch (const UException<&excpt::NoAccess>& bv) {
	is.errStream << loc << " "
		     << "Illegal assignment (LHS is constant)\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }
#endif
      catch (...) {
	is.errStream << loc << " "
		  << "Unknown error\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      return Value::Unit;
    }

  case at_s_def:
    {
      boost::shared_ptr<Value> v = child(1)->interp(is);
      std::string ident = child(0)->s;

      if (!isValue(v)) {
	is.errStream << loc << " " << 
	  "Right hand side of assignment is not a value." << '\n';
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      boost::shared_ptr<Binding<Value> > b = is.env->getLocalBinding(ident);
      if (b) {
	is.errStream << loc << " "
		  << "Variable \""
		  << ident << "\" is already bound.\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      is.env->addBinding(ident, v->get());
      if (flags & af_export)
	is.env->setFlags(ident, BF_EXPORTED);
      
      return Value::Unit;
    }

  case at_s_enum:
  case at_s_capreg:
    {
      BigNum curValue = 0;

      boost::shared_ptr<Environment<Value> > env;
      if (child(0)->astType == at_ident) {
	env = Environment<Value>::make();
	std::string ident = child(0)->s;

	boost::shared_ptr<Binding<Value> > b = is.env->getLocalBinding(ident);
	if (b) {
	  is.errStream << loc << " "
		       << "Symbol \""
		       << ident << "\" is already bound.\n";
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}

	is.env->addBinding(ident, EnvValue::make(env));
	if (flags & af_export)
	  is.env->setFlags(ident, BF_EXPORTED);
      }
      else
	env = is.env;

      for (size_t i = 1; i < children.size(); i++) {
	boost::shared_ptr<AST> eBinding = child(i);

	std::string ident = eBinding->child(0)->s;

	boost::shared_ptr<Binding<Value> > b = env->getLocalBinding(ident);
	if (b) {
	  is.errStream << loc << " "
		       << "Identifier \""
		       << ident << "\" is already bound.\n";
	  is.backtrace();
	  THROW(excpt::BadValue, "Bad interpreter result");
	}

	if (eBinding->children.size() == 2) {
	  InterpState exprInterpState = is;
	  exprInterpState.pureMode = true;
	  boost::shared_ptr<Value> v = eBinding->child(1)->interp(exprInterpState)->get();
	  if (v->kind != Value::vk_int) {
	    is.errStream << eBinding->child(1)->loc << " "
			 << "Enum/capreg value must be an integer.\n";
	    is.backtrace();
	    THROW(excpt::BadValue, "Bad interpreter result");
	  }

	  curValue = dynamic_pointer_cast<IntValue>(v)->bn;
	}

	env->addBinding(ident, IntValue::make(curValue));
	env->setFlags(ident, BF_PURE);
	if (flags & af_export)
	  env->setFlags(ident, BF_EXPORTED);

	curValue = curValue + 1;
      }

      return Value::Unit;
    }

#if 0
  case at_s_constitdef:
    {
      std::string ident = child(0)->s;

      boost::shared_ptr<Binding<Value> > b = env->getLocalBinding(ident);
      if (b) {
	is.errStream << loc << " "
		  << "Identifier \""
		  << ident << "\" is already bound.\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      boost::shared_ptr<Environment<Value> > newEnv = new Environment<Value>(env);
      boost::shared_ptr<AST> ids = child(1);

      for (size_t i = 0; i < ids->children.size(); i++) {
	std::string ident = ids->child(i)->s;
	newEnv->addBinding(ident, new IntValue(i));
	newEnv->setFlags(ident, BF_CONSTANT);
      }

      env->addBinding(ident, new EnvValue(newEnv));

      return Value::Unit;
    }
#endif

  case at_s_fndef:
    {
      // All we do here is enter it into the environment as a value,
      // much as if we were processing s_bind

      // Check for a competing binding:
      std::string ident = child(0)->s;
      boost::shared_ptr<AST> args = child(1);
      boost::shared_ptr<AST> body = child(2);

      boost::shared_ptr<Binding<Value> > b = is.env->getLocalBinding(ident);
      if (b) {
	is.errStream << loc << " "
		  << "Variable \""
		  << ident << "\" is already bound.\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      is.env->addBinding(ident, FnValue::make(ident, is.env, args, body));
      if (flags & af_export)
	is.env->setFlags(ident, BF_EXPORTED);

      return Value::Unit;
    }

  case at_ident:
    {
      boost::shared_ptr<Binding<Value> > b = is.env->getBinding(s);
      if (!b) {
	is.errStream << loc << " "
		  << "Reference to unbound variable \""
		  << s << "\"\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      if (is.pureMode && !(b->flags & BF_PURE)) {
	is.errStream << loc << " "
		     << "Reference to impure binding \""
		     << s << "\" in ENUM definition\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      return BoundValue::make(b);
    }

  case at_dot:
    {
      /* If this appears in context of an ENUM RHS, then we are in
	 pure mode. Given lhs.rhs, the pure mode identifier restriction
	 applies only to the 'rhs'. The lhs is processed without
	 regard to pure mode. */
      InterpState lhsInterpState = is;
      lhsInterpState.pureMode = false;

      // This case is going to end up being quite a nuisance in due
      // course, but for now.
      boost::shared_ptr<Value> vl = child(0)->interp(lhsInterpState)->get();

      if (child(1)->astType != at_ident) {
	is.errStream << loc << " "
		  << "Right-hand side of '.' must be an identifier.\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      boost::shared_ptr<Environment<Value> > env = vl->getEnvironment();

      if (!env) {
	is.errStream << loc << " "
		  << "Cannot handle '.' on values without environments.\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      boost::shared_ptr<Binding<Value> > binding = env->getBinding(child(1)->s);

      if (!binding) {
	is.errStream << loc << " "
		  << "Reference to unbound field/member \""
		     << child(1)->s << "\"\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      if (is.pureMode && !(binding->flags & BF_PURE)) {
	is.errStream << loc << " "
		     << "Reference to impure binding \""
		     << child(1)->s << "\" in ENUM definition\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      boost::shared_ptr<Value> val = binding->val;

      if (val->isOpen)		// container was not an environment
	return dynamic_pointer_cast<OpenValue>(val)->closeWith(vl->get());

      return BoundValue::make(binding);

    }

  case at_vecref:
    {
      // There are actually two distinct cases here: dereferencing an
      // actual vector or vector-like LHS vs. dereferencing a field
      // that merely *behaves* like a vector. The latter case arises
      // in things like procCap.capReg[5]. That is the vk_primField
      // case.

      boost::shared_ptr<Value> v_vec = child(0)->interp(is);
      boost::shared_ptr<Value> v_ndx = child(1)->interp(is)->get();

      // This code should not currently be used. The idea is that if
      // we see someGpt.cap[5][6], then someGpt.cap[5] is probably a
      // capPage. In any case we need to treat the someGpt.cap[5] as
      // an r-value and then deal with the dereference of that.
      if (v_vec->kind == Value::vk_primField) {
	boost::shared_ptr<PrimFieldValue> pfv =
	  dynamic_pointer_cast<PrimFieldValue>(v_vec);
	if (pfv->ndx != ~0u)
	  v_vec = pfv->get();
      }
      else if (v_vec->kind == Value::vk_boundValue)
	v_vec = v_vec->get();

      if (!canDereference(v_vec)) {
	is.errStream << loc << " "
		  << "LHS of index operation must be dereferenceable.\n";
	is.backtrace();
	THROW(excpt::BadValue, "Bad interpreter result");
      }

      size_t ndx = dynamic_pointer_cast<IntValue>(v_ndx)->bn.as_uint32();

      if (v_vec->kind == Value::vk_primField) {
	boost::shared_ptr<PrimFieldValue> pfv =
	  dynamic_pointer_cast<PrimFieldValue>(v_vec);

	pfv->ndx = ndx;
	pfv->cacheTheValue();	// it's now a value r-value
	return pfv;
      }
      else if (v_vec->kind == Value::vk_cap)
	return dynamic_pointer_cast<CapValue>(v_vec)->derefAsVector(ndx);

      is.errStream << loc << " "
		<< "mkimage bug: Inappropriate type on LHS "
		<< "of index operation.\n";
      is.backtrace();
      THROW(excpt::BadValue, "Bad interpreter result");
    }

  case at_s_print:
    {

      boost::shared_ptr<Value> v0 =
	child(0)->interp(is)->get();
      vector<boost::shared_ptr<Value> > args;
      args.push_back(v0);

      pf_print_one(is, v0);
      std::cout << '\n';

      return Value::Unit;
    }

  case at_s_printstar:
    {
      boost::shared_ptr<Value> v0 =
	child(0)->interp(is)->get();
      vector<boost::shared_ptr<Value> > args;
      args.push_back(v0);

      return pf_doprint_star(is, args);
    }

  case at_s_return:
    {
      boost::shared_ptr<Value> v0 = child(0)->interp(is);

      throw FnReturn(v0);
    }

  case at_block:
    {
      InterpState blockState(is);
      blockState.env = Environment<Value>::make(is.env);
      boost::shared_ptr<Value> v = Value::Unit;

      for (size_t c = 0; c < children.size(); c++)
	v = child(c)->interp(blockState);

      return v->get();
    }

  default:
    is.errStream << loc << " "
	      << "Do not know how to interpret AST nodes of type \""
	      << nodeName()
	      << "\"\n";
    THROW(excpt::BadValue, "Bad interpreter result");
  }
}

bool
UocInfo::pass_interp(std::ostream& errStream, DepSet& deps, 
		     unsigned long flags)
{
  boost::shared_ptr<AST> modName = ast->child(0);
  if (uocName != "coyotos.TargetInfo" && modName->s != uocName) {
    errStream << modName->loc << " "
	      << "Defined module name \"" << modName->s 
	      << "\" does not match file module name \"" << uocName << "\"."
	      << "\n";
    return false;
  }

  env = Environment<Value>::make(getBuiltinEnv(ci));

  InterpState is(errStream, ci, 
		 shared_from_this(), ast, deps, env);

  try {
    ast->interp(is);
  } catch (...) {
    // Diagnostic has already been issued.
    return false;
  }

  return true;
}

InterpState::InterpState(std::ostream & _errStream, 
			 boost::shared_ptr<CoyImage> _ci,
			 boost::shared_ptr<UocInfo> _curUOC,
			 boost::shared_ptr<AST> _curAST,
			 DepSet& _deps,
			 boost::shared_ptr<Environment<Value> > _env
			 )
  : errStream(_errStream), deps(_deps)
{
  ci = _ci;
  curUOC = _curUOC;
  curAST = _curAST;
  env = _env;
  pureMode = false;
  parent = 0;

  // fnSym inits to default null
  // actuals inits to default null
}

InterpState::InterpState(const InterpState& is)
  : errStream(is.errStream), deps(is.deps)
{
  ci = is.ci;
  curUOC = is.curUOC;
  curAST = is.curAST;
  env = is.env;
  pureMode = is.pureMode;
  parent = &is;

  // fnSym inits to default null
  // actuals inits to default null
}

void
InterpState::backtrace() const
{
#if 0
  errStream << "  Emit backtrace here.\n";
#endif

  for (const InterpState *pIS = this; pIS; pIS = pIS->parent) {
#if 0
    errStream << (void *) pIS << " " << pIS->curAST->astTypeName() << "\n";
#endif

    if (pIS->curAST->astType != at_fncall)
      continue;

    if (!pIS->fnSym)
      continue;

    boost::shared_ptr<Value> fn = pIS->fnSym;

    if (fn->kind == Value::vk_function) {
      boost::shared_ptr<FnValue> fnv = dynamic_pointer_cast<FnValue>(fn);

      errStream << "  " << fnv->nm << "(";
#if 0
      boost::shared_ptr<AST> args = fnv->args;
      for (size_t c = 0; c < args->children.size(); c++) {
	if (c > 0)
	  errStream << ", ";
	errStream << args->child(c)->s;
	if (NULL != pIS->actuals)
	  errStream << "=" << pIS->actuals.at(c);
	else
	  errStream << "=???";
      }
#endif
    }
    else if (fn->kind == Value::vk_primfn) {
      boost::shared_ptr<PrimFnValue> fnv = dynamic_pointer_cast<PrimFnValue>(fn);

      errStream << "  " << fnv->nm << "(";
    }
    else if (fn->kind == Value::vk_primMethod) {
      boost::shared_ptr<PrimMethodValue> pmv =
	dynamic_pointer_cast<PrimMethodValue>(fn);

      errStream << "  " << pmv->nm << "(";
    }

    if (pIS->actuals.size()) {
      for (size_t c = 0; c < pIS->actuals.size(); c++) {
	if (c > 0)
	  errStream << ", ";
	errStream << pIS->actuals[c];
      }
    }
    else
      errStream << "...";

    errStream << ") at " << pIS->curAST->loc << "\n";
  }
}
