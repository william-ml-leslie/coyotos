#ifndef DOMNODE_HXX
#define DOMNODE_HXX

/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <vector>
#include <boost/shared_ptr.hpp>
#include <libsherpa/LexLoc.hxx>

struct DomNode;
typedef std::vector<boost::shared_ptr<DomNode> > DomVec;

struct DomNode {
  sherpa::LexLoc loc;
  std::string  name;

  enum {
    ntNone,
    ntRoot,
    ntElem,
    ntAttr,
    ntText,
    ntWhiteSpace
  } nodeType;

  struct HtmlElementInfo {
    std::string wrap;
    std::string wclass;
    std::string prefix;
    std::string name;
  };

  DomVec attrs;
  DomVec children;

  DomNode()
  {
    name = "<?>";
    loc = sherpa::LexLoc();
  }

  virtual ~DomNode();

  void dump(std::ostream&, int indent) const;

  // Following is implemented in DocComment.cxx, so that it will stay
  // in sync with the ElementInfo vector there.
  HtmlElementInfo htmlElementInfo() const;
  void emitHtml(std::ostream& out) const;
  void emitXML(std::ostream& out) const;

  inline void addChild(const boost::shared_ptr<DomNode>& child)
  {
    children.push_back(child);
  }

  inline void addAttr(const boost::shared_ptr<DomNode>& attr)
  {
    attrs.push_back(attr);
  }

  bool isEmpty() {
    return children.size() == 0;
  }
};

struct RootDomNode : DomNode {
  RootDomNode()
  {
    name = "<root>";
    nodeType = ntRoot;
  }

  static inline boost::shared_ptr<RootDomNode>
  make()
  {
    return boost::shared_ptr<RootDomNode>(new RootDomNode);
  }

  ~RootDomNode();
};

struct ElemDomNode : DomNode {
  ElemDomNode(const sherpa::LexLoc& where, const std::string& s)
  {
    name = s;
    loc = where;
    nodeType = ntElem;
  }

  static inline boost::shared_ptr<ElemDomNode>
  make(const sherpa::LexLoc& where, const std::string& s) {
    ElemDomNode *tmp = new ElemDomNode(where, s);
    return boost::shared_ptr<ElemDomNode>(tmp);
  }
  ~ElemDomNode();
};

struct AttrDomNode : DomNode {
  std::string  value;
  bool squote;

  AttrDomNode(const sherpa::LexLoc& where, const std::string& nm, 
	      const std::string& v, bool sq = false)
  {
    name = nm;
    loc = where;
    value = v;
    nodeType = ntAttr;
    squote = sq;
  }

  static inline boost::shared_ptr<AttrDomNode>
  make(const sherpa::LexLoc& where, const std::string& nm, 
	      const std::string& v, bool sq = false) {
    AttrDomNode *tmp = new AttrDomNode(where, nm, v, sq);
    return boost::shared_ptr<AttrDomNode>(tmp);
  }

  ~AttrDomNode();
};

struct TextDomNode : DomNode {
  std::string  text;

  TextDomNode(const sherpa::LexLoc& where, const std::string& s)
  {
    name = "<text>";
    loc = where;
    text = s;
    nodeType = ntText;
  }

  static inline boost::shared_ptr<TextDomNode>
  make(const sherpa::LexLoc& where, const std::string& s) {
    TextDomNode *tmp = new TextDomNode(where, s);
    return boost::shared_ptr<TextDomNode>(tmp);
  }

  ~TextDomNode();
};

struct WhiteSpaceDomNode : TextDomNode {

  WhiteSpaceDomNode(const sherpa::LexLoc& where, const std::string& s)
    : TextDomNode(where, s)
  {
    nodeType = ntWhiteSpace;
  }

  static inline boost::shared_ptr<WhiteSpaceDomNode>
  make(const sherpa::LexLoc& where, const std::string& s) {
    WhiteSpaceDomNode *tmp = new WhiteSpaceDomNode(where, s);
    return boost::shared_ptr<WhiteSpaceDomNode>(tmp);
  }

  ~WhiteSpaceDomNode();
};

#endif /* DOMNODE_HXX */
