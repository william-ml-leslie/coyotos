#ifndef AST_HXX
#define AST_HXX


/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <string>
#include <vector>


#include <stdint.h>
#include <set>
#include <string>
#include <boost/shared_ptr.hpp>
#include <libsherpa/INOstream.hxx>
#include "Interp.hxx"



enum AstType {
  at_Null,
  at_AnyGroup,
  at_ident,
  at_intLiteral,
  at_uoc,
  at_unit,
  at_instances,
  at_instance,
  at_oreg,
  at_reg,
  at_describe,
  at_def,
  at_constdef,
  at_addrlist,
  at_bit,
  at_field,
  at_value,
  at_binop,
  at_unop,
  agt_topdef,
  agt_unitdef,
  agt_anyfield,
  agt_expr,
};

enum { at_NUM_ASTTYPE = agt_expr };

#ifndef AST_SMART_PTR
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#define AST_SMART_PTR(T) boost::shared_ptr<T>
#endif /* AST_SMART_PTR */

#ifndef AST_LOCATION_TYPE
#include <libsherpa/LexLoc.hxx>
#define AST_LOCATION_TYPE sherpa::LexLoc
#endif /* AST_LOCATION_TYPE */

#ifndef AST_TOKEN_TYPE
#include <libsherpa/LToken.hxx>
#define AST_TOKEN_TYPE sherpa::LToken
#endif /* AST_TOKEN_TYPE */

#ifndef AST_GC_THISPTR_SUPERCLASS
#define AST_GC_THISPTR_SUPERCLASS boost::enable_shared_from_this<AST>
#endif /* AST_GC_THISPTR_SUPERCLASS */

class AST :public AST_GC_THISPTR_SUPERCLASS { 
  bool isOneOf(AstType);
public:
  template <class T>
  struct GCPtr {
    typedef AST_SMART_PTR(T) Type;
  };

  AstType        astType;
  ::std::string    s;
  AST_LOCATION_TYPE loc;
  ::std::vector<GCPtr<AST>::Type > children;


 private:  
  static unsigned long long astCount;
  
 public:
  typedef std::set< std::string > DepSet;

  typedef uint64_t NumberType;

  unsigned long long ID; // Unique ID of this AST

  unsigned long flags;

  NumberType   intValue;
  char         opName;
  
  unsigned printVariant;	// which syntax to use for pretty printing

  static boost::shared_ptr<AST> makeIntLit(const sherpa::LToken &tok);
  static boost::shared_ptr<AST> makeOperator(const sherpa::LToken &tok, 
				 boost::shared_ptr<AST> arg);
  static boost::shared_ptr<AST> makeOperator(const sherpa::LToken &tok, 
				 boost::shared_ptr<AST> leftArg, boost::shared_ptr<AST> rightArg);
  
  std::string asString() const;

  void PrettyPrint(sherpa::INOstream& out) const;
  void PrettyPrint(std::ostream& out) const;

  // For use in GDB:
  void PrettyPrint() const;

  NumberType interp(InterpState& is);
  void check(CheckState& cs);
  void depends(const std::string& curPkg, DepSet& deps);
  void emit(EmitState& cs);

  AST(const AstType at = at_Null);
  // for literals:
  AST(const AstType at, const AST_TOKEN_TYPE& tok);
  AST(const AstType at, const AST_LOCATION_TYPE &loc);
  AST(const AstType at, const AST_LOCATION_TYPE &loc,
      GCPtr<AST>::Type child1);
  AST(const AstType at, const AST_LOCATION_TYPE &loc,
      GCPtr<AST>::Type child1,
      GCPtr<AST>::Type child2);
  AST(const AstType at, const AST_LOCATION_TYPE &loc,
      GCPtr<AST>::Type child1,
      GCPtr<AST>::Type child2,
      GCPtr<AST>::Type child3);
  AST(const AstType at, const AST_LOCATION_TYPE &loc,
      GCPtr<AST>::Type child1,
      GCPtr<AST>::Type child2,
      GCPtr<AST>::Type child3,
      GCPtr<AST>::Type child4);
  AST(const AstType at, const AST_LOCATION_TYPE &loc,
      GCPtr<AST>::Type child1,
      GCPtr<AST>::Type child2,
      GCPtr<AST>::Type child3,
      GCPtr<AST>::Type child4,
      GCPtr<AST>::Type child5);
  AST(const AstType at, const AST_LOCATION_TYPE &loc,
      GCPtr<AST>::Type child1,
      GCPtr<AST>::Type child2,
      GCPtr<AST>::Type child3,
      GCPtr<AST>::Type child4,
      GCPtr<AST>::Type child5,
      GCPtr<AST>::Type child6);
  AST(const AstType at, const AST_LOCATION_TYPE &loc,
      GCPtr<AST>::Type child1,
      GCPtr<AST>::Type child2,
      GCPtr<AST>::Type child3,
      GCPtr<AST>::Type child4,
      GCPtr<AST>::Type child5,
      GCPtr<AST>::Type child6,
      GCPtr<AST>::Type child7);
  AST(const AstType at, const AST_LOCATION_TYPE &loc,
      GCPtr<AST>::Type child1,
      GCPtr<AST>::Type child2,
      GCPtr<AST>::Type child3,
      GCPtr<AST>::Type child4,
      GCPtr<AST>::Type child5,
      GCPtr<AST>::Type child6,
      GCPtr<AST>::Type child7,
      GCPtr<AST>::Type child8);
  ~AST();

  // Helper quasi-constructors
  static inline GCPtr<AST>::Type
  make(const AstType at = at_Null)
  {
    AST *ast = new AST(at);
    return GCPtr<AST>::Type(ast);
  }

  static inline GCPtr<AST>::Type
  make(const AstType at, const AST_TOKEN_TYPE& tok)
  {
    AST *ast = new AST(at, tok);
    return GCPtr<AST>::Type(ast);
  }

  static inline GCPtr<AST>::Type
  make(const AstType at, const AST_LOCATION_TYPE &loc)
  {
    AST *ast = new AST(at, loc);
    return GCPtr<AST>::Type(ast);
  }

  static inline GCPtr<AST>::Type
  make(const AstType at, const AST_LOCATION_TYPE &loc,
       GCPtr<AST>::Type child1)
  {
    AST *ast = new AST(at, loc, child1);
    return GCPtr<AST>::Type(ast);
  }

  static inline GCPtr<AST>::Type
  make(const AstType at, const AST_LOCATION_TYPE &loc,
       const GCPtr<AST>::Type child1,
       const GCPtr<AST>::Type child2)
  {
    AST *ast = new AST(at, loc, child1, child2);
    return GCPtr<AST>::Type(ast);
  }

  static inline GCPtr<AST>::Type
  make(const AstType at, const AST_LOCATION_TYPE &loc,
       const GCPtr<AST>::Type child1,
       const GCPtr<AST>::Type child2,
       const GCPtr<AST>::Type child3)
  {
    AST *ast = new AST(at, loc, child1, child2,
                       child3);
    return GCPtr<AST>::Type(ast);
  }

  static inline GCPtr<AST>::Type
  make(const AstType at, const AST_LOCATION_TYPE &loc,
       const GCPtr<AST>::Type child1,
       const GCPtr<AST>::Type child2,
       const GCPtr<AST>::Type child3,
       const GCPtr<AST>::Type child4)
  {
    AST *ast = new AST(at, loc, child1, child2,
                       child3, child4);
    return GCPtr<AST>::Type(ast);
  }

  static inline GCPtr<AST>::Type
  make(const AstType at, const AST_LOCATION_TYPE &loc,
       const GCPtr<AST>::Type child1,
       const GCPtr<AST>::Type child2,
       const GCPtr<AST>::Type child3,
       const GCPtr<AST>::Type child4,
       const GCPtr<AST>::Type child5)
  {
    AST *ast = new AST(at, loc, child1, child2,
                       child3, child4, child5);
    return GCPtr<AST>::Type(ast);
  }

  static inline GCPtr<AST>::Type
  make(const AstType at, const AST_LOCATION_TYPE &loc,
       const GCPtr<AST>::Type child1,
       const GCPtr<AST>::Type child2,
       const GCPtr<AST>::Type child3,
       const GCPtr<AST>::Type child4,
       const GCPtr<AST>::Type child5,
       const GCPtr<AST>::Type child6)
  {
    AST *ast = new AST(at, loc, child1, child2,
                       child3, child4, child5,
                       child6);
    return GCPtr<AST>::Type(ast);
  }

  static inline GCPtr<AST>::Type
  make(const AstType at, const AST_LOCATION_TYPE &loc,
       const GCPtr<AST>::Type child1,
       const GCPtr<AST>::Type child2,
       const GCPtr<AST>::Type child3,
       const GCPtr<AST>::Type child4,
       const GCPtr<AST>::Type child5,
       const GCPtr<AST>::Type child6,
       const GCPtr<AST>::Type child7)
  {
    AST *ast = new AST(at, loc, child1, child2,
                       child3, child4, child5,
                       child6, child7);
    return GCPtr<AST>::Type(ast);
  }

  static inline GCPtr<AST>::Type
  make(const AstType at, const AST_LOCATION_TYPE &loc,
       const GCPtr<AST>::Type child1,
       const GCPtr<AST>::Type child2,
       const GCPtr<AST>::Type child3,
       const GCPtr<AST>::Type child4,
       const GCPtr<AST>::Type child5,
       const GCPtr<AST>::Type child6,
       const GCPtr<AST>::Type child7,
       const GCPtr<AST>::Type child8)
  {
    AST *ast = new AST(at, loc, child1, child2,
                       child3, child4, child5,
                       child6, child7, child8);
    return GCPtr<AST>::Type(ast);
  }


  const GCPtr<AST>::Type
  child(size_t i) const
  {
    return children[i];
  }

  GCPtr<AST>::Type&
  child(size_t i)
  {
    return children[i];
  }

  void addChild(GCPtr<AST>::Type cld);
  ::std::string getTokenString();

  void
  addChildrenFrom(GCPtr<AST>::Type other)
  {
    for(size_t i = 0; i < other->children.size(); i++)
      addChild(other->child(i));
  }

  static const char *tagName(const AstType at);
  static const char *nodeName(const AstType at);
  static const char *printName(const AstType at);

  inline const char *tagName() const
  { return tagName(astType); }

  inline const char *nodeName() const
  { return nodeName(astType); }

  inline const char *printName() const
  { return printName(astType); }

  bool isMemberOfType(AstType) const;
  bool isValid() const;
};


#endif /* AST_HXX */
