%{
/*
 * Copyright (C) 2006, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <sys/fcntl.h>
#include <sys/stat.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <dirent.h>
#include <iostream>

#include <string>

#include "AST.hxx"
#include "ParseType.hxx"
#include "Options.hxx"
  
using namespace std;
using namespace boost;
using namespace sherpa;

#include "Lexer.hxx"

#define YYSTYPE ParseType
#define YYLEX_PARAM (Lexer *)lexer
#undef yyerror
#define yyerror(lexer, topast, s) lexer->ReportParseError(s)

#define SHOWPARSE(s) \
  do { \
    if (Options::showParse) \
      lexer->errStream << (s) << std::endl;		\
  } while(false);
#define SHOWPARSE1(s,x) \
  do { \
    if (Options::showParse) \
      lexer->errStream << (s) << " " << (x) << std::endl;	\
  } while(false);


int num_errors = 0;  /* hold the number of syntax errors encountered. */

inline int regdeflex(YYSTYPE *lvalp, Lexer *lexer)
{
  return lexer->lex(lvalp);
}

%}

%lex-param {Lexer *lexer}
%pure-parser
%parse-param {Lexer *lexer}
%parse-param {boost::shared_ptr<AST> & topAst}

%token <tok> tk_Reserved	/* reserved words */

/* Categorical terminals: */
%token <tok> tk_Ident
%token <tok> tk_Int

 /* Statements */
%token <tok> ';' '{' '}' '='

 //%token <tok> tk_FROM
%token <tok> tk_INSTANCE tk_AT tk_IS tk_FROM tk_PACKAGE
%token <tok> tk_DEF tk_CONST
%token <tok> tk_REG tk_OREG tk_DESCRIBE
%token <tok> tk_BIT
%token <tok> tk_FIELD
%token <tok> tk_UNIT

 /* Customary expression operators here. Order of occurrence is
    significant, because it defines precedence. Here is the operator
    precedence in ANSI C, from highest to lowest:

     L   function() [] -> .
     R   ! ~ ++ -- unary+ unary- unary* & (type) sizeof
     L   * / %
     L   + -
     L   << >>
     L   < <= > >=
     L   == !=
     L   &
     L   ^
     L   |
     L   &&
     L   ||
     R   ?:
     R   = += -= *= /= %/ &= ^= |= <<= >>=
     L   ,
 */
 /* Lowest precedence */
%left <tok> '-' '+'
%left <tok> '*' '/' '%'
%right <tok> '!'
%left <tok> '('
%token <tok> ')'
 /* Highest precedence */

%type <ast> start topdef
// %type <ast> topdef_seq topdef
%type <ast> def constdef
%type <ast> instance instances
%type <ast> unit unitdef_seq unitdef
%type <ast> value_seq value
%type <ast> reg oreg
%type <ast> field field_seq
%type <ast> expr
%type <tok> pkgname

%%

start: tk_PACKAGE pkgname ';' topdef {
  SHOWPARSE("start -> topdef_seq");
  topAst = AST::make(at_uoc, $1.loc, AST::make(at_ident, $2), $4);
}

pkgname: {
    /* lexer->setIfIdentMode(true); */
  } tk_Ident {
    /* lexer->setIfIdentMode(false); */

  SHOWPARSE("pkgname -> Ident");
  $$ = $2;
}

pkgname: pkgname {
    /* lexer->setIfIdentMode(true); */
  } '.' tk_Ident {
    /* lexer->setIfIdentMode(false); */

  SHOWPARSE("pkgname -> pkgname '.' Ident");
  $$ = LToken($1.loc, $1.str + "." + $4.str);
}

topdef: reg {
  SHOWPARSE("topdef -> reg");
  $$ = $1;
};

topdef: def {
  SHOWPARSE("topdef -> def");
  $$ = $1;
};

topdef: constdef {
  SHOWPARSE("topdef -> constdef");
  $$ = $1;
};

topdef: unit {
  SHOWPARSE("topdef -> unit");
  $$ = $1;
};

//topdef_seq: topdef {
//  SHOWPARSE("topdef_seq -> topdef");
//  $$ = new AST(at_Null, $1->loc, $1);
//}

//topdef_seq: topdef_seq topdef {
//  SHOWPARSE("topdef_seq -> topdef_seq topdef");
//  $$ = $1;
//  $1->addChild($2);
//}

unit: tk_UNIT tk_Ident '{' unitdef_seq '}' {
  $$ = AST::make(at_unit, $1.loc,
		 AST::make(at_ident, $2),
		 AST::make(at_instances)	/* empty instances */
		 );
  $$->addChildrenFrom($4);
}

unit: tk_UNIT tk_Ident '{' instances unitdef_seq '}' {
  $$ = AST::make(at_unit, $1.loc,
		 AST::make(at_ident, $2),
		 $4
		 );

  $$->addChildrenFrom($5);
}

instance: tk_INSTANCE tk_Int tk_AT expr ';' {
  $$ = AST::make(at_instance, $1.loc,
		 AST::makeIntLit($2),
		 $4);
}

instance: tk_INSTANCE tk_Int tk_AT expr tk_FROM pkgname ';' {
  $$ = AST::make(at_instance, $1.loc,
		 AST::makeIntLit($2),
		 $4,
		 AST::make(at_ident, $6));
}

instances: instance {
  $$ = AST::make(at_instances, $1->loc, $1);
}

instances: instances instance {
  $$ = $1;
  $$->addChild($2);
}

unitdef: def {
  SHOWPARSE("unitdef -> def");
  $$ = $1;
};

unitdef: constdef {
  SHOWPARSE("unitdef -> constdef");
  $$ = $1;
};

unitdef: reg {
  SHOWPARSE("unitdef -> reg");
  $$ = $1;
};

unitdef: oreg {
  SHOWPARSE("unitdef -> oreg");
  $$ = $1;
};

unitdef_seq: unitdef {
  SHOWPARSE("unitdef_seq -> unitdef");
  $$ = AST::make(at_Null, $1->loc, $1);
}

unitdef_seq: unitdef_seq unitdef {
  SHOWPARSE("unitdef_seq -> unitdef_seq unitdef");
  $$ = $1;
  $1->addChild($2);
}

oreg: tk_REG tk_Ident expr expr ';' {
  SHOWPARSE("offset -> REG Ident expr expr");
  $$ = AST::make(at_oreg, $1.loc,
		 AST::make(at_ident, $2),
		 $3, $4,
		 AST::make(at_instances));
}

oreg: tk_REG tk_Ident expr expr '{' instances '}' {
  SHOWPARSE("offset -> REG Ident expr expr");
  $$ = AST::make(at_oreg, $1.loc,
		 AST::make(at_ident, $2),
		 $3, $4, $6);
}

oreg: tk_REG tk_Ident expr expr '{' field_seq '}' {
  SHOWPARSE("offset -> REG Ident expr expr");
  $$ = AST::make(at_oreg, $1.loc,
		 AST::make(at_ident, $2),
		 $3, $4, 
		 AST::make(at_instances));
  $$->addChildrenFrom($6);
}

oreg: tk_REG tk_Ident expr expr '{' instances field_seq '}' {
  SHOWPARSE("offset -> REG Ident expr expr");
  $$ = AST::make(at_oreg, $1.loc,
		 AST::make(at_ident, $2),
		 $3, $4, $6);
  $$->addChildrenFrom($7);
}

constdef: tk_CONST tk_Ident expr tk_IS expr ';' {
  SHOWPARSE("constdef -> CONST Ident expr = expr");
  $$ = AST::make(at_constdef, $1.loc,
		 AST::make(at_ident, $2),
		 $3, $5);
}

def: tk_DEF tk_Ident expr tk_AT expr ';' {
  SHOWPARSE("def -> DEF Ident expr AT expr");
  $$ = AST::make(at_def, $1.loc,
		 AST::make(at_ident, $2),
		 $3, $5);
}

reg: tk_REG tk_Ident expr ';' {
  SHOWPARSE("reg -> REG Ident expr");
  $$ = AST::make(at_reg, $1.loc,
		 AST::make(at_ident, $2),
		 $3);
}

reg: tk_REG tk_Ident expr '{' field_seq '}' {
  SHOWPARSE("reg -> REG Ident expr '{' field_seq '}' ");
  $$ = AST::make(at_reg, $1.loc,
		 AST::make(at_ident, $2),
		 $3);
  $$->addChildrenFrom($5);
}

// The DESCRIBE entry is used in corner cases where you have a number
// of registers in a single functional unit fitting a common pattern,
// e.g. Coldfire FlexBus.
reg: tk_DESCRIBE tk_Ident expr '{' field_seq '}' {
  SHOWPARSE("reg -> DESCRIBE Ident expr '{' field_seq '}' ");
  $$ = AST::make(at_describe, $1.loc,
		 AST::make(at_ident, $2),
		 $3);
  $$->addChildrenFrom($5);
}

field_seq: field {
  SHOWPARSE("field_seq -> field");
  $$ = AST::make(at_Null, $1->loc, $1);
};

field_seq: field_seq field {
  SHOWPARSE("field_seq -> field_seq field");
  $$ = $1;
  $1->addChild($2);
}

field: tk_BIT tk_Ident expr ';' {
  SHOWPARSE("bit -> Ident expr");
  $$ = AST::make(at_bit, $1.loc,
		 AST::make(at_ident, $2),
		 $3);
}

field: tk_BIT tk_Ident expr '{' value_seq '}'{
  SHOWPARSE("bit -> Ident expr '{' value_seq '}' ");
  $$ = AST::make(at_bit, $1.loc,
		 AST::make(at_ident, $2),
		 $3);

  $$->addChildrenFrom($5);
}

field: tk_FIELD tk_Ident expr expr ';' {
  SHOWPARSE("field -> Ident expr expr ';' ");
  $$ = AST::make(at_field, $1.loc,
		 AST::make(at_ident, $2),
		 $3, $4);
}

field: tk_FIELD tk_Ident expr expr '{' value_seq '}' {
  SHOWPARSE("field -> Ident expr expr '{' value_seq '}' ");
  $$ = AST::make(at_field, $1.loc,
		 AST::make(at_ident, $2),
		 $3, $4);

  $$->addChildrenFrom($6);
}

value: tk_Ident '=' expr ';' {
  SHOWPARSE("value -> Ident '=' expr");
  $$ = AST::make(at_value, $1.loc,
		 AST::make(at_ident, $1),
		 $3);
}

value_seq: value {
  SHOWPARSE("value_seq -> value");
  $$ = AST::make(at_Null, $1->loc, $1);
}

value_seq: value_seq value {
  SHOWPARSE("value_seq -> value_seq value");
  $$ = $1;
  $1->addChild($2);
}

//nexpr : expr '+' expr {
//  SHOWPARSE("expr -> expr '+' expr");
//  $$ = AST::makeOperator($2, $1, $3);
//};
//
//expr : expr '-' expr {
//  SHOWPARSE("expr -> expr '-' expr");
//  $$ = AST::makeOperator($2, $1, $3);
//};
//
//expr : expr '*' expr {
//  SHOWPARSE("expr -> expr '*' expr");
//  $$ = AST::makeOperator($2, $1, $3);
//};
//
//expr : expr '/' expr {
//  SHOWPARSE("expr -> expr '/' expr");
//  $$ = AST::makeOperator($2, $1, $3);
//};
//
//expr : expr '%' expr {
//  SHOWPARSE("expr -> expr '%' expr");
//  $$ = AST::makeOperator($2, $1, $3);
//};
//
//
//expr : '-' expr {
//  SHOWPARSE("expr -> '-' expr");
//  $$ = AST::makeOperator($1, $2);
//};
//
//expr : '(' expr ')' {
//  SHOWPARSE("expr -> '(' expr ')'");
//  $$ = $2;
//};

expr : tk_Int {
  SHOWPARSE("expr -> Int");
  $$ = AST::makeIntLit($1);
};

%%

