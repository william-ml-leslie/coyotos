/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <assert.h>
#include <stddef.h>
#include <vector>
#include <string>
#include <iomanip>

// #include <libsherpa/BigNum.hxx>
#include <libsherpa/UExcept.hxx>
#include "AST.hxx"

using namespace sherpa;

#define BUFSZ 20 /* 19 for digits + 1 for null */
// These are pulled temporarily from libsherpa::util.cxx until I can
// separate out those implementations.
static const char *
unsigned64_to_buffer(uint64_t ul, char *buf)
{
  char *p;

  buf[BUFSZ-1] = 0;
  p = &buf[BUFSZ-1];

  if (ul == 0) {
    *(--p) = '0';
  }
  else {
    while(ul) {
      *(--p) = '0' + (ul % 10);
      ul = ul / 10;
    }
  }

  return p;
}

/* bc says that 2^64-1==18446744073709551615, which looks to me like
   19 characters. */
static std::string
unsigned64_str(uint64_t ul)
{
  char buf[BUFSZ];
  const char *p = unsigned64_to_buffer(ul, buf);

  return p;
}

// #define DEBUG

using namespace std;
using namespace boost;

AST::NumberType
AST::interp(InterpState& is)
{
  is.curAST = shared_from_this();

  for (size_t c = 0; c < children.size(); c++) {
    InterpState child_is = is;
    child(c)->interp(child_is);
  }

  switch(astType) {
  case at_intLiteral:
    return intValue;

  case at_binop:
    switch (opName) {
    case '+':
      return child(0)->intValue + child(1)->intValue;
    case '-':
      return child(0)->intValue - child(1)->intValue;
    case '*':
      return child(0)->intValue * child(1)->intValue;
    case '/':
      return child(0)->intValue / child(1)->intValue;
    case '%':
      return child(0)->intValue % child(1)->intValue;
    default:
      is.errStream << loc << " "
		   << "Unknown binary operator '" << opName << "'.\n";
      THROW(excpt::BadValue, "Bad interpreter result");
    }

  case at_unop:
    return - child(0)->intValue;

  default:
    {
      return (NumberType)0;
    }
  }
}

void
AST::check(CheckState& cs)
{
  cs.curAST = shared_from_this();

  switch(astType) {
  case at_unit:
    {
      // Propagate unit instance information to our children so that
      // we can validate the combined address.
      cs.instances = child(1);
      break;
    }

  case at_intLiteral:
    break;

      // Propagate unit instance information to our children.
      cs.instances = child(1);
    /// @bug It is regrettable that there is no way to validate
    /// instance offsets.

  case at_constdef:
    {
      NumberType width = child(1)->intValue;

      // Want width to be a power of two and a multiple of bytes.
      if (! (width == 8 || width == 16 || width == 32 || width == 64)) {
	cs.errStream << loc << " "
		     << "Invalid constant width " << width << ".\n";
	cs.valid = false;
	// Can't sensibly check beneath here, so don't bother recursing.
	return;
      }

      break;
    }

  case at_describe:
  case at_reg:
  case at_def:
  case at_oreg:
    {
      NumberType width = child(1)->intValue;

      // Want width to be a power of two and a multiple of bytes.
      if (! (width == 8 || width == 16 || width == 32 || width == 64)) {
	cs.errStream << loc << " "
		     << "Invalid register width " << width << ".\n";
	cs.valid = false;
	// Can't sensibly check beneath here, so don't bother recursing.
	return;
      }

      cs.regWidth = width;

      if (astType != at_reg && astType != at_describe) {
	// Check that offset/address is a multiple of width:
	uint8_t bytes = cs.regWidth / 8;

	if (child(2)->astType != at_addrlist) {
	  NumberType addr = child(2)->intValue;
	  if ((addr % bytes) != NumberType(0)) {
	    cs.errStream << loc << " "
			 << "Address 0x" 
			 << std::hex << addr << std::dec 
			 << " is not a properly aligned multiple of " 
			 << cs.regWidth
			 << "-bit register width\n";
	    cs.valid = false;
	  }
	}
	else {
	  for (size_t i = 0; i < child(2)->children.size(); i++) {
	    NumberType addr = child(2)->child(i)->intValue;
	    if ((addr % bytes) != NumberType(0)) {
	      cs.errStream << loc << " "
			   << "Address 0x" 
			   << std::hex << addr << std::dec 
			   << " is not a properly aligned multiple of " 
			   << cs.regWidth
			   << "-bit register width\n";
	      cs.valid = false;
	    }
	  }
	}
      }

      break;
    }

  case at_bit:
    {
      NumberType pos = child(1)->intValue;
      if (pos >= cs.regWidth) {
	cs.errStream << loc << " "
		     << "Bit position is larger than register width.\n";
	cs.valid = false;
	return;
      }
      
      break;
    }

  case at_field:
    {
      uint32_t bottom = child(1)->intValue;
      if (bottom >= cs.regWidth) {
	cs.errStream << loc << " "
		     << "Lower bit position is larger than register width.\n";
	cs.valid = false;
	return;
      }
      
      uint32_t top = child(2)->intValue;
      if (top >= cs.regWidth) {
	cs.errStream << loc << " "
		     << "Upper bit position is larger than register width.\n";
	cs.valid = false;
	return;
      }

      if (top == bottom) {
	cs.errStream << loc << " "
		     << "Single bit field should be defined using BIT keyword.\n";
	cs.valid = false;
      }

      if (top < bottom) {
	cs.errStream << loc << " "
		     << "Low bit should be smaller than high bit.\n";
	cs.valid = false;
	return;
      }

      cs.baseBit = bottom;
      cs.regWidth = (top + 1) - bottom;

      break;
    }

  case at_value:
    {
      NumberType limit = NumberType(1) << cs.regWidth;
      NumberType val = child(1)->intValue;
      if (val >= limit) {
	cs.errStream << loc << " "
		     << "Field value " << val << " exceeds range of "
		     << cs.regWidth << " field size.\n";
	cs.valid = false;
      }

      break;
    }

  default:
    /* Just process children */
    break;
  }

  for (size_t c = 0; c < children.size(); c++) {
    CheckState child_cs = cs;
    child(c)->check(child_cs);
  }
}

static std::string 
concat(const std::string& pfx, const std::string sfx)
{
  if (pfx.size() == 0)
    return sfx;
  else
    return pfx + "_" + sfx;
}

void
AST::depends(const std::string& pkgName, DepSet& deps)
{
  if (astType == at_instance) {
    if (children.size() > 2) {
      std::string baseRegName = child(2)->s;
      std::string::size_type pos = baseRegName.rfind('.');

      if (pos != std::string::npos) {
	std::string modName = baseRegName.substr(0, pos);

	if (modName != pkgName)
	  deps.insert(modName);
      }
    }
  }

  for (size_t c = 0; c < children.size(); c++)
    child(c)->depends(pkgName, deps);
}

void
AST::emit(EmitState& es)
{
  es.curAST = shared_from_this();

  switch(astType) {
  case at_uoc:
    {
      es.pkgName = child(0)->s;
      es.nm = es.pkgName;

      for (size_t i = 0; i < es.nm.size(); i++)
	if (es.nm[i] == '.')
	  es.nm[i] = '_';

      break;
    }

  case at_unit:
    {
      es.nm = concat(es.nm, child(0)->s);

      es.ostream << "#ifndef __" << es.nm << "_H__\n";
      es.ostream << "#define __" << es.nm << "_H__\n\n";

      es.ostream << "#ifndef _REGDEF8\n"
		 << "#ifndef __ASSEMBLER__\n"
		 << "#define _REGDEF8(x)  ((volatile uint8_t *) (x))\n"
		 << "#define _REGDEF16(x) ((volatile uint16_t *) (x))\n"
		 << "#define _REGDEF32(x) ((volatile uint32_t *) (x))\n"
		 << "#define _REGDEF64(x) ((volatile uint64_t *) (x))\n"
		 << "#else /* __ASSEMBLER__ */\n"
		 << "#define _REGDEF8(x)  (x)\n"
		 << "#define _REGDEF16(x) (x)\n"
		 << "#define _REGDEF32(x) (x)\n"
		 << "#define _REGDEF64(x) (x)\n"
		 << "#endif /* __ASSEMBLER__ */\n"
		 << "#endif\n\n";

      DepSet deps;
      depends(es.pkgName, deps);

      for(DepSet::iterator itr = deps.begin(); itr != deps.end(); ++itr) {
	std::string hdrName = (*itr);
	for(size_t c = 0; c < hdrName.size(); c++)
	  if (hdrName[c] == '.')
	    hdrName[c] = '/';

	es.ostream << "#include \"" << hdrName << ".h\"\n";
      }
      if (deps.size())
	es.ostream << "\n";

      // Propagate unit instance information to our children.
      es.instances = child(1);
      break;
    }

  case at_constdef:
    {
      NumberType& addr = child(2)->intValue;
      es.nm = concat(es.nm, child(0)->s);
      es.ostream << "#define " << es.nm << " "
		 << "0x" << std::hex << addr << std::dec
		 << "\n";
      break;
    }

  case at_def:
    {
      NumberType& addr = child(2)->intValue;
      es.nm = concat(es.nm, child(0)->s);
      es.ostream << "#define " << es.nm << "_value "
		 << "0x" << std::hex << addr << std::dec
		 << "\n"
		 << "#define " << es.nm << "(x) ("
		 << es.nm 
		 << "_value + (x))\n";
      break;
    }

  case at_reg:
  case at_describe:
    {
      /* Nothing specific to put out for a bare register definition. */
      es.nm = concat(es.nm, child(0)->s);
      break;
    }

  case at_oreg:
    {
      // Following name will be passed to children for field
      // definitions, and will be used here as the register name if
      // there are no explicit instances.
      std::string unitBaseName = es.nm;
      std::string regName = concat(unitBaseName, child(0)->s);

      es.nm = regName; // for fields


      NumberType regSize = child(1)->intValue;

      /* Because all top-level entities are units, we allow units that
	 have no instances. For example, the registers of the CPU core
	 might be described by a top-level unit having no explicitly
	 identified instances. 

	 An oreg definition in this case would correspond to a
	 register whose global address is hardwired by the
	 architecture. */


      if (es.instances->children.size() == 0) {
	NumberType& offset = child(2)->intValue;
	es.ostream << "#define " << es.nm 
		   << " " 
		   << "_REGDEF"
		   << regSize
		   << "("
		   << std::hex << offset << std::dec
		   << ")\n";
      }

      else {
	/* Need to emit one per unit instance, but also need to respect
	   local instance overrides. */
	for (size_t i = 0; i < es.instances->children.size(); i++) {

	  boost::shared_ptr<AST> instance = es.instances->child(i);
	  NumberType instNo = instance->child(0)->intValue;
	  
	  // Fetch the default offset. This may be overridden by
	  // local instance override.
	  NumberType offset = child(2)->intValue;

	  // Check for locally overridden instance offset
	  boost::shared_ptr<AST> localInstances = child(3);
	  for (size_t li = 0; li < localInstances->children.size(); li++) {
	    if (localInstances->child(li)->child(0)->intValue == instNo) {
	      instance = localInstances->child(li);
	      offset = 0;
	    }
	  }

	  NumberType instanceBase = instance->child(1)->intValue;

	  std::string unitName = unitBaseName;
	  if (es.instances->children.size() > 1)
	    unitName = unitBaseName + unsigned64_str(instNo);

	  std::string unitRegName = concat(unitName, child(0)->s);

	  bool hasBaseReg = instance->children.size() > 2;

	  std::string baseRegName;
	  if (hasBaseReg) {
	    baseRegName = instance->child(2)->s;
	    for (size_t i = 0; i < baseRegName.size(); i++)
	      if (baseRegName[i] == '.') 
		baseRegName[i] = '_';
	  }

	  if (hasBaseReg) {
	    es.ostream << "#define " << unitRegName 
		       << " " 
		       << "_REGDEF"
		       << regSize
		       << "("
		       << baseRegName << "(0x" 
		       << std::hex << instanceBase << std::dec
		       << ") + "
		       << "0x" << std::hex << offset << std::dec
		       << ")\n";
	  }
	  else {
	    es.ostream << "#define " << unitRegName
		       << "_REGDEF"
		       << regSize
		       << "("
		       << "0x" << std::hex << instanceBase << std::dec
		       << " + "
		       << "0x" << std::hex << offset << std::dec
		       << ")\n";
	  }
	}

	if (es.instances->children.size() > 1) {
	  es.ostream << "#define " << regName << "(__unit) \\\n";

	  for (size_t i = 0; i < es.instances->children.size(); i++) {
	    boost::shared_ptr<AST> instance = es.instances->child(i);
	    NumberType instNo = instance->child(0)->intValue;

	    std::string unitName = unitBaseName + unsigned64_str(instNo);
	    std::string unitRegName = concat(unitName, child(0)->s);

	    es.ostream << "  (((__unit) == " 
		       << i
		       << ") ? " 
		       << unitRegName
		       << " : \\\n";
	  }

	  // Need a default if we approach it this way. Thank god for
	  // constant folding!
	  es.ostream << "  _REGDEF"
		     << regSize
		     << "(0)";

	  for (size_t i = 0; i < es.instances->children.size(); i++) {
	    es.ostream << ')';
	  }
	  es.ostream << '\n';
	}
      }

#if 0
      if (es.instances->children.size() == 0) {
	NumberType& offset = child(2)->intValue;
	es.nm = concat(es.nm, child(0)->s);
	es.ostream << "#define " << es.nm 
		   << " " << es.fromReg
		   << "(" << std::hex << offset << std::dec
		   << ")\n";
      }
#endif
      break;
    }

  case at_bit:
    {
      NumberType pos = child(1)->intValue;
      es.nm = concat(es.nm, child(0)->s);
      es.baseBit = pos;
      std::string sfx = "";
      if (pos > 31)
	sfx = "ull";

      es.ostream << "#define " << es.nm 
		 << " 0x" << std::hex
		 << ((NumberType)1 << pos)
		 << std::dec
		 << sfx << "\n";

      break;
    }

  case at_field:
    {
      NumberType bottom = child(1)->intValue;
      NumberType top = child(2)->intValue;

      es.baseBit = bottom;
      es.regWidth = (top + 1) - bottom;

      std::string sfx = "";
      if (es.baseBit > 31)
	sfx = "ull";

      NumberType limit = NumberType(1) << es.regWidth;
      limit -= 1;

      es.nm = concat(es.nm, child(0)->s);
      es.ostream << "#define " << es.nm 
		 << " 0x" << std::hex
		 << (limit << es.baseBit)
		 << std::dec
		 << "\n";

      es.ostream << "#define " << es.nm 
		 << "_shift " << es.baseBit << "\n";

      if (children.size() < 4) {
	/* No field values are provided. Emit a generic value
	   constructor. This exploits macro overloading by argument
	   count. */

	es.ostream << "#define " << es.nm 
		   << "_val(i) (0x" << std::hex
		   << (1u << es.baseBit)
		   << std::dec
		   << sfx
		   << " * (i))\n";
      }
      break;
    }

  case at_value:
    {
      es.nm = concat(es.nm, child(0)->s);

      std::string sfx = "";
      if (es.baseBit > 31)
	sfx = "ull";


      NumberType val = child(1)->intValue;
      es.ostream << "#define " << es.nm 
		 << " 0x" << std::hex
		 << (val << es.baseBit)
		 << std::dec
		 << sfx
		 << "\n";

      break;
    }

  default:
    /* Just process children */
    break;
  }

  for (size_t c = 0; c < children.size(); c++) {
    EmitState child_es = es;
    child(c)->emit(child_es);
  }

  if (astType == at_unit)
    es.ostream << "\n#endif /* __" << es.nm << "_H__ */\n";
}

InterpState::InterpState(std::ostream & _errStream, 
			 boost::shared_ptr<AST> _curAST)
  : errStream(_errStream)
{
  curAST = _curAST;
}

InterpState::InterpState(const InterpState& is)
  : errStream(is.errStream)
{
  curAST = is.curAST;
}

CheckState::CheckState(std::ostream & _errStream, 
		       boost::shared_ptr<AST> _curAST)
  : errStream(_errStream)
{
  curAST = _curAST;
  regWidth = 0;
  baseBit = 0;
  valid = true;
}

CheckState::CheckState(const CheckState& cs)
  : errStream(cs.errStream)
{
  curAST = cs.curAST;
  instances = cs.instances;
  regWidth = cs.regWidth;
  baseBit = cs.baseBit;
  valid = cs.valid;
}

EmitState::EmitState(std::ostream & _ostream, 
		     boost::shared_ptr<AST> _curAST)
  : ostream(_ostream)
{
  curAST = _curAST;  
  regWidth = 0;
  baseBit = 0;
  nm = "";
  fromReg = "";
}

EmitState::EmitState(const EmitState& es)
  : ostream(es.ostream)
{
  curAST = es.curAST;
  instances = es.instances;
  regWidth = es.regWidth;
  baseBit = es.baseBit;
  nm = es.nm;
  pkgName = es.pkgName;
  fromReg = es.fromReg;
}
