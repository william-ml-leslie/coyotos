/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdint.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>


#include <fstream>
#include <getopt.h>
#include <langinfo.h>
#include <iostream>
#include <sstream>

#include <boost/shared_ptr.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <libsherpa/UExcept.hxx>

#include "Version.hxx"
#include "AST.hxx"
#include "Options.hxx"
#include "Lexer.hxx"
#include "Interp.hxx"

using namespace std;
using namespace boost;
using namespace sherpa;

bool Options::showLex = false;
bool Options::showParse = false;
bool Options::dumpTree = false;
bool Options::emitDeps = false;
boost::filesystem::path Options::outputFileName;
boost::filesystem::path Options::depFile;
  
#define LOPT_SHOWLEX      257   /* Show tokens */
#define LOPT_SHOWPARSE    258   /* Show parse */
#define LOPT_DUMP         259   /* Dump the tree */
#define LOPT_MDEPEND      260   /* Write dependencies too */
#define LOPT_MDEPFILE     261   /* Target name for dependencies */

struct option longopts[] = {
  { "MD",                   0,  0, LOPT_MDEPEND },
  { "MF",                   1,  0, LOPT_MDEPFILE },
  { "dump",                 0,  0, LOPT_DUMP },
  { "help",                 0,  0, 'h' },
  { "showlex",              0,  0, LOPT_SHOWLEX },
  { "showparse",            0,  0, LOPT_SHOWPARSE },
  {0,                       0,  0, 0}
};

void
help()
{
  std::cerr 
    << "Usage:" << endl
    << "  regdef [-o outfile] infile\n"
    << "Debugging options:\n"
    << "  --showlex --showparse --help\n" 
    << flush;
}
 
void
fatal()
{
  cerr << "Confused due to previous errors, bailing."
       << endl;
  exit(1);
}


int
main(int argc, char *argv[]) 
{
  int c;
  extern int optind;
  int opterr = 0;

  while ((c = getopt_long(argc, argv, 
			  "o:Vh",
			  longopts, 0
		     )) != -1) {
    switch(c) {
    case 'V':
      cerr << "regdef Version: " << REGDEF_VERSION << endl;
      exit(0);
      break;

    case LOPT_SHOWPARSE:
      Options::showParse = true;
      break;

    case LOPT_SHOWLEX:
      Options::showLex = true;
      break;

    case LOPT_DUMP:
      Options::dumpTree = true;
      break;

    case LOPT_MDEPEND:
      Options::emitDeps = true;
      break;

    case LOPT_MDEPFILE:
      Options::depFile = optarg;
      break;

    case 'h': 
      {
	help();
	exit(0);
      }

    case 'o':
      Options::outputFileName = optarg;
      break;

    default:
      opterr++;
      break;
    }
  }
  
  argc -= optind;
  argv += optind;
  
  if (argc == 0)
    opterr++;

  if (argc > 1)
    opterr++;

  if (opterr) {
    std::cerr << "Usage: Try regdef --help" << std::endl;
    exit(1);
  }

  try {
    std::string fileName(argv[0]);
    std::ifstream fin(fileName.c_str(), std::ios_base::binary);

    if (!fin.is_open()) {
      std::cerr << "Couldn't open input file \""
		<< fileName
		<< "\"\n";
    }

    Lexer lexer(std::cerr, fin, fileName);

    lexer.setDebug(Options::showLex);

    boost::shared_ptr<AST> ast;

    extern int regdefparse(Lexer *lexer, boost::shared_ptr<AST> &topAst);
    regdefparse(&lexer, ast);  
    // On exit, ast is a pointer to the AST tree root.
  
    fin.close();

    if (lexer.num_errors != 0u) {
      ast = AST::make(at_Null);
      return false;
    }

    if(!ast->isValid()) {
      std::cerr << "PANIC: Invalid AST built for file \""
		<< fileName.c_str()
		<< "\". "
		<< "Please report this problem.\n"; 
      ast->PrettyPrint(std::cerr);
      std::cerr << std::endl;
      exit(1);
    }

    assert(ast->astType == at_uoc);

    InterpState is(std::cerr, ast);
    (void) ast->interp(is);

    CheckState cs(std::cerr, ast);
    (void) ast->check(cs);
    if (!cs.valid)
      exit(1);

    if (!Options::outputFileName.empty()) {
      ofstream ofs(Options::outputFileName.string().c_str(),
		   ofstream::out | ofstream::trunc);
      if (ofs.fail()) {
	boost::filesystem::remove(Options::outputFileName);
	std::cerr << "Could not write output file \""
		  << Options::outputFileName << "\"\n";
	THROW(excpt::IoError, "Output write failure");
      }

      EmitState es(ofs, ast);
      (void) ast->emit(es);
    }
    else {
      EmitState es(std::cout, ast);
      (void) ast->emit(es);
    }

    if (Options::emitDeps) {
      if (Options::outputFileName.empty()) {
	std::cerr << "Cannot generate depedencies for output to stdout.\n";
	THROW(excpt::BadValue, "Bad option combination");

      }

      if (Options::depFile.empty()) {
	std::cout << Options::outputFileName << ": " << argv[0] << "\n";
      }
      else {
	ofstream dfs(Options::depFile.string().c_str(),
		     ofstream::out | ofstream::trunc);
	if (dfs.fail()) {
	  if (!Options::outputFileName.empty())
	    boost::filesystem::remove(Options::outputFileName);
	  std::cerr << "Could not write dependency file \""
		    << Options::depFile << "\"\n";
	  THROW(excpt::IoError, "Depend write failure");
	}

	dfs << Options::outputFileName << ": " << argv[0] << "\n";
      }
    }

    if (Options::dumpTree) {
      INOstream ino(std::cout);
      ast->PrettyPrint(ino);
      ino << '\n';
    }
  }
  catch ( const sherpa::UExceptionBase& ex ) {
    std::cerr << "Exception "
	      << ex.et->name
	      << " at "
	      << ex.file
	      << ":"
	      << ex.line
	      << ": "
	      << ex.msg
	      << std::endl;
    exit(1);
  }

  exit(0);
}
