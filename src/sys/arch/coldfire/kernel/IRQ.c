/*
 * Copyright (C) 2008, The EROS Group, LLC
 * Copyright (C) 2024, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Data structures and initialization code for the Coldfire
 * interrupt handling mechanism.
 */
#include <hal/irq.h>
#include <kerninc/printf.h>
#include <kerninc/vector.h>
#include <kerninc/event.h>
#include <kerninc/Queue.h>
#include "IRQ.h"
#include "hwmap.h"
#include "MCF/ExceptWord.h"

#define DEBUG_PAGEFAULT if (0)

#define SAVED_VECNO(w)   (((w) >>  18) & 0xffu)
#define SAVED_ISUSER(w)  (((w) & MCF_SR_S) == 0)
#define SAVED_ISSUPER(w) (((w) & MCF_SR_S) == MCF_SR_S)

/* This is moderately unusual. It turns out that we are always doing
 * constant compare on the FS field, so rather than try to extract a
 * four bit quantity from the exception frame we use bit select and
 * build up the appropriate constant value using constant arithmetic:
 */
#define SAVED_FS(w)  ((w) & (MCF_ExceptWord_FS_hi|MCF_ExceptWord_FS_lo))
#define FS_VALUE(i)  \
  ( MCF_ExceptWord_FS_hi_val((i & 0xCu)>>2) | MCF_ExceptWord_FS_lo_val(i & 0x3u) )

#define FS(w)						\
  ((((w) >> (MCF_ExceptWord_FS_hi_shift - 2)) & 0xc) |	\
   (((w) >> MCF_ExceptWord_FS_lo_shift) & 0x3))



/** @brief Reverse map for finding vectors from IRQ numbers */
VectorInfo *IrqVector[NUM_IRQ];

/** @brief Handler function for user-mode interrupts. */
void
vh_BoundIRQ(VectorInfo *vec, Process *inProc, fixregs_t *saveArea)
{
  assert(!local_interrupts_enabled());

  vec->status = vec_st_fired;

  if (!queue_isEmpty(&vec->stallQ)) {
    vec->next = CUR_CPU->wakeVectors;
    CUR_CPU->wakeVectors = vec;
  }
}

VectorInfo *
irq_MapInterrupt(irq_t irq)
{
  switch (IRQ_BUS(irq)) {
  case IBUS_GLOBAL:
    return &VectorMap[irq+64];

  default:
    fatal("Unknown bus type for binding\n");
  }
}

/** @brief Set up the software vector table to point to the provided
    handler procedure. */
void
irq_Bind(irq_t irq, uint32_t mode, uint32_t level, VecFn fn)
{
  VectorInfo *vector;

  switch (IRQ_BUS(irq)) {
  case IBUS_GLOBAL:
    {
      mode = VEC_MODE_LEVEL;
      level = VEC_LEVEL_ACTHIGH; /* ?? */

      vector = &VectorMap[IRQ_PIN(irq)+64];
      break;
    }

  default:
    fatal("Unknown bus type for binding\n");
  }

  assert(vector);

  VectorHoldInfo vhi = vector_grab(vector);

  vector->count = 0;
  vector->fn = fn;

  vector->mode = mode;
  vector->level = level;
  vector->ctrlr->setup(vector);

  vector_release(vhi);
}

/** @brief Software-level interrupt dispatch mechanism. */
struct VectorInfo VectorMap[NUM_VECTOR];

/** @brief Handler function for unbound vectors. */
static void
vh_UnboundVector(VectorInfo *vec, Process *inProc, fixregs_t *saveArea)
{
  fatal("Trap from %s to unbound vector %d\n", 
	inProc ? "process" : "kernel",
	SAVED_VECNO(saveArea->ExceptWord));
}

/** @brief Handler function for unbound interrupts. */
void
vh_UnboundIRQ(VectorInfo *vec, Process *inProc, fixregs_t *saveArea)
{
  fatal("Received orphaned %s interrupt %d [excptWord=0x%x]\n", 
	inProc ? "process" : "kernel",
	vec->irq, saveArea->ExceptWord);
}

static void 
vh_ReservedException(VectorInfo *vec, Process *inProc, fixregs_t *saveArea)
{
  fatal("Reserved exception 0x%x\n", SAVED_VECNO(saveArea->ExceptWord));
}

static void 
vh_PageFault(VectorInfo *vec, Process *inProc, fixregs_t *saveArea)
{
  uintptr_t e = saveArea->ExceptWord;
  uintptr_t addr = saveArea->ExceptAddr;

  LOG_EVENT(ety_PageFault, inProc, e, addr);

  /** @bug wait;  should we be using current instead of inProc? */
  DEBUG_PAGEFAULT
    printf("Page fault trap va=0x%0P, exceptWord 0x%x\n", saveArea->ExceptAddr,
	   saveArea->ExceptWord);

  /* There appear to be no faults arising from reserved bits in the
     PTE-equivalent. */
  assert((inProc == 0) == SAVED_ISSUPER(e));

  if (addr >= KVA && SAVED_ISUSER(e))
    bug("supervisor-mode pagefault at 0x%x\n", addr);

  if (inProc == 0) {
    /* We have taken a page fault from kernel mode while attempting to
       access a user-mode address. This can occur only from the
       invocation path. What we do here is attribute the page fault to
       the current process and proceed exactly as if we had faulted
       from user mode. */
    inProc = MY_CPU(current);
    assert(inProc);
    printf("User-address pagefault in supervisor mode at EIP=0x%P VA=0x%P\n",
	saveArea->ExceptPC, addr);
    /* Enable interrupts, because we want to allow preemption during
       processing of this page fault.

       Note that this creates a possible exception nesting on the
       kernel stack:

       Top
         PageFault
           Other Interrupt
    */
    locally_enable_interrupts(local_get_flags() & ~MCF_SR_I_MASK);
  }

  /* Error on instruction fetch should not occur. Should manifest as
     ITLB miss. */
  assert (SAVED_FS(e) != 0x4);
  /* Error on data write should not occur. Should manifest as either
     DTLB miss or attempted [read-modify-]write of protected space. */
  assert (SAVED_FS(e) != 0x8);
  /* Error on data read should not occur. Should manifest as DTLB miss. */
  assert (SAVED_FS(e) != 0xc);	/* error on data read */
  
#if 1
  bool wantExec =  ((SAVED_FS(e) >= FS_VALUE(0x4)) && (SAVED_FS(e) <= FS_VALUE(0x7)));
  bool wantWrite = ((SAVED_FS(e) == FS_VALUE(0x9)) || (SAVED_FS(e) == FS_VALUE(0xd)));
#else
  uint32_t fs = FS(e);
  bool wantExec =  (fs >= 0x4) && (fs <= 0x7);
  bool wantWrite =  (fs == 0x9) || (fs == 0xd);
#endif


  DEBUG_PAGEFAULT
    printf("do_pageFault(proc=0x%P, va=0x%08x, %s, %s, false)\n", inProc, addr,
	   wantWrite ? "wantWrite" : "0", wantExec ? "wantExec" : "0");

  do_pageFault(inProc, addr, wantWrite, wantExec, false);

  DEBUG_PAGEFAULT
    printf("do_pageFault(...) done\n");

  vm_switch_curcpu_to_map(inProc->mapping);
}

static void
vh_UserFault(VectorInfo *vec, Process *inProc, fixregs_t *saveArea)
{
  fatal("User-mode faults not yet delivered.\n");
}

void
vh_SysCall(VectorInfo *vec, Process *inProc, fixregs_t *saveArea)
{
  /* On TRAP #n, CPU saves PC of next instruction. Need to unwind that
     to enable system call restart. */
  assert(inProc);

  FIX_PC(inProc->state.regs.fix) -= 2;

  proc_syscall();

  if (atomic_read(&inProc->issues) & pi_SysCallDone)
    FIX_PC(inProc->state.regs.fix) += 2;
}

static void 
vh_DebugTrap(VectorInfo *vec, Process *inProc, fixregs_t *saveArea)
{
  /* We can get here via a trace trap, a Non-PC Breakpoint interrupt,
   * or a PC breakpoint interrupt. I have also wired the usual
   * breakpoint trap through this path.
   *
   * All of these cases advance the PC, so we correct it here.
 */

  assert(inProc);

  FIX_PC(inProc->state.regs.fix) -= 2;
  
  uint32_t faultCode = 
    (SAVED_VECNO(saveArea->ExceptWord) == vec_BptInstr)
    ? coyotos_Process_FC_BreakPoint
    : coyotos_Process_FC_Debug;

  proc_TakeFault(inProc, faultCode, 0);
}

static void
vh_FatalFault(VectorInfo *vec, Process *inProc, fixregs_t *saveArea)
{
  uint32_t vecno = SAVED_VECNO(saveArea->ExceptWord);

  switch(vecno) {
  /* Faults that are systemwide fatal */
  case vec_FormatError:
    {
      fatal("Unexpected FormatError\n");
    }
  case vec_UninitializedIntr:
    {
      fatal("Unexpected uninitialized interrupt\n");
    }
  case vec_SpuriousIntr:
    {
      fatal("Spurious interrupt\n");
    }
  }
}

void
irq_OnTrapOrInterrupt(Process *inProc, fixregs_t *saveArea)
{
  LOG_EVENT(ety_Trap, inProc, saveArea->ExceptWord, 0);

#ifndef NDEBUG
  /* If we are coming in from user mode, then either (a) this is a an
   * interrupt which will preempt us, or (b) this is an exception. In
   * either case all CPU flags should be zero.
   */
  if (inProc)
    assert(atomic_read(&CUR_CPU->flags) == 0);
#endif 

  // printf("OnTrapOrInterrupt with vector %d\n", vecno);

  VectorInfo *vector = &VectorMap[SAVED_VECNO(saveArea->ExceptWord)];

  if (vector->type == vt_Interrupt) { /* hardware interupt */
#ifndef NDEBUG
    Process *preIrqProc = MY_CPU(current);
#endif

    irq_t irq = vector->irq;
    IrqController *ctrlr = vector->ctrlr;

    /* If level triggered interrupts are in use, or if the controller
     * has ringing problems, the following check will filter spurious
     * interrupts. 
     */
    if (! ctrlr->isPending(vector)) {
      fatal("IRQ %d no longer pending\n", irq);
      ctrlr->ack(vector);

      return;
    }

    /* Interrupt actually appears to be pending. This is good. Mask
     * the interrupt at the interrupt controller acknowledge we have
     * taken responsibility for it.
     */
    vector_set_status(vector, vec_st_fired);

    vector->count++;
    vector->fn(vector, inProc, saveArea);

#ifndef NDEBUG
    assert (preIrqProc == MY_CPU(current));
#endif

    /* If the low-level handler preempted us, put ourselves back onto
     * the ready queue, enable interrupts, and abandon the current
     * transaction. */
    if (inProc && (atomic_read(&MY_CPU(flags)) & CPUFL_WAS_PREEMPTED)) {
      runnable_schedule(inProc);

      /* Enable interrupts, because choosing a new process can involve
	 a fair bit of work. */
      locally_enable_interrupts(local_get_flags() & ~MCF_SR_I_MASK);


      sched_abandon_transaction();
    }

    /* We do NOT re-enable the taken interrupt on the way out. That is
     * the driver's job to do if it wants to do it.
     */
  }
  else {
    locally_enable_interrupts(local_get_flags() & ~MCF_SR_I_MASK);

    if (inProc)
      mutex_grab(&inProc->hdr.lock);

    vector->count++;
    vector->fn(vector, inProc, saveArea);
  }

  if (inProc) {
    assert(MY_CPU(current));
    proc_dispatch_current();

    /* NOTREACHED */
  }

  /* Return from kernel interrupt. */
  return;
}

void
vector_init()
{
  for (size_t i = 0; i < NUM_VECTOR; i++) {
    VectorMap[i].type = vt_Unbound;
    VectorMap[i].mode = VEC_MODE_FROMBUS;
    VectorMap[i].level = VEC_LEVEL_FROMBUS;
    VectorMap[i].fn = vh_UnboundVector;
    VectorMap[i].status = vec_st_uninit;
    queue_init(&VectorMap[i].stallQ);
  }

  /* Vectors 2..23, inclusive, are hardware traps: */
  for (size_t i = 2; i <= 23; i++)
    VectorMap[i].type = vt_HardTrap;

  VectorMap[vec_AccessError].fn       = vh_PageFault;
  VectorMap[vec_AddressError].fn      = vh_UserFault;
  VectorMap[vec_IllegalInstr].fn      = vh_UserFault;
  VectorMap[vec_DivZero].fn           = vh_UserFault;
  VectorMap[vec_PrivViolation].fn     = vh_UserFault;
  VectorMap[vec_Trace].fn             = vh_DebugTrap;
  VectorMap[vec_BadOpcodeLineA].fn    = vh_UserFault;
  VectorMap[vec_BadOpcodeLineF].fn    = vh_UserFault;
  VectorMap[vec_NonPCBreakpoint].fn   = vh_DebugTrap;
  VectorMap[vec_PCBreakpoint].fn      = vh_DebugTrap;
  VectorMap[vec_FormatError].fn       = vh_FatalFault;

  VectorMap[vec_UninitializedIntr].fn = vh_FatalFault; /* ??? */
  VectorMap[vec_SpuriousIntr].fn      = vh_FatalFault; /* ??? */
  
#if 0
  /* Vectors 24..31, inclusive, are interrupts: */
  for (size_t i = 24; i <= 31; i++) {
  }
#endif

  /* Vectors 32..47 are user-initiatable traps. Wire vectors 32..46 to
   * vh_SysCall.  By convention, trap 47 is used for BPT
   * instruction. Wire that to vh_DebugTrap. */
  for (size_t i = 32; i <= 46; i++) {
    VectorMap[i].type = vt_SysCall;
    VectorMap[i].fn = vh_SysCall;
    VectorMap[i].user = 1;
    VectorMap[i].status = vec_st_live;
  }

  VectorMap[47].fn = vh_DebugTrap;
  VectorMap[47].user = 1;
  VectorMap[47].status = vec_st_live;

  /* Vectors 48..55 are floating point traps */
  for (size_t i = 48; i <= 55; i++) {
    VectorMap[i].type = vt_HardTrap;
    VectorMap[i].fn = vh_UserFault;
  }

  /* Vectors 56..63 are reserved */
  for (size_t i = 56; i <= 63; i++) {
    VectorMap[i].type = vt_HardTrap;
    VectorMap[i].fn = vh_ReservedException; /* until otherwise proven */
  }

  /* Except that vector 61 is unsupported instruction */
  VectorMap[vec_UnsupportedInstr].fn  = vh_UserFault;
}
