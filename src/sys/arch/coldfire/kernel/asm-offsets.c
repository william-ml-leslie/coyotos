/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Generator for assembly-language offsets against the Process
 * and register layout data structures.
 *
 * This is conceptually derived from a technique proposed by Keith
 * Owens for linux 2.5, but it shares no code.
 *
 * Note that this file is NOT linked into the Coyotos kernel. It is
 * used only to generate an intermediate assembly file, which is in
 * turn used to produce asm-offsets.h via an AWK script.
 */

#include <stddef.h>
#include <kerninc/ccs.h>
#include <kerninc/Process.h>
#include <kerninc/Endpoint.h>
#include <kerninc/GPT.h>
#include <kerninc/Depend.h>

// #define offsetof(ty, fld) ((size_t) &((ty *) 0)->fld)

#define ASMDEF(sym, struct, field) \
  GNU_INLINE_ASM("\n#define " #sym " %0" :: "i" (offsetof(struct, field)))

#define ASMSIZEOF(sym, ty) \
  GNU_INLINE_ASM("\n#define " #sym " %0" :: "i" (sizeof(ty)))

int
main(void)
{
  ASMDEF(PR_OFF_FIXREGS, Process, state.regs.fix);
  ASMDEF(PR_OFF_REENTRY_SP, Process, state.regs.fp);

  ASMDEF(PR_OFF_ISSUES, Process, issues);
  ASMDEF(PR_OFF_SP, Process, state.regs.fix.a[7]);
  ASMDEF(PR_OFF_AREGS, Process, state.regs.fix.a);
  ASMDEF(PR_OFF_DREGS, Process, state.regs.fix.d);
  ASMDEF(PR_OFF_PFADDR, Process, state.regs.fix.ExceptAddr);
  ASMDEF(PR_OFF_EXCEPTWORD, Process, state.regs.fix.ExceptWord);
  ASMDEF(PR_OFF_EXCEPTPC, Process, state.regs.fix.ExceptPC);

  ASMDEF(PR_OFF_ONCPU, Process, onCPU);
  ASMDEF(CPU_OFF_STACKTOP, CPU, topOfStack);

  ASMDEF(FIX_OFF_SP, fixregs_t, a[7]);
  ASMDEF(FIX_OFF_AREGS, fixregs_t, a);
  ASMDEF(FIX_OFF_DREGS, fixregs_t, d);
  ASMDEF(FIX_OFF_PFADDR, fixregs_t, ExceptAddr);
  ASMDEF(FIX_OFF_EXCEPTWORD, fixregs_t, ExceptWord);
  ASMDEF(FIX_OFF_EXCEPTPC, fixregs_t, ExceptPC);

  ASMSIZEOF(PROCESS_SIZE, Process);
  ASMSIZEOF(GPT_SIZE, GPT);
  ASMSIZEOF(PAGESTRUCT_SIZE, Page);
  ASMSIZEOF(ENDPOINT_SIZE, Endpoint);
  ASMSIZEOF(DEPEND_SIZE, Depend);
  ASMSIZEOF(CPU_SIZE, CPU);
  return 0;
}
