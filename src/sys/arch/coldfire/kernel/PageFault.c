/*
 * Copyright (C) 2007, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Page Fault implementation.
 */

#include <stddef.h>
#include <kerninc/assert.h>
#include <kerninc/printf.h>
#include <kerninc/Process.h>
#include <kerninc/MemWalk.h>
#include <kerninc/RevMap.h>
#include <kerninc/GPT.h>
#include <kerninc/Depend.h>
#include <kerninc/util.h>
#include "hwmap.h"
#include "kva.h"
#include "MCF/ExceptWord.h"

/* Set this to add readable user mappings for code pages so that GDB
   can disassemble user-mode code. */
#define DEBUG_GDB_USER if (1)

#define DEBUG_PGFLT if (0)
#define DEBUG_TRANSMAP if (0)

#define SAVED_FS(w)  ((w) & (MCF_ExceptWord_FS_hi|MCF_ExceptWord_FS_lo))
#define FS_VALUE(i)  \
  ( MCF_ExceptWord_FS_hi_val((i & 0xCu)>>2) | MCF_ExceptWord_FS_lo_val(i & 0x3u) )

void
do_pageFault(Process *p, uintptr_t fault_va, 
	     bool wantWrite, 
	     bool wantExec,
	     bool wantCap)
{
  assert(p == MY_CPU(current));

  /** @bug Lots of bugs, fixes, and comments to carry over from
   * i386. */
  uintptr_t addr = fault_va;

  coyotos_Process_FC result;
  MemWalkResults mwr;
  size_t restr;

  size_t restr_mask = 
    CAP_RESTR_RO | CAP_RESTR_WK | CAP_RESTR_NX;
  size_t leaf_restr_mask = 
    CAP_RESTR_CD | CAP_RESTR_WT | restr_mask;

  result = memwalk(&p->state.addrSpace, addr, wantWrite, &mwr);

  if (addr >= KVA) {
    result = coyotos_Process_FC_InvalidDataReference;
    goto deliver_fault;
  }

  if (wantExec && (mwr.cum_restr & CAP_RESTR_NX)) {
    result = coyotos_Process_FC_NoExecute;
    goto deliver_fault;
  }

  if (result != 0)
    goto deliver_fault;

  MemWalkEntry *mwe = &mwr.ents[0];
  MemWalkEntry *mwe_last = &mwr.ents[mwr.count-1];

  if (!wantCap && (mwe_last->entry->hdr.ty == ot_CapPage)) {
    result = coyotos_Process_FC_DataAccessTypeError;
    goto deliver_fault;
  }

  restr = (mwe->restr & restr_mask);

  /* 
   * The target of the addrSpace capability is always the producer of
   * the top-level table.  This prevents us from needing Depend
   * entries that target Processes.
   */

  /* minl2 is the least l2g or l2v value seen so far. */
  size_t minl2 = min(COYOTOS_HW_ADDRESS_BITS, mwe->l2g);

  Mapping *curmap = p->mapping;

  if (curmap == &KernMapping || (curmap->restr != mwe->restr)) {
    /* No current top-level map for this process. */

    /* The top-level pointer has no restrictions, so they must all be
     * pushed into the mapping table.
     *
     * Want to use mwe->restr because don't want to be switching
     * mappings every time a particular path permission differs from
     * the previous path visited. */
    curmap = 
      mapping_get(mwe->entry, mwe->guard, ~(((coyaddr_t)2 << (minl2 - 1)) - 1), mwe->restr);

    rm_install_process_mapping(curmap, p);
    p->mapping = curmap;
  }

  assert(curmap->asid != KernMapping.asid);

  /* We are already at the leaf entry. Do we need to install a revmap
   * entry for this?
   *
   * Yes. Either we need to introduce a revmap entry for every GPT
   * traversed, or we need to flush the TLB every time a GPT slot is
   * modified. At present we do not have a hazard bit to use.
   *
   * So I need to copy over the GPT walk-by code from i386.
   */

  // We are generating a leaf entry, so the producer is the target
  // page. Pass through the intervening GPTs to build the required
  // depend entries:
  for (;;) {
    /* Stop if this is the producer of the next page table.
     * Note that pages have mwe->l2v == 0, so we will stop when
     * we reach the last MemWalkEntry. */
    if (!mwe->window && 
	(minl2 < COYOTOS_PAGE_ADDR_BITS || mwe->l2v < COYOTOS_PAGE_ADDR_BITS))
      break;

    if (mwe->slot != MEMWALK_SLOT_BACKGROUND) {
      // generate a depend entry for this GPT
      assert(mwe->entry->hdr.ty == ot_GPT);

      minl2 = min(minl2, mwe->l2v);

      DependEntry de;
      de.gpt = (GPT *)mwe->entry;
      de.map = curmap;
#if 0
      /* This would be for a hierarchical table structure: */
      de.slotMask = 1u << mwe->slot;
      de.slotBias = mwe->slot;
      de.l2slotSpan = min(minl2, curPT->l2table) - curPT->l2slot;
      de.basePTE = slot & ~((1u << de.l2slotSpan) - 1);
#else
      de.va = fault_va;
#endif

      depend_install(de);
    }

    /** @bug Need a depend entry for the MEMWALK_SLOT_BACKGROUND
     * slot case. */

    mwe++;
    restr |= (mwe->restr & restr_mask);
    minl2 = min(minl2, mwe->l2g);
    continue;
  }

  restr |= (mwe->restr & leaf_restr_mask);

  // To install a writable mapping to a page, it must be marked dirty
  if (!(restr & (CAP_RESTR_RO|CAP_RESTR_WK)) && wantWrite) {
    /* Target page may be immutable. Is this the right result
       code? */
    if (!obhdr_mark_dirty(&mwe_last->entry->hdr)) {
      result = coyotos_Process_FC_AccessViolation;
      goto deliver_fault;
    }
  } else {
    /* if the page isn't already dirty, make the PTE read-only. */
    if (!mwe_last->entry->hdr.dirty)
      restr |= CAP_RESTR_RO;
  }
  /* Large pages:  if l2g of the page is bigger than 
   * COYOTOS_PAGE_ADDR_BITS, we need to include the correct address bits
   * in our target PA.
   */
  kpa_t target_pa = 
    ((Page *)mwe_last->entry)->pa + (mwe_last->remAddr & ~COYOTOS_PAGE_ADDR_MASK);

  /* Do the revmap insertions here, which is after the last
   * possible fault.
   *
   * For a table-less design you can record prefix bits to do
   * range-respecting invalidation. The common prefix bits are
   *
   *   (addr & ~(safe_left_shift(1u,mwe->l2g) - 1u)).
   *
   * That plus the l2g itself tells you what you need to know to get
   * base and bound, which is enough to do invalidate on a table-less
   * design.
   */

#if 0
  /* FOLLOWING IS FROM I386:

     We should use while (mwe != mwe_last) {}
  */

  // Loop to pass through the non-producing GPTs, so that we can find
  // the next producer. That will be the producer of the next page
  // table down. We will bail out if we reach the leaf page.
  for (;;) {

    /* FOLLOWING NOT NEEDED - ALWAYS WALKING TO END */
    /* Stop if this is the producer of the next page table.
     * Note that pages have mwe->l2v == 0, so we will stop when
     * we reach the last MemWalkEntry. */
    if (!mwe->window && 
	(minl2 < curPT->l2slot || mwe->l2v < curPT->l2slot))
      break;

    /* FOLLOWING NEEDED */
    if (mwe->slot != MEMWALK_SLOT_BACKGROUND) {
      // generate a depend entry for this GPT
      assert(mwe->entry->hdr.ty == ot_GPT);

      minl2 = min(minl2, mwe->l2v);

      /* UPDATE HERE */
      DependEntry de;
      de.gpt = (GPT *)mwe->entry;
      de.map = curmap;
      de.slotMask = 1u << mwe->slot;
      de.slotBias = mwe->slot;
      de.l2slotSpan = min(minl2, curPT->l2table) - curPT->l2slot;
      de.basePTE = slot & ~((1u << de.l2slotSpan) - 1);

      depend_install(de);
    }

    mwe++;
    restr |= (mwe->restr & restr_mask);
    minl2 = min(minl2, mwe->l2g);
    continue;
  }
#endif

  /* Danger, Will Robinson!
   *
   * As of Feb 12, 2008, the TLB/WTLB commands of the BDI2000 probe
   * cannot be used reliably while single stepping this sequence.  The
   * probe firmware rewrites MMUTR, MMUDR, MMUAR, and MMUOR using the
   * BDM interface in order to perform those commands, but it does not
   * restore the previous (program established) values.  At the end of
   * the TLB/WTLB command, MMUTR and MMUDR hold the last values
   * read/written.
   *
   * You can work around this by:
   *
   *  - Writing them down and restore them by hand, or
   *  - Step to the end of the sequence, and THEN use TLB/WTLB to
   *    inspect the result.
   */

  /* Set up the TLB entry. We don't yet handle cache modes properly: */
  assert((restr & (CAP_RESTR_CD|CAP_RESTR_WT)) == 0);

  uint32_t mmutr = 
    ((fault_va & ~COYOTOS_PAGE_ADDR_MASK)
     | MCF_MMU_MMUTR_ID_val(curmap->asid)
     | MCF_MMU_MMUTR_V);

  uint32_t mmudr_cm = 0;
  uint32_t mmudr = 
    ( (target_pa & ~COYOTOS_PAGE_ADDR_MASK)
      | MCF_MMU_MMUDR_R
      | ((restr & CAP_RESTR_RO) ? 0 : MCF_MMU_MMUDR_W)
      | ((restr & CAP_RESTR_NX) ? 0 : MCF_MMU_MMUDR_X)
      | MCF_MMU_MMUDR_SZ_sz8K
      | mmudr_cm );

  /* Following just for grins: */
  // uint32_t mmuar = 0;

  uint32_t mmuor =
    (MCF_MMU_MMUOR_ACC | MCF_MMU_MMUOR_UAA | (wantExec ? MCF_MMU_MMUOR_ITLB : 0));
  *MCF_MMU_MMUTR = mmutr;
  *MCF_MMU_MMUDR = mmudr;
  // *MCF_MMU_MMUAR = mmuar;
  *MCF_MMU_MMUOR = mmuor;

  DEBUG_GDB_USER if (wantExec)
    *MCF_MMU_MMUOR = (mmuor & ~MCF_MMU_MMUOR_ITLB);

  return;

 deliver_fault:
  assert(result != 0);
  proc_deliver_memory_fault(p, result, fault_va, &mwr);
  /*  proc_TakeFault(MY_CPU(current), result, addr); */
}
