/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file 
 * @brief Initial interval timer configuration
 *
 * Coyotos requires that some form of initial timer be set up to drive
 * round-robin scheduling during early application-level
 * bootstrap. The interval timer will rapidly be taken over by
 * user-mode driver code, but an initial setting is helpful in order
 * drive an initial round-robin policy while the drivers sort
 * themselves out during boot.
 */

#include <kerninc/printf.h>
#include <kerninc/assert.h>
#include <kerninc/Sched.h>
#include <kerninc/event.h>
#include <kerninc/Interval.h>
#include "IRQ.h"
#include "MCF/SLT.h"

#ifdef BSP_m5485evb
/* Temporary. For debugging LEDs */
#include "BSP/defs.h"
#include "MCF/GPT.h"
#endif

/* We have configured SLT1 as a free-running timer and SLT0 as an
   interval timer. */

static uint64_t slt1_ticks_since_start;
static uint64_t slt1_last_count;

#define BASETICK 200		/* 5 ms */

static inline uint32_t slt1_read_tick()
{
  return *MCF_SLT1_SCNT;
}

// #define USE_LAPIC
#define DEBUG_HARDCLOCK if (0)

/** @brief Interval timer wakeup handler.
 *
 * The assumption here is that we will never miss a tick!
 */
static void 
hardclock_interval_wakeup(VectorInfo *vec, Process *inProc, fixregs_t *saveArea)
{
  uint64_t last = slt1_last_count;
  slt1_last_count = slt1_read_tick();

  /* CMOS uses a 16 bit counter. Deal with underflow: */
  if (last < slt1_last_count) last += 0x10000000ull;
  uint32_t delta = last - slt1_last_count;

  slt1_ticks_since_start += delta;

  Interval curTick;

  curTick.sec = slt1_ticks_since_start / BSP_defs_SYSCLK;
  curTick.usec = slt1_ticks_since_start % BSP_defs_SYSCLK;
  curTick.usec /= 100;

  interval_update_now(curTick);

#ifdef BSP_m5485evb
  /* Debugging LEDs: */
  *MCF_GPT_GMS(3) = (curTick.sec & 0x1u) ? 0x34 : 0x24;
  *MCF_GPT_GMS(1) = (curTick.sec & 0x2u) ? 0x34 : 0x24;
  *MCF_GPT_GMS(0) = (curTick.sec & 0x4u) ? 0x34 : 0x24;
#endif /* BSP_m5485evb */

  /* Write 1 to ST bit of SLT0 SSR to clear interrupt before re-enable. */
  *MCF_SLT0_SSR = MCF_SLT_SSR_ST;


  /* Re-enable the periodic timer interrupt line: */
  vector_set_status(vec, vec_st_live);

  //  printf("%s Timer Interrupt!\n", inProc ? "Process" : "Kernel");

  /* Preemption has occurred. Arrange for current process to be
   * preempted on return from unterrupt if a preemption has not
   * already occurred.
   *
   * Note that the timer may get set in motion before any current
   * process is established. It is also possible that we take a
   * preemption timer interrupt while we are hunting down a new
   * process to run. In either case, MY_CPU(current) will be NULL and
   * there is really nothing much to do here.
     */
  if (MY_CPU(current) &&
      ((atomic_read(&CUR_CPU->flags) & CPUFL_WAS_PREEMPTED) == 0)) {
    /* If the timer is free-running, it may be set in motion before
     * there is any process on the current CPU. In that case there is
     * nothing to preempt yet.
     */
    atomic_set_bits(&MY_CPU(current)->issues, pi_Preempted);
    atomic_set_bits(&CUR_CPU->flags, CPUFL_WAS_PREEMPTED);

    LOG_EVENT(inProc ? ety_UserPreempt : ety_KernPreempt,
	      MY_CPU(current), 0, 0);
    DEBUG_HARDCLOCK
      printf("Current process preempted by timer (from %s).\n",
	     inProc ? "user" : "kernel");
  }

  return;
}

void
hardclock_init()
{
  /* Halt the timers temporarily. */
  *MCF_SLT0_SCR = 0;
  *MCF_SLT1_SCR = 0;

  irq_Bind(irq_SLT0, VEC_MODE_FROMBUS, VEC_LEVEL_FROMBUS, 
	   hardclock_interval_wakeup);

  /* SLT1 is free-running counter at SYSCLK rate, no interrupts: */
  *MCF_SLT1_STCNT = 0xffffffff;
  *MCF_SLT1_SCR = MCF_SLT_SCR_RUN|MCF_SLT_SCR_TEN;  

  /* SLT0 set for 10ms intervals, or 100 per minute: */
  *MCF_SLT0_STCNT = BSP_defs_SYSCLK / BASETICK;
  *MCF_SLT0_SCR = MCF_SLT_SCR_RUN|MCF_SLT_SCR_TEN|MCF_SLT_SCR_IEN;

  VectorInfo *vec = irq_MapInterrupt(irq_SLT0);
  vector_set_status(vec, vec_st_live);
}
