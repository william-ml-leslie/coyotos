/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Manipulation of the hardware mapping table.
 */

#include <stddef.h>
#include <kerninc/assert.h>
#include <kerninc/printf.h>
#include <kerninc/Depend.h>
#include <kerninc/GPT.h>
#include <kerninc/PhysMem.h>
#include <kerninc/Cache.h>
#include <kerninc/Process.h>
#include <kerninc/malloc.h>
#include <kerninc/util.h>
#include <kerninc/AgeList.h>
#include <kerninc/pstring.h>
#include "hwmap.h"
#include "kva.h"

PDPT KernPDPT;

Mapping KernMapping;

/** @brief True if we are using PAE mode. Set in boot.S as we do
 * initial CPU feature probe.
 */
bool IA32_UsingPAE = false;
bool IA32_NXSupported = false;
bool IA32_HavePSE = false;

/** @bug This can be optimized. If the passed asid is KernMapping.asid
 * (which is zero), then we have to flush unconditionally, because
 * this is a transmap flush. Otherwise, if the passed asid does not
 * match the current ASID, we don't need to do anything.
 */
void
local_tlb_flush(asid_t asid)
{
  GNU_INLINE_ASM("mov %%cr3,%%eax\n"
		 "mov %%eax,%%cr3\n"
		 : /* No outputs */
		 : /* No inputs */
		 : "ax");
}

/** @bug This can be optimized. If the passed asid is KernMapping.asid
 * (which is zero), then we have to flush unconditionally, because
 * this is a transmap flush. Otherwise, if the passed asid does not
 * match the current ASID, we don't need to do anything.
 */
void
local_tlb_flushva(asid_t asid, kva_t va)
{
  /* Mapping argument is not used on this target. */

  GNU_INLINE_ASM("invlpg %0\n"
		 : /* no output */
		 : "m" (*(char *)va)
		 : "memory"
		 );
}

/* Following are placeholder implementations */
void
global_tlb_flush(asid_t asid)
{
  local_tlb_flush(asid);
}

void
hwmap_enable_low_map()
{
  if (IA32_UsingPAE)
    KernPDPT.entry[0] = KernPDPT.entry[3];
  else
    KernPageDir[0] = KernPageDir[768];
}

void
hwmap_disable_low_map()
{
  if (IA32_UsingPAE)
    PTE_CLEAR(KernPDPT.entry[0]);
  else
    PTE_CLEAR(KernPageDir[0]);
}


static void
reserve_pgtbls(void)
{
  MappingCache *mc = &Cache.mappings[MAPPING_CLASS_NORMAL];

  kpa_t pa = 
    pmem_AllocBytes(&pmem_need_pages, COYOTOS_PAGE_SIZE * mc->count, 
		    pmu_PGTBL, "page tbls");

  mc->vec = CALLOC(Mapping, mc->count);
  for (size_t i = 0; i < mc->count; i++) {
    mc->vec[i].asid = 1+i;
    mc->vec[i].pa = pa + COYOTOS_PAGE_SIZE * i;
    ageable_init(&mc->vec[i].age);
    agelist_addBack(&mc->ageList, &mc->vec[i]);
  }
}

static void
reserve_pdpts(void)
{
  if (!IA32_UsingPAE)
    return;

#if 0
  assert(Cache.c_Process.count);

  size_t nPDPT = Cache.c_Process.count * 16;	/* pretty arbitrary */
  size_t nPDPTbytes = align_up(nPDPT * sizeof(PDPT), COYOTOS_PAGE_SIZE);
  nPDPT = nPDPTbytes / sizeof(PDPT);

  /* Grab them as physical pages, mainly because this guarantees
     pleasant alignment. */
  size_t nPDPTpages = nPDPTbytes / COYOTOS_PAGE_SIZE;

#endif

  /* PDPT structures are pointed to by hardware page directory
     register, which can only reference structures within the physical
     low 4G. Grab page-aligned start for best possible alignment. */
  PmemConstraint low4g = pmem_need_pages;
  low4g.bound = 0x0ffffffffllu;

  MappingCache *mc = &Cache.mappings[MAPPING_CLASS_PDPT];

  kpa_t pa = 
    pmem_AllocBytes(&low4g, COYOTOS_PAGE_SIZE * mc->count, 
		    pmu_PGTBL, "PDPTs");

  /* Yes, the ASID ids assigned here do overlap with the ones assigned
   * above. Note, however, that the ASID of an address space is the
   * ASID of its uppermost Mapping, and on any given processor either
   * ALL of the uppermost mappings are PDPTs or none of them are.
   */
  mc->vec = CALLOC(Mapping, mc->count);
  for (size_t i = 0; i < mc->count; i++) {
    mc->vec[i].asid = 1+i;
    mc->vec[i].pa = pa + sizeof(PDPT) * i;
    ageable_init(&mc->vec[i].age);
    agelist_addBack(&mc->ageList, &mc->vec[i]);
  }
}

size_t
pagetable_reserve(size_t nPages)
{
  /** @bug The following selections are completely arbitrary, and
   * should be based on something meaningful.
   */
  size_t nReserve = 0;

  Cache.mappings[MAPPING_CLASS_NORMAL].count = nPages/10;
  nReserve += Cache.mappings[MAPPING_CLASS_NORMAL].count;

  if (IA32_UsingPAE) {
    size_t nPDPT = 1024;
    size_t nPDPTbytes = align_up(nPDPT * sizeof(PDPT), COYOTOS_PAGE_SIZE);
    nPDPT = nPDPTbytes / sizeof(PDPT);
    nReserve += nPDPTbytes / COYOTOS_PAGE_SIZE;

    Cache.mappings[MAPPING_CLASS_PDPT].count = nPDPT;
  }

  return nReserve;
}

void 
pagetable_init(void)
{
  printf("Reserving page tables\n");
  reserve_pgtbls();
  reserve_pdpts();

  if (IA32_UsingPAE) {
    KernMapping.pa = KVTOP(&KernPDPT);
    KernMapping.level = 2;
  }
  else {
    KernMapping.pa = KVTOP(&KernPageDir);
    KernMapping.level = 1;
  }
  ageable_init(&KernMapping.age);
  CUR_CPU->curMap = &KernMapping;
}

/** @brief Make the mapping @p m undiscoverable by removing it from
 * its product chain. 
 *
 * @bug This should be generic code, but it isn't obvious what source
 * file to stick it in.
 */
static void
mapping_make_unreachable(Mapping *m)
{
  /* Producer chains are guarded by the mappingListLock. Okay to
     fiddle them here. */

  MemHeader *hdr = m->producer;
  Mapping **mPtr = &hdr->products;

  while (*mPtr != m)
    mPtr = &((*mPtr)->nextProduct);

  /* We really *should* actually be *on* the product list: */
  assert(*mPtr == m);

  *mPtr = m->nextProduct;
}

static Mapping *
pgtable_alloc(size_t level)
{
  assert(mutex_isheld(&Cache.mappingsLock));

  const size_t cls =
    (level == 2) ? MAPPING_CLASS_PDPT : MAPPING_CLASS_NORMAL;
  MappingCache *mc = &Cache.mappings[cls];

  assert(level < 2 || IA32_UsingPAE);

  Mapping *pt = agelist_oldest(&mc->ageList);

  if (pt->producer) {

    /* whack any outstanding pointers to this page table */
    rm_whack_mapping(pt);

    /* and remove it from its product chain. */
    mapping_make_unreachable(pt);
  }

  /* Re-initialize mapping table to safe state */
  pt->match = 0;
  pt->mask = 0;
  pt->restr = 0;
  pt->level = level;

  if (level == 2) {
    memcpy_vtop(pt->pa, &KernPDPT, sizeof(KernPDPT));
    pt->userSlots = PDPT_SIZE - 1;
  } else if (level == 1 && !IA32_UsingPAE) {
    /* Re-initialize PDPT to safe state */
    memcpy_vtop(pt->pa, &KernPageDir, COYOTOS_PAGE_SIZE);
    pt->userSlots = PTE_PGDIR_NDX(KVA);
  } else {
    memset_p(pt->pa, 0, COYOTOS_PAGE_SIZE);
    pt->userSlots = IA32_UsingPAE ? NPAE_PER_PAGE : NPTE_PER_PAGE;
  }

  /* Newly allocated, so move to front. */
  agelist_remove(&mc->ageList, pt);
  agelist_addFront(&mc->ageList, pt);

  return pt;
}

Mapping *
pgtbl_get(MemHeader *hdr, size_t level, 
	  coyaddr_t guard, coyaddr_t mask, size_t restr)
{
  assert(level <= 1 + IA32_UsingPAE);

  HoldInfo hi = mutex_grab(&Cache.mappingsLock);

  Mapping *cur;

  for (cur = hdr->products;
       cur != NULL;
       cur = cur->nextProduct) {
    if (cur->level == level &&
	cur->match == guard &&
	cur->mask == mask &&
	cur->restr == restr) {

      const size_t cls =
	(level == 2) ? MAPPING_CLASS_PDPT : MAPPING_CLASS_NORMAL;
      MappingCache *mc = &Cache.mappings[cls];

      agelist_remove(&mc->ageList, cur);
      agelist_addFront(&mc->ageList, cur);

      goto done;
    }
  }
  
  cur = pgtable_alloc(level);

  cur->level = level;
  cur->match = guard;
  cur->mask = mask;
  cur->restr = restr;

  cur->producer = hdr;
  cur->nextProduct = hdr->products;
  hdr->products = cur;

 done:
  mutex_release(hi);
  return cur;
}

/** @brief Disassociate all mappings from an object without destroying
 * the content of those mapping structures.
 */
void
memhdr_invalidate_products(MemHeader *hdr)
{
  HoldInfo hi = mutex_grab(&Cache.mappingsLock);
  Mapping *pt;

  for (pt = hdr->products; pt != 0; pt = pt->nextProduct)
    rm_whack_mapping(pt);

  mutex_release(hi);
}

void
memhdr_destroy_products(MemHeader *hdr)
{
  HoldInfo hi = mutex_grab(&Cache.mappingsLock);

  Mapping *pt;

  while ((pt = hdr->products) != NULL) {
    /* Make this page table undiscoverable. */
    mapping_make_unreachable(pt);

    rm_whack_mapping(pt);

    const size_t cls =
      (pt->level == 2) ? MAPPING_CLASS_PDPT : MAPPING_CLASS_NORMAL;
    MappingCache *mc = &Cache.mappings[cls];

    agelist_remove(&mc->ageList, pt);
    agelist_addBack(&mc->ageList, pt);
  }

  mutex_release(hi);
}

void
depend_entry_invalidate(const DependEntry *entry, int slot)
{
  assert ((slot == -1) || (slot >= 0 && slot < NUM_GPT_SLOTS));

  Mapping *map = entry->map;
  size_t mask = entry->slotMask;

  if (IA32_UsingPAE) {
    uintptr_t offset = (map->pa & COYOTOS_PAGE_ADDR_MASK);
    char *map_base = TRANSMAP_MAP(map->pa - offset, char *);
    IA32_PAE *pte = (IA32_PAE *)(map_base + offset);
    size_t maxpte = COYOTOS_PAGE_SIZE / sizeof (*pte);
    
    size_t base = entry->basePTE;
    size_t slotSize = (1u << entry->l2slotSpan);
    size_t slotBias = entry->slotBias;
    assert((base & (slotSize - 1)) == 0);

    for (size_t i = slotBias; i < NUM_GPT_SLOTS; i++) {
      if (slot >= 0 && i != slot)
	continue;
      if (mask & (1u << i)) {
	int biased = i - slotBias;
	assert(base + biased * slotSize + (slotSize - 1) < maxpte);
	for (size_t j = 0; j < slotSize; j++) {
	  size_t slot = base + biased * slotSize + j;
	  if (slot >= map->userSlots)
	    continue;

	  pte_invalidate((struct PTE *)&pte[slot]);
	}
      }
    }
    TRANSMAP_UNMAP(map_base);
  }
  else {
    IA32_PTE *pte = TRANSMAP_MAP(map->pa, IA32_PTE *);
    size_t maxpte = COYOTOS_PAGE_SIZE / sizeof (*pte);
    
    size_t base = entry->basePTE;
    size_t slotSize = (1u << entry->l2slotSpan);
    size_t slotBias = entry->slotBias;
    assert((base & (slotSize - 1)) == 0);

    for (size_t i = slotBias; i < NUM_GPT_SLOTS; i++) {
      if (slot >= 0 && i != slot)
	continue;
      if (mask & (1u << i)) {
	size_t biased = i - slotBias;
	assert(base + biased * slotSize + (slotSize - 1) < maxpte);
	for (size_t j = 0; j < slotSize; j++) {
	  size_t slot = base + biased * slotSize + j;
	  if (slot >= map->userSlots)
	    continue;

	  pte_invalidate((struct PTE *)&pte[slot]);
	}
      }
    }
    TRANSMAP_UNMAP(pte);
  }

  /* Now that all of the entries are invalidated, flush them from the
     TLB. Unfortunately we need to use KernMapping.asid if this is not
     a top-level mapping, because lower-level page tables can be
     shared by upper level page tables. */
  asid_t asid = 
    (map->level == KernMapping.level) ? map->asid : KernMapping.asid;
  global_tlb_flush(asid);

  /** @bug need to cross-call other CPUs to have them flush their TLBs.
   * Also, do we want to delay doing this until all of the Depend entries
   * are gone?
   */
}

void
rm_whack_pte(struct Mapping *map,  size_t slot)
{
  if (IA32_UsingPAE) {
    size_t offset = (map->pa & COYOTOS_PAGE_ADDR_MASK);

    char *base = TRANSMAP_MAP(map->pa - offset, char *);

    IA32_PAE *pte = (IA32_PAE *)(base + offset);
    size_t maxpte = COYOTOS_PAGE_SIZE / sizeof (*pte);

    assert(slot < maxpte);
    assert(slot < map->userSlots);
    pte_invalidate((struct PTE *)&pte[slot]);

    TRANSMAP_UNMAP(base);
  } else {
    IA32_PTE *pte = TRANSMAP_MAP(map->pa, IA32_PTE *);
    size_t maxpte = COYOTOS_PAGE_SIZE / sizeof (*pte);

    assert(slot < maxpte);
    pte_invalidate((struct PTE *)&pte[slot]);

    TRANSMAP_UNMAP(pte);
  }

  /* Now that all of the entries are invalidated, flush them from the TLB */
  asid_t asid = 
    (map->level == KernMapping.level) ? map->asid : KernMapping.asid;
  global_tlb_flush(asid);

  /** @bug need to cross-call other CPUs to have them flush their
   * TLBs, but do we want to delay doing this until all of the revmap
   * entries are gone?
   */
}

#define DEBUGGER_SUPPORT
#ifdef DEBUGGER_SUPPORT
#define MAX_INDENT 40

static void
hwmap_print_indent(int indent)
{
  char spaces[MAX_INDENT + 1];
  int idx;
  for (idx = 0; idx < min(indent, MAX_INDENT); idx++)
    spaces[idx] = ' ';
  spaces[idx] = 0;
  printf("%s", spaces);
}

void
hwmap_dump_pte(uint32_t slot, IA32_PTE *pte, int indent)
{
  if (!pte->bits.SW2) {
    // not soft-valid;  nothing to print
    return;
  }
  hwmap_print_indent(indent);

  printf("0x%03x: fr:0x%llx %s%s%s%s%s%s%s%s%s%s%s%s\n",
	 slot,
	 (kpa_t)PTE_FRAME_TO_KPA(pte->bits.frameno),
	 pte->bits.V ? "VA " : "va ",
	 pte->bits.W ? "WR " : "wr ",
	 pte->bits.USER ? "USR " : "usr ",
	 pte->bits.PWT ? "WT " : "",
	 pte->bits.PCD ? "CD " : "",
	 pte->bits.ACC ? "ACC " : "acc ",
	 pte->bits.DIRTY ? "DTY " : "dty ",
	 pte->bits.PGSZ ? "LGPG " : "",
	 pte->bits.GLBL ? "GLBL " : "",
	 pte->bits.SW0 ? "SW0 " : "sw0 ",
	 pte->bits.SW1 ? "SW1 " : "sw1 ",
	 pte->bits.SW2 ? "SW2 " : "sw2 ");
}

void
hwmap_dump_pae(uint32_t slot, IA32_PAE *pte, int indent)
{
  if (!pte->bits.SW2) {
    // not soft-valid;  nothing to print
    return;
  }
  hwmap_print_indent(indent);

  printf("0x%03x: PA:0x%llx %s%s%s%s%s%s%s%s%s%s%s%s%s\n",
	 slot,
	 (kpa_t)PAE_FRAME_TO_KPA(pte->bits.frameno),
	 pte->bits.V ? "VA " : "va ",
	 pte->bits.W ? "WR " : "wr ",
	 pte->bits.USER ? "USR " : "usr ",
	 pte->bits.PWT ? "WT " : "",
	 pte->bits.PCD ? "CD " : "",
	 pte->bits.ACC ? "ACC " : "acc ",
	 pte->bits.DIRTY ? "DTY " : "dty ",
	 pte->bits.PGSZ ? "LGPG " : "",
	 pte->bits.GLBL ? "GLBL " : "glbl ",
	 pte->bits.SW0 ? "SW0 " : "sw0 ",
	 pte->bits.SW1 ? "SW1 " : "sw1 ",
	 pte->bits.SW2 ? "SW2 " : "sw2 ",
	 pte->bits.NX ? "NX " : (IA32_NXSupported ? "nx " : ""));
}

void
hwmap_dump_table(kpa_t kpa, int level, int indent)
{
  hwmap_print_indent(indent);
  printf("PageTable: kpa 0x%08llx, level %d\n", kpa, level);
  if (IA32_UsingPAE) {
    if (level == 2) {
      uintptr_t offset = kpa & COYOTOS_PAGE_ADDR_MASK;
      
      uintptr_t tbl = TRANSMAP_MAP(kpa - offset, uintptr_t);
      IA32_PAE *table = (IA32_PAE *)(tbl + offset);
      size_t idx;
      
      for (idx = 0; idx < PDPT_SIZE; idx++) {
	hwmap_dump_pae(idx, &table[idx], indent + 2);
	// if statement is a hack to avoid printing the kernel map
	if (table[idx].bits.V && idx < PDPT_SIZE - 1)
	  hwmap_dump_table(PAE_FRAME_TO_KPA(table[idx].bits.frameno),
			   level - 1, indent + 4);
      }
      TRANSMAP_UNMAP(tbl);
      return;
    }
      
    IA32_PAE *table = TRANSMAP_MAP(kpa, IA32_PAE *);
    size_t idx;

    for (idx = 0; idx < NPAE_PER_PAGE; idx++) {
      hwmap_dump_pae(idx, &table[idx], indent + 2);
      if (level == 1 && table[idx].bits.V)
	hwmap_dump_table(PAE_FRAME_TO_KPA(table[idx].bits.frameno),
			 level - 1, indent + 4);
    }

    TRANSMAP_UNMAP(table);
  } else {
    IA32_PTE *table = TRANSMAP_MAP(kpa, IA32_PTE *);
    size_t idx;

    for (idx = 0; idx < NPTE_PER_PAGE; idx++) {
      hwmap_dump_pte(idx, &table[idx], indent + 2);
      if (level == 1 && table[idx].bits.V)
	hwmap_dump_table(PTE_FRAME_TO_KPA(table[idx].bits.frameno),
			 level - 1, indent + 4);
    }
    TRANSMAP_UNMAP(table);
  }
}
#endif /* DEBUGGER_SUPPORT */
