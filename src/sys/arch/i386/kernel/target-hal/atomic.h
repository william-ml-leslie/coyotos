#ifndef I386_HAL_ATOMIC_H
#define I386_HAL_ATOMIC_H
/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Structure declarations for atomic words and their
 * manipulators. 
*/

typedef struct {
  uint32_t  w;
} TARGET_HAL_ATOMIC32_T;

/** @brief Atomic compare and swap.
 *
 * If current word value is @p oldval, replace with @p
 * newval. Regardless, return value of target word prior to
 * operation. */
static inline uint32_t 
compare_and_swap(TARGET_HAL_ATOMIC32_T *a, uint32_t oldval, uint32_t newval)
{
  uint32_t result;

  __asm__ __volatile__("lock cmpxchgl %1,%2"
		       : "=a" (result)
		       : "r" (newval), "m" (*((uint32_t *)(a))), "0" (oldval)
		       : "memory", "cc");
  return result;

}

/** @brief Atomic compare and swap for pointers */
static inline void *
compare_and_swap_ptr(void **ap, void *oldval, void *newval)
{
  void * result;

  __asm__ __volatile__("lock cmpxchgl %1,%2"
		       : "=a" (result)
		       : "r" (newval), "m" (*((uintptr_t *)(ap))), "0" (oldval)
		       : "memory", "cc");
  return result;

}

/** @brief Atomic word read. */
static inline uint32_t 
atomic_read(TARGET_HAL_ATOMIC32_T *a)
{
  return a->w;
}

/** @brief Atomic word write. */
static inline void
atomic_write(TARGET_HAL_ATOMIC32_T *a, uint32_t u)
{
  a->w = u;
}

/** @brief Atomic OR bits into word. */
static inline void
atomic_set_bits(TARGET_HAL_ATOMIC32_T *a, uint32_t mask)
{ 
  __asm__ __volatile__("lock orl %[mask],%[wval]"
		       : [wval] "+m" (*((uint32_t *)(a)))
		       : [mask] "r" (mask)
		       : "cc");
}

/** @brief Atomic AND bits into word. */
static inline void 
atomic_clear_bits(TARGET_HAL_ATOMIC32_T *a, uint32_t mask)
{
  __asm__ __volatile__("lock andl %[mask],%[wval]"
		       : [wval] "+m" (*((uint32_t *)(a)))
		       : [mask] "r" (~mask)
		       : "cc");
}

#define APTR_READ(ap)    ((ap).a_ptr)
#define APTR_SET(ap,val) ((ap).a_ptr = (val))

/* The assignments to __old and __new are present for the purpose of
   statically type checking the arguments. */
#define APTR_CAS(ap,old,new)					\
  ({ __typeof__((ap).a_ptr) __old = (old);			\
    __typeof__((ap).a_ptr) __new = (new);			\
    ((__typeof__((ap).a_ptr))					\
     compare_and_swap_ptr((void **)&(ap).a_ptr, __old, __new)); })

#endif /* I386_HAL_ATOMIC_H */
