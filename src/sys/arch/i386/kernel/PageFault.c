/*
 * Copyright (C) 2007, The EROS Group, LLC
 * Copyright (C) 2024, William ML Leslie
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Page Fault implementation.
 */

#include <stddef.h>
#include <kerninc/assert.h>
#include <kerninc/printf.h>
#include <kerninc/PhysMem.h>
#include <kerninc/Cache.h>
#include <kerninc/MemWalk.h>
#include <kerninc/GPT.h>
#include <kerninc/util.h>
#include <kerninc/Process.h>
#include <kerninc/Mapping.h>
#include <kerninc/Depend.h>
#include <kerninc/pstring.h>
#include "hwmap.h"
#include "kva.h"

#define DEBUG_PGFLT if (0)
#define DEBUG_TRANSMAP if (0)

/** @brief A description of a level of PageTables. */
typedef const struct PageTableLevel {
  /** @brief log_2 of the space covered by a table. */
  uint8_t l2table;

  /** @brief Page Table Level (used in pgtbl_get()) */
  uint8_t level;

  /** @brief log_2 of the space covered by a slot, or zero if this
   * level describes pages. 
   */
  uint8_t l2slot;
  /**< @brief hardware-supported restr in table slots */
  uint8_t slot_restr;

#ifdef NEED_KPA_TO_MAPPING
  /** @brief routine to convert a KPA @p kpa to a Mapping pointer at this
   * level.
   */
  Mapping *(*KPAToMapping)(kpa_t kpa);
#endif /* NEED_KPA_TO_MAPPING */

  /** @brief Get a kernel VA pointer to a particular PTE, identified
   * by @p table and @p slot.
   * 
   * When finished with the returned PTE, finish_pte() should be
   * called on the returned value.
   */
  pte_t *(*findPTE)(Mapping *table, size_t slot);

  /** @brief routine to read the current value of a PTE 
   *
   * If this returns true, @p pte is valid, and
   * @p content and @p restr are filled with the values from the PTE.
   */
  bool (*readPTE)(pte_t *pte,
		  kpa_t /*OUT*/ *content, uint8_t /*OUT*/ *restr);

  /** 
   * @brief Routine to install a canary value in @p pte, in preperation for
   * later use by installPTE.
   */
  void (*canaryPTE)(pte_t *);

  /** @brief Routine to install a PTE to a Page or Mapping in a table.
   *
   * @p pte is the PTE physical address, returned from find_pte().  @p
   * content is the physical address of the page table or page at the
   * next lower level.  @p restr are the restrictions the PTE should
   * have, and will already be masked by the <tt>slot_restr</tt>.  @p
   * isCapPage is set iff we are installing a PTE to a CapPage.
   */
  bool (*installPTE)(pte_t *pte, kpa_t content, uint8_t restr, bool isCapPage);

  /** @brief Release a PTE value we have been manipulating */
  void (*finishPTE)(pte_t *pte);

} PageTableLevel;

static pte_t *
mapPAE(Mapping *map, size_t slot)
{
  size_t offset = map->pa & COYOTOS_PAGE_ADDR_MASK;
  kpa_t base = map->pa - offset;

  uintptr_t vbase = TRANSMAP_MAP(base, uintptr_t);
  IA32_PAE *ptr = (IA32_PAE *)(vbase + offset);

  DEBUG_TRANSMAP
    printf("xmapped PA 0x%llx slot 0x%lx; va %x;  PAE's value is 0x%llx\n",
	 map->pa, slot, &ptr[slot], ptr[slot].value);
  return ((pte_t *)&ptr[slot]);
}

static void
unmapPAE(pte_t *pte)
{
  DEBUG_TRANSMAP printf("unmapping va 0x%x\n", pte);
  uintptr_t base = ((uintptr_t)pte) & ~COYOTOS_PAGE_ADDR_MASK;
  TRANSMAP_UNMAP((void *)base);
}

static bool
readPAE(pte_t *pte, kpa_t /*OUT*/ *content, uint8_t /*OUT*/ *restr)
{
  IA32_PAE val;
  memcpy(&val, pte, sizeof(val));

  if (!val.bits.SW2)
    return false;   /* not soft-valid, must have been invalidated */

  *content = PAE_FRAME_TO_KPA(val.bits.frameno);
  *restr = 
    (val.bits.W?   0            : CAP_RESTR_RO) |
    (val.bits.SW0? CAP_RESTR_WK : 0) |
    (val.bits.NX?  CAP_RESTR_NX : 0);
  return true;
}

static IA32_PAE PAE_canary = {
  {
    0,				/* Valid */
    1,				/* writable */
    0,				/* user-accessable */
    0,				/* write-through */
    0,				/* cache disable */
    0,				/* accessed */
    0,				/* dirty */
    0,				/* large page */
    0,				/* global */
    0,				/* SW0:  Weak */
    0,				/* SW1: unused */
    0,				/* SW2: soft-valid */
    0,				/* physical frame */
    0,				/* reserved: must be zero */
    0				/* No-execute */
  }
};
  
static void
canaryPAE(pte_t *pte)
{
  IA32_PAE *target = (IA32_PAE *)pte;

  memcpy(target, &PAE_canary, sizeof (*target));
}

static bool
installPAE(pte_t *pte, kpa_t content, uint8_t restr, bool isCapPage)
{
  IA32_PAE *target = (IA32_PAE *)pte;

  IA32_PAE val = {
    {
      (isCapPage) ? 0 : 1,		/* Valid */
      (restr & CAP_RESTR_RO)? 0 : 1,	/* writable */
      1,				/* user-accessable */
      (restr & CAP_RESTR_WT) ? 1 : 0,   /* write-through */
      (restr & CAP_RESTR_CD) ? 1 : 0,   /* cache disable */
      1,				/* accessed */
      1,				/* dirty */
      0,				/* large page */
      0,				/* global */
      (restr & CAP_RESTR_WK)? 1 : 0,    /* SW0:  Weak */
      0,				/* SW1: unused */
      1,				/* SW2: soft-valid */
      PAE_KPA_TO_FRAME(content),
      0,				/* reserved: must be zero */
      (restr & CAP_RESTR_NX)? 1 : 0	/* No-execute */
    }
  };

  DEBUG_PGFLT printf("Installing physaddr: 0x%llx restr: 0x%x capPage: %d in VA %x\n",
	 content, restr, isCapPage, pte);  
  
  /** @bug use CAS */
  if (memcmp(target, &PAE_canary, sizeof (PAE_canary)) != 0)
    return false;

  memcpy(pte, &val, sizeof(val));
  DEBUG_PGFLT printf("succeeded.  New value: 0x%08llx\n", val.value);

  return true;
}

static bool
installPAE_PDPE(pte_t *pte, kpa_t content, uint8_t restr, bool isCapPage)
{
  IA32_PAE *target = (IA32_PAE *)pte;

  IA32_PAE val = {
    {
      (isCapPage) ? 0 : 1,		/* Valid */
      0,				/* MBZ */
      0,				/* MBZ */
      0,				/* write-through */
      0,				/* cache disable */
      0,				/* MBZ */
      0,				/* MBZ */
      0,				/* MBZ */
      0,				/* MBZ */
      (restr & CAP_RESTR_WK)? 1 : 0,    /* SW0:  Weak */
      0,				/* SW1: unused */
      1,				/* SW2: soft-valid */
      PAE_KPA_TO_FRAME(content),
      0,				/* reserved: must be zero */
      (restr & CAP_RESTR_NX)? 1 : 0	/* No-execute */
    }
  };

  DEBUG_PGFLT 
    printf("Installing physaddr: 0x%llx restr: 0x%x capPage: %d in VA %x\n",
	 content, restr, isCapPage, pte);  
  
  /** @bug use CAS */
  if (memcmp(target, &PAE_canary, sizeof (PAE_canary)) != 0)
    return false;

  memcpy(pte, &val, sizeof(val));
  DEBUG_PGFLT printf("succeeded.  New value: 0x%08llx\n", val.value);

  return true;
}

static pte_t *
mapPTE(Mapping *map, size_t slot)
{
  size_t offset = map->pa & COYOTOS_PAGE_ADDR_MASK;
  kpa_t base = map->pa - offset;

  uintptr_t vbase = TRANSMAP_MAP(base, uintptr_t);
  IA32_PTE *ptr = (IA32_PTE *)(vbase + offset);

  return ((pte_t *)&ptr[slot]);
}

static void
unmapPTE(pte_t *pte)
{
  uintptr_t base = ((uintptr_t)pte) & ~COYOTOS_PAGE_ADDR_MASK;
  TRANSMAP_UNMAP((void *)base);
}

static bool
readPTE(pte_t *pte, kpa_t /*OUT*/ *content, uint8_t /*OUT*/ *restr)
{
  IA32_PTE val;
  memcpy(&val, pte, sizeof(val));

  if (!val.bits.SW2)
    return false;   /* not soft-valid, must have been invalidated */

  *content = PTE_FRAME_TO_KPA(val.bits.frameno);
  *restr = 
    (val.bits.W?   0            : CAP_RESTR_RO) |
    (val.bits.SW0? CAP_RESTR_WK : 0);

  return true;
}

static IA32_PTE PTE_canary = {
  {
    0,				/* Valid */
    1,				/* writable */
    0,				/* user-accessable */
    0,				/* write-through */
    0,				/* cache disable */
    0,				/* accessed */
    0,				/* dirty */
    0,				/* large page */
    0,				/* global */
    0,				/* SW0:  Weak */
    0,				/* SW1: unused */
    0,				/* SW2: soft-valid */
    0				/* physical frame */
  }
};

static void
canaryPTE(pte_t *pte)
{
  IA32_PTE *target = (IA32_PTE *)pte;
  
  memcpy(target, &PTE_canary, sizeof (*target));
}


static bool
installPTE(pte_t *pte, kpa_t content, uint8_t restr, bool isCapPage)
{
  IA32_PTE *target = (IA32_PTE *)pte;

  IA32_PTE val = {
    {
      (isCapPage) ? 0 : 1,		/* Valid */
      (restr & CAP_RESTR_RO)? 0 : 1,	/* writable */
      1,				/* user-accessable */
      (restr & CAP_RESTR_WT) ? 1 : 0,   /* write-through */
      (restr & CAP_RESTR_CD) ? 1 : 0,   /* cache disable */
      1,				/* accessed */
      1,				/* dirty */
      0,				/* large page */
      0,				/* global */
      (restr & CAP_RESTR_WK)? 1 : 0,    /* SW0:  Weak */
      0,				/* SW1: unused */
      1,				/* SW2: soft-valid */
      PTE_KPA_TO_FRAME(content)
    }
  };
    
  /** @bug use CAS */
  if (memcmp(target, &PTE_canary, sizeof (PTE_canary)) != 0)
    return false;

  memcpy(pte, &val, sizeof(val));
  return true;
}

const PageTableLevel paePtbl[] = {
  { 32, 2, 30, 0, 				      
    mapPAE, readPAE, canaryPAE, installPAE_PDPE, unmapPAE },
  { 30, 1, 21, 0,
    mapPAE, readPAE, canaryPAE, installPAE, unmapPAE },
  { 21, 0, 12, CAP_RESTR_RO|CAP_RESTR_NX|CAP_RESTR_CD|CAP_RESTR_WT,
    mapPAE, readPAE, canaryPAE, installPAE, unmapPAE },
  { 0 }
};

const PageTableLevel normPtbl[] = {
  { 32, 1, 22, 0,
    mapPTE, readPTE, canaryPTE, installPTE, unmapPTE },
  { 22, 0, 12, CAP_RESTR_RO|CAP_RESTR_CD|CAP_RESTR_WT,
    mapPTE, readPTE, canaryPTE, installPTE, unmapPTE },
  { 0 }
};

/** @brief Given a process @p p, a virtual address @p addr, and a set
 * of desired permissions, establish the necessary page table entries
 * to support those permissions.
 *
 * do_pageFault constructs hardware page table entries, allocating
 * page tables and page directories along the way.
 *
 * @bug do_pageFault() is currently called only from the page fault
 * path, so @p addr can be a uintptr_t. In the cross-space case it
 * may get called with a full 64-bit logical va, and we check for
 * values that may be larger than the hardware-expressable address
 * range. That will in turn induce a fast/slow cross-space issue.
 * This ties in with the cross-space mapping design, which is
 * pending and needs review.
 */
void
do_pageFault(Process *p, uintptr_t addr, 
	     bool wantWrite, 
	     bool wantExec,
	     bool wantCap)
{
  /** @bug The following assert is a sanity check. Later, when we do
   * the fast path, we may call this in the cross-space case, and p may
   * not equal current. Note that in that case we must not deliver
   * the fault unless the caller is willing to wait. 
   *
   * @bug The cross-space case must return a success or failure
   * indication. 
   */
  assert(p == MY_CPU(current));
  assert(mutex_isheld(&p->lohdr.hdr.lock));

  /* Save original fault address, in case of keeper invocation. */
  uintptr_t fault_va = addr;

  size_t restr_mask = 
    CAP_RESTR_RO | CAP_RESTR_WK | (IA32_NXSupported ? CAP_RESTR_NX : 0);
  size_t leaf_restr_mask = 
    CAP_RESTR_CD | CAP_RESTR_WT | restr_mask;

  MemWalkResults mwr;
  coyotos_Process_FC result = 
    memwalk(&p->state.addrSpace, addr, wantWrite, &mwr);

  if (addr >= KVA) {
    result = coyotos_Process_FC_InvalidDataReference;
    goto deliver_fault;
  }

  if (wantExec && (mwr.cum_restr & CAP_RESTR_NX)) {
    result = coyotos_Process_FC_NoExecute;
    goto deliver_fault;
  }

  if (result != 0)
    goto deliver_fault;

  if (!wantCap && (mwr.ents[mwr.count - 1].entry->hdr.ty == ot_CapPage)) {
    result = coyotos_Process_FC_DataAccessTypeError;
    goto deliver_fault;
  }

  MemWalkEntry *mwe = &mwr.ents[0];

  size_t restr = (mwe->restr & restr_mask);
  /* minl2 is the least l2g or l2v value seen so far. If any bit above
   * COYOTOS_HW_ADDRESS_BITS is set, we should not be generating a
   * hardware table. */
  size_t minl2 = min(COYOTOS_HW_ADDRESS_BITS, mwe->l2g);

  /* 
   * First, handle the Process top-level pointer, since it's special.
   * Unlike lower down levels, the target of the addrSpace capability
   * is *always* the producer of the top-level table.  This prevents
   * us from needing Depend entries that target Processes.
   */

  /** @bug Pending bug: we are in a race between us updating the page
   * table and somebody else reclaiming that page table. It is either
   * necessary to spinlock the mapping structure during update or to
   * explain very carefully why no such mutual exclusion is
   * required. Note that a spinlock of this form does not obviate the
   * canary, because PTE invalidates need to be strictly prompt. That
   * problem is orthogonal to the reclaim problem. */
  const PageTableLevel *curPT = IA32_UsingPAE? paePtbl : normPtbl;

  /** @bug Don't re-fetch curmap if we already have one. */
  Mapping *curmap = p->mapping;

  /** If we do not have a top-level Mapping, or if the the access
   * restrictions on the one that we have do not match the access
   * restrictions on the address space capability, then, we need to
   * re-fetch the map.
   *
   * It was surprising to me that considering only the permissions on
   * the address space capability itself is sufficient. The question
   * is: why do we not need to consider the cumulative permissions on
   * ALL capabilities that take part in the path prefix leading up to
   * the top-level page directory?
   *
   * The answer consists of two parts:
   *
   *  1. None of the later capabilities in that prefix path can
   *     increase the permissions established by the address space
   *     capability.
   *
   *  2. Each of the later capabilities in that prefix path has an
   *     associated depend entry that whacks @em every PTE in the top
   *     level page directory, so if the permissions on any of them
   *     are altered we will (somewhat draconianly) end up with the
   *     desired effect that all permissions expressed in the page
   *     tables are conservatively correct.
   *
   * Note that in the permissions change case, we will leave a stale
   * revmap entry that will eventually be recycled. That is harmless.
   */

  if (true || curmap == &KernMapping || (curmap->restr != restr)) {
    /* We don't have a top-level mapping table for this process yet. */

    /* The hardware top-level address space pointer has no
       restrictions, so they must all be pushed into the mapping
       table. */
    curmap = 
      pgtbl_get(mwe->entry, curPT->level, mwe->guard,
		~(((coyaddr_t)2 << (minl2 - 1)) - 1), restr);

    rm_install_process_mapping(curmap, p);
    p->mapping = curmap;
  }
 
  /* Iterate downwards across each page table level in the
     hierarchy. */
  for (;
       curPT->l2slot != 0;
       curPT++) {
    /** 
     * we've found the next mapping table and installed its pointer.
     * Now, we need to figure out the next slot, fill it with a canary
     * value, and install depend entries for any GPTs we walk through
     * on the way to the next mapping level.
     */
    
    size_t slot = addr >> curPT->l2slot;
    addr -= slot << curPT->l2slot;

    pte_t *pte = curPT->findPTE(curmap, slot);
    
    curPT->canaryPTE(pte);

    // Pass through the non-producing GPTs, so that we can find
    // the next producer. That will be the producer of the next page
    // table down. We will bail out if we reach the leaf page.
    for (;;) {
      /* Stop if this is the producer of the next page table.
       * Note that pages have mwe->l2v == 0, so we will stop when
       * we reach the last MemWalkEntry. */
      if (!mwe->window && 
	  (minl2 < curPT->l2slot || mwe->l2v < curPT->l2slot))
	break;

      if (mwe->slot != MEMWALK_SLOT_BACKGROUND) {
	// generate a depend entry for this GPT
	assert(mwe->entry->hdr.ty == ot_GPT);

	minl2 = min(minl2, mwe->l2v);

	DependEntry de;
	de.gpt = (GPT *)mwe->entry;
	de.map = curmap;
	de.slotMask = 1u << mwe->slot;
	de.slotBias = mwe->slot;
	de.l2slotSpan = min(minl2, curPT->l2table) - curPT->l2slot;
	de.basePTE = slot & ~((1u << de.l2slotSpan) - 1);

	depend_install(de);
      }

      /** @bug Need a depend entry for the MEMWALK_SLOT_BACKGROUND
       * slot case. */

      mwe++;
      restr |= (mwe->restr & restr_mask);
      minl2 = min(minl2, mwe->l2g);
    }

    /* We have either reached the leaf page or we now have in hand the
     * producer of the next page table down in mwe->entry (or
     * both). It is possible that a leaf page may act as producer for
     * a page table or a page directory, so both statements may be
     * true simultaneously. In that case, we will be iterating again
     * in the outer loop (over the page table levels).
     *
     * Depend entries have been built for non-producing GPTs. No
     * depend entries have been built for the producer. Entries
     * inserted into the RevMap will handle page table entries in the
     * parent hardware table (above us), if any. The next pass of the
     * outer loop will insert the depend entries for the (possibly
     * new) child page table (if any). */
    kpa_t target_pa;
    Mapping *newmap = 0;

    /** @issue Large-page support: We currently handle large Coyotos
     * pages using normal PTEs. If we want to generate large page
     * mappings at the hardware level, this code will have to be
     * updated to check for larger values of curPT->l2slot in
     * combination with an eligable large page size (by checking l2g
     * on the cap in mwe->l2g), and generate the appropriate large PTE
     * at the call to installPTE() below.
     */
    if (curPT->l2slot == COYOTOS_PAGE_ADDR_BITS) {
      /* We are processing the lowest level page table, so we are
       * about to build a PTE. */

      // the page, boss, the page!
      assert(mwe->entry->hdr.ty == ot_Page || 
	     mwe->entry->hdr.ty == ot_CapPage);

      /* Greater or equal because this might be a fractional mapping
	 into middle of large page established via smaller window. */
      // assert(mwe->entry->hdr.l2g >= curPT->l2slot);
      //assert(mwe->entry->l2g >= curPT->l2slot);

      assert(mwe->l2g >= curPT->l2slot);

      rm_install_pte_page((Page *)mwe->entry, curmap, slot);
      
      // This might be a frame named by a physical page, in which case
      // we need to incorporate the CAP_RESTR_CD and CAP_RESTER_WT
      // bits at this stage. Those bits *only* apply to leaf pages, so
      // we can add them here.
      restr |= (mwe->restr & leaf_restr_mask);

      // To install a writable mapping to a page, it must be marked dirty
      if (!(restr & (CAP_RESTR_RO|CAP_RESTR_WK)) && wantWrite) {
	/* Target page may be immutable. Is this the right result
	   code? */
	if (!obhdr_mark_dirty(&mwe->entry->hdr)) {
	  result = coyotos_Process_FC_AccessViolation;
	  goto deliver_fault;
	}
      } else {
	/* if the page isn't already dirty, make the PTE read-only. */
	if (!mwe->entry->hdr.dirty)
	  restr |= CAP_RESTR_RO;
      }

      /* Large pages: if l2g of the page is bigger than
       * COYOTOS_PAGE_ADDR_BITS, we need to include the correct
       * address bits in our target PA. mwe->remAddr is within the
       * range [0, 2^l2g-1], which is what we want, but we need to
       * strip out the page offset bits. While we have
       * a large page, we are building a normal-size PTE here, so the
       * page offset bits that we want to strip out are the
       * COYOTOS_PAGE_ADDR_MASK bits.
       */
      target_pa = 
	((Page *)mwe->entry)->pa + (mwe->remAddr & ~COYOTOS_PAGE_ADDR_MASK);
    } else {
      /* We are looking at a non-leaf hardware slot. Note that
       * mwe->entry @em may be a page that is serving as a degenerate
       * case producer of the next table down. */
      coyaddr_t guard = mwe->guard;

      // We know that this GPT is producing the next page table.
      if (minl2 > curPT->l2slot) {
	assert(mwe->l2v < curPT->l2slot);
	// This is a split case;  the GPT is producing multiple Page Table 
	// entries.
	guard = (mwe->slot << mwe->l2v) & ~((1 << curPT->l2slot) - 1);
      }
      else {
	/* clear the bits above the table size */
	guard &= ~(((coyaddr_t)1 << curPT->l2slot) - 1);
      }

      /* We need to be holding the mappingListLock until the RevMap
       * entry is installed. The following grab should do the job.
       */
      HoldInfo hi = mutex_grab(&Cache.mappingsLock);

      newmap = pgtbl_get(mwe->entry, curPT->level - 1, guard,
			 ~(((coyaddr_t)2 << (minl2 - 1)) - 1),
			 (restr & ~curPT->slot_restr));

      rm_install_pte_mapping(newmap, curmap, slot);

      mutex_release(hi);

      target_pa = newmap->pa;
    }
    /* attempt to install the PTE; if this fails, one of our depend entries
     * might have been whacked.  We must start over from scratch.
     */
    if (!curPT->installPTE(pte,
			   target_pa, 
			   (restr & curPT->slot_restr),
			   (mwe->entry->hdr.ty == ot_CapPage))) {
      curPT->finishPTE(pte);
      sched_restart_transaction();
    }
    curPT->finishPTE(pte);

    /* Clear the restrictions the pointer contains. */
    restr &= ~curPT->slot_restr;
    curmap = newmap;
  }

  /** @bug Why is this necessary? Shap thinks this is a PAE
   * requirement for AMD in case intervening TLB-cached levels had
   * insufficient permissions. The key issue is how the hardware
   * responds to permissions upgrade -- or rather, whether it
   * short-circuits the search based on upper levels.
   *
   * Don't do a global flush here. If the same mapping is present on
   * some other CPU, they will deal with the upgrade problem on their
   * own dime.
   *
   * Note that this is @em only required if an intermediate PTE had
   * to be upgraded at some point. Either we just did that upgrade
   * above or it was done on some other CPU and our TLB contains a
   * stale short-circuit value. A mere transition from invalid to
   * valid does not require a flush.
   *
   * All of that is nice, and don't try to optimize it here for now.
   */
  local_tlb_flushva(p->mapping->asid, fault_va);

  return;

 deliver_fault:

  /** @bug This should not be done in the cross-space case unless the
   * sender is willing to wait. */
  DEBUG_PGFLT
    printf("Fault va was 0x%P\n", fault_va);

  proc_deliver_memory_fault(p, result, fault_va, &mwr);
  /*  proc_TakeFault(MY_CPU(current), result, addr); */
}
