/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief BSP memory configuration for IA-32 multiboot platforms.
 */

#include <kerninc/printf.h>
#include <kerninc/PhysMem.h>
#include <kerninc/string.h>
#include <kerninc/pstring.h>
#include <kerninc/CommandLine.h>
#include <kerninc/ModInfo.h>
#include <kerninc/Cache.h>
#include <kerninc/util.h>
#include "../../kernel/hwmap.h"
#include "../../kernel/kva.h"

#include "MultiBoot.h"

/// @brief Recorded multiboot signature.
uint32_t multibootSignature;
/// @brief Recorded multiboot information pointer.
uint32_t multibootInfo;

extern uint8_t _mkimage_start[];
extern uint8_t _mkimage_end[];


static PmemClass 
mmap_to_class(uint32_t multibootType)
{
  switch(multibootType) {
  case 1: return pmc_RAM;
  case 2: return pmc_ROM;
  case 3: return pmc_NVRAM;
  case 4: return pmc_RAM;
  default: assert(false);
  }
}

static PmemUse 
mmap_to_use(uint32_t multibootType)
{
  switch(multibootType) {
  case 1: return pmu_AVAIL;
  case 2: return pmu_BIOS;
  case 3: return pmu_ACPI_NVS;
  case 4: return pmu_ACPI_DATA;
  default: assert(false);
  }
}

/** @brief Initialize physical memory.
 *
 * This routine is <em>extremely</em> multiboot-specific.
 *
 * @todo This routine is *supposed* to get the physical memory layout,
 * but I have just determined that grub under QEMU isn't providing all
 * of the information that I want to gather, so I need to check what
 * it does on real hardware. The issue is that I want the ROM
 * locations as well. Drat!
 * 
 * @todo The case !MBI_MMAP and MBI_VALID should only appear on
 * ancient legacy BIOS's that are no longer shipping. It is not clear
 * whether we should still be supporting this.
 */
static void
config_physical_memory(void)
{
  /* On entry, only the low 1G (0x40000000) is mapped! */

  if (multibootSignature != MULTIBOOT_BOOTLOADER_MAGIC)
    fatal("Not Multiboot: 0x%08x\n", multibootSignature);

  assert(multibootInfo < 0x40000000u);
  assert(multibootInfo + sizeof(struct MultibootInfo) < 0x40000000u);

  struct MultibootInfo *mbi = PTOKV(multibootInfo, struct MultibootInfo *);

  if (mbi->flags & MBI_MMAP) {
    // printf("Memory map valid: 0x%x %d\n", mbi->mmap_addr, mbi->mmap_length);

    assert(mbi->mmap_addr < 0x40000000u);
    assert(mbi->mmap_addr + mbi->mmap_length < 0x40000000u);

    uint32_t offset = 0;
    while (offset < mbi->mmap_length) {
      struct MultibootMMap *mmap = 
	PTOKV(mbi->mmap_addr + offset, struct MultibootMMap *);

      kpa_t base = mmap->base_addr_high;
      base <<= 32;
      base |= mmap->base_addr_low;

      kpa_t len = mmap->length_high;
      len <<= 32;
      len |= mmap->length_low;

      PmemClass cls = mmap_to_class(mmap->type);
      PmemUse   use = mmap_to_use(mmap->type);

      // printf("[%llx,%llx] multiboot type %x\n", base, base+len, mmap->type);

      pmem_AllocRegion(base, base+len, cls, use, pmc_descrip(cls));

      offset += mmap->size + 4;	/* size field is size of following bytes */
    }
  }
  else if (mbi->flags & MBI_MEMVALID) {
    // printf("General memory valid: %d %d\n", mbi->mem_lower, mbi->mem_upper);

    pmem_AllocRegion(0, mbi->mem_lower * 1024, pmc_RAM, pmu_AVAIL, pmc_descrip(pmc_RAM));
    pmem_AllocRegion(0x100000, mbi->mem_upper * 1024, pmc_RAM, pmu_AVAIL, pmc_descrip(pmc_RAM));
  }
  else
    fatal("Multiboot did not provide memory config\n");

  /*
   * Reserve some memory regions so that they do not get grabbed by
   * the heap backing storage allocator prematurely.
   */

  /* Reserve some additional regions: */
  pmem_AllocRegion(0, 1024, pmc_RAM, pmu_BIOS, "BIOS area");

#if MAX_NCPU > 1
  /* Reserve some additional regions: */
  pmem_AllocRegion(4096, 8192, pmc_RAM, pmu_SMP, "SMP AP boot");
#endif

  {
    /* We need to guard the EBDA region. Not all BIOS memory queries
     * report this region, with the consequence that multiboot itself
     * does not always identify it.
     *
     * If multiboot DOES define it, it will have shown up as
     * "reserved", and therefore will have been reported as a ROM BIOS
     * region. We will actually treat it as ROM, but it's a RAM
     * region.
     *
     * Unfortunately, some BIOS's report an overlap. For example, the
     * HP Pavilion a6030n reports an initial RAM region of [0,0x9f400],
     * a BIOS ROM region of [0x9f400,0x9fc00], and then we get here and
     * discover a claimed EBDA region of [0x9f000,0x9fc00]. Obviously
     * the various people who worked on that BIOS didn't talk to each
     * other. The problem is that we do not know who has it right, and
     * the EBDA may contain various bits of information that we will
     * need later for ACPI. The conservative solution is to ensure
     * that the EBDA gets reserved, which is what we do here.
     *
     * 16-bit segment base of EBDA appears at address 0x403. First
     * byte of the EBDA gives size of EBDA in kilobytes.
     */
    uint32_t ebda_base = (* PTOKV(0x40e, uint16_t *)) << 4;
    uint32_t ebda_size = (* PTOKV(ebda_base, uint8_t *));
    ebda_size *= 1024;
    uint32_t ebda_bound = ebda_base + ebda_size;

    while (ebda_base < ebda_bound) {
      PmemInfo *pmi = pmem_FindRegion(ebda_base);
      uint32_t bound = ebda_bound;
      if (pmi->bound < ebda_bound)
	bound = pmi->bound;

      pmem_AllocRegion(ebda_base, bound, pmc_RAM, pmu_BIOS,
		       "Extended BIOS Data Area");

      ebda_base = bound;
    }

    printf("EBDA = [0x%lx:%lx] (0x%lx)\n", ebda_base, ebda_bound, 
	   ebda_size);
  }
}

static bool 
module_option_isstring(const char *cmdline, const char *opt, const char *val)
{
  size_t len = strlen(opt);
  size_t vlen = strlen(val);

  if (cmdline == NULL)
    return false;

  for( ;*cmdline; cmdline++) {
    if (*cmdline != *opt)
      continue;

    if (memcmp(cmdline, opt, len) != 0)
      continue;

    if (cmdline[len] != '=')
      continue;

    cmdline += (len + 1);
    break;
  }

  if (*cmdline == 0)
    return false;

  if (memcmp(cmdline, val, vlen) != 0)
    return false;

  return (cmdline[vlen] == 0 || cmdline[vlen] == ' ');
}

/** @brief Protect multiboot-allocated memory areas from allocation.
 *
 * Record the physical memory locations of modules and command
 * line. This ensures that they are not allocated by the physical
 * memory allocator as we build the heap later.
 */
static void
process_multiboot_info(void)
{
  struct MultibootInfo *mbi = PTOKV(multibootInfo, struct MultibootInfo *);

  /* Copy in the multiboot command line */
  if (mbi->flags & MBI_CMDLINE) {
    /* The multiboot command line is null terminated, but we do not know
     * its length. We aren't prepared to accept more than
     * COMMAND_LINE_LIMIT bytes anyway, so the simple thing to do is
     * just copy COMMAND_LINE_LIMIT bytes starting at mbi-cmdline, and
     * then put a NUL on the end in case the command line supplied by
     * the boot loader was longer. */
    memcpy_ptov(CommandLine, mbi->cmdline, COMMAND_LINE_LIMIT);
    CommandLine[COMMAND_LINE_LIMIT - 1] = 0;
  }

  if (mbi->flags & MBI_MODS) {
    size_t nmods = mbi->mods_count;
  
    const struct MultibootModuleInfo *mmi = 
      PTOKV(mbi->mods_addr, const struct MultibootModuleInfo *);
  
    /* First pass: protect the existing modules */
    for (size_t i = 0; i < nmods; i++, mmi++) {
      const char *cmdline = PTOKV(mmi->string, const char *);

      if (module_option_isstring(cmdline, "type", "load"))
	module_register(mmi->mod_start, mmi->mod_end, "SysImage");
    }

  }
}

void
bsp_mem_config()
{
  printf("IA32_UsingPAE: %s\n", IA32_UsingPAE ? "yes" : "no");

  pmem_init(0, IA32_UsingPAE ? PAE_PADDR_BOUND : PTE_PADDR_BOUND);

  /* Need to get the physical memory map before we do anything else. */
  config_physical_memory();

  if (_mkimage_start != _mkimage_end)
    module_register(KVTOP(_mkimage_start), KVTOP(_mkimage_end),
		    "Emb. Image");
}

void
bsp_protect_modules()
{
  /* Make sure that we don't overwrite the loaded modules during cache
   * initialization.
   */
  process_multiboot_info();

  module_relocate();
}
