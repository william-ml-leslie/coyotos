/*
 * Copyright (C) 2021, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <stdbool.h>
#include <hal/kerntypes.h>
#include <hal/irq.h>
#include <kerninc/ctype.h>
#include <coyotos/i386/io.h>

// IF we want serial debugging, we're going to assume it's on the
// standard port and avoid going through A Seepy Eye.  I suspect that
// those with a non-standard configuration will know to hand-tweak
// this code, anyway. - WL
#define SERIAL_PORT 0x3f8
 
#define LINE_CONTROL SERIAL_PORT + 3
#define REG_DATA SERIAL_PORT
#define INTERRUPT_ENABLE SERIAL_PORT + 1
#define BAUD_DIV_L SERIAL_PORT
#define BAUD_DIV_H SERIAL_PORT + 1
#define FIFO_CONTROL SERIAL_PORT + 2
#define MODEM_CONTROL SERIAL_PORT + 4
#define LINE_STATUS SERIAL_PORT + 5
#define TRANSMIT_HOLDING_REGISTER_EMPTY 0x20

void write_serial(char a) {
  while ((inb(LINE_STATUS) & TRANSMIT_HOLDING_REGISTER_EMPTY) == 0);

  outb(a, SERIAL_PORT);
}

int init_serial() {
  // I'ma let you finish, but
  outb(0, INTERRUPT_ENABLE);

  // Enable baud rate divisor configuration
  outb(0x80, LINE_CONTROL);
  // for 115200 bps, set the baud rate divisor to 1.  I think qemu
  // actually ignores this.
  outb(1, BAUD_DIV_L);
  outb(0, BAUD_DIV_H);
  // 8 bits, no parity, one stop bit
  outb(3, LINE_CONTROL);
  // please buffer up to 14 bytes of input.
  //
  // TBF we aren't *expecting* any input at this point, and maybe
  // when we do, we won't want to buffer quite so many.
  outb(0xC7, FIFO_CONTROL);

  // Set in loopback mode, test the serial chip
  outb(0x1E, MODEM_CONTROL);

  // Echo me a -C
  outb(0xBD, SERIAL_PORT + 0);

  if(inb(SERIAL_PORT + 0) != 0xBD) {
    return 1;
  }
  
  // out2 + request to send + data terminal ready
  outb(0xB, MODEM_CONTROL);

  return 0;
}
