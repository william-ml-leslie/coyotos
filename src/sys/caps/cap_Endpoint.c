/*
 * Copyright (C) 2007, The EROS Group, LLC
 * Copyright (C) 2024, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <kerninc/capability.h>
#include <kerninc/InvParam.h>
#include <kerninc/Endpoint.h>
#include <kerninc/assert.h>
#include <kerninc/Queue.h>
#include <hal/syscall.h>
#include <coyotos/syscall.h>
#include <idl/coyotos/Endpoint.h>

extern void cap_Cap(InvParam_t* iParam);

void cap_Endpoint(InvParam_t *iParam)
{
  uintptr_t opCode = iParam->opCode;

  switch(opCode) {
  case OC_coyotos_Cap_getType:	/* Must override. */
    {
      INV_REQUIRE_ARGS(iParam, 0);

      sched_commit_point();
      InvTypeMessage(iParam, IKT_coyotos_Endpoint);
      return;
    }

  case OC_coyotos_Endpoint_setRecipient:
    {
      INV_REQUIRE_ARGS(iParam, 1);

      /* Before changing recipient slot, must wake up everyone
       * currently on the rcvWaitQ queue of the current target process,
       * assuming there is one. */
      Endpoint *ep = (Endpoint *)iParam->iCap.cap->u2.prepObj.target;

      Process *newRecipient =
        (Process *)cap_prepAndLock(iParam->srcCap[1].cap, 0);
      Process *oldRecipient =
         (Process *)cap_prepAndLock(&ep->state.recipient, 0);

      require(obhdr_mark_dirty(&ep->hdr));

      sched_commit_point();

      if ((iParam->srcCap[1].cap->type != ct_Process) &&
	  (iParam->srcCap[1].cap->type != ct_Null)) {
	InvErrorMessage(iParam, RC_coyotos_Cap_RequestError);
	return;
      }

      // if we're on a sending list, we need to clear this.
      if (oldRecipient != NULL && ep->state.sending == 1) {
        epq_recipient_clear(oldRecipient, ep);
      }

      cap_set(&ep->state.recipient, iParam->srcCap[1].cap);

      // if we have any senders, place our endpoint on the senders heaps.
      if (newRecipient != NULL && !queue_isEmpty(&ep->queue)) {
        epq_recipient_set(newRecipient, ep);
      }

      iParam->opw[0] = InvResult(iParam, 0);
      return;
    }

  case OC_coyotos_Endpoint_setPayloadMatch:
    {
      INV_REQUIRE_ARGS(iParam, 0);

      Endpoint *ep = (Endpoint *)iParam->iCap.cap->u2.prepObj.target;
      if (ep->state.pm == 0)
	obhdr_mark_dirty(&ep->hdr);
      cap_prepare(&ep->state.recipient);

      sched_commit_point();

      if (ep->state.pm == 0) {
        /* Should we do anything about processes already sending to
         * this endpoint that aren't going to match the payload?
         *
         * Shap's previous note here said that it's not necessary for
         * correctness, and that these senders will cleared naturally
         * during message processing.  Their implementation woke up
         * these senders eagerly because it allows them to make
         * progress sooner.
         *
         * It might still be worth doing that, but we have no
         * mechanism for this.  It'd be O(n) to go through all of the
         * queued senders, check their protected payload, and wake
         * them if there's no match; and we don't want to perform any
         * unbounded operations in a single kernel transaction.
         */

	ep->state.pm = 1;
      }

      iParam->opw[0] = InvResult(iParam, 0);
      return;
    }

  case OC_coyotos_Endpoint_setEndpointID:
    {
      uint64_t epID = get_iparam64(iParam);
      INV_REQUIRE_ARGS(iParam, 0);

      Endpoint *ep = (Endpoint *)iParam->iCap.cap->u2.prepObj.target;

      if (epID != ep->state.endpointID)
	obhdr_mark_dirty(&ep->hdr);

      sched_commit_point();

      /* Changing the endpoint ID does NOT require waking
       * senders. They will re-acquire the endpoint ID when they
       * actually make it through the endpoint during invocation.
       */

      if (epID != ep->state.endpointID)
	ep->state.endpointID = epID;

      iParam->opw[0] = InvResult(iParam, 0);
      return;
    }

  case OC_coyotos_Endpoint_getEndpointID:
    {
      INV_REQUIRE_ARGS(iParam, 0);

      Endpoint *ep = (Endpoint *)iParam->iCap.cap->u2.prepObj.target;

      sched_commit_point();

      put_oparam64(iParam, ep->state.endpointID);
      iParam->opw[0] = InvResult(iParam, 0);
      return;
    }

  case OC_coyotos_Endpoint_makeEntryCap:
  case OC_coyotos_Endpoint_makeAppNotifier:
    {
      uint32_t pp = get_iparam32(iParam);

      INV_REQUIRE_ARGS(iParam, 0);

      sched_commit_point();

      cap_set(&iParam->srcCap[0].theCap, iParam->iCap.cap);
      iParam->srcCap[0].theCap.type = 
	(opCode == OC_coyotos_Endpoint_makeEntryCap) ? ct_Entry : ct_AppNotice;
      iParam->srcCap[0].theCap.u1.protPayload = pp;
      
      iParam->opw[0] = InvResult(iParam, 1);
      return;
    }

  default:
    cap_Cap(iParam);
    break;
  }
}
