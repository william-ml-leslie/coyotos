/*
 * Copyright (C) 2005, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Kernel heap management.
 *
 * The kernel heap is used for all data structures that require
 * virtually contiguous mappings. Allocation from the heap does
 * <em>not</em> guarantee physically contiguous mappings. If you need
 * that, you need to talk to the physical memory allocation logic.
 * The Coyotos kernel makes much heavier use of the heap than the EROS
 * kernel did, though it still does so primarily at initialization
 * time. Once the system is initialized, the heap will be used only
 * when device drivers are advising the kernel of new device memory
 * regions. Even in that case, the heap is used only for creation of
 * the per-page frame structures for that memory.
 *
 * Bootstrapping the kernel heap is a little delicate. In abstract, we
 * would prefer to allocate frames for the heap from page
 * space. Unfortunately, we would also like to use the heap fairly
 * early during the bootstrap code before page space has been
 * allocated.
 *
 * In general, the rule is: <b>a heap frame is never returned</b>. To
 * ensure that this rule can be satisfied, we never allocate heap
 * frames from dismountable memory.
 *
 * @todo If we ever need to support machine(s) with hot-plug memory
 * (e.g. Tandem) we will need a mechanism to do an exchange of frames
 * before removing the memory from the bus. In abstract, this is not
 * difficult, but I haven't implemented it.
 *
 */

#include <stddef.h>
#include <hal/kerntypes.h>
#include <kerninc/assert.h>
#include <kerninc/util.h>
#include <kerninc/printf.h>
#include <kerninc/string.h>
#include <kerninc/util.h>
#include <kerninc/PhysMem.h>
#include <kerninc/Cache.h>
#include <kerninc/mutex.h>
#include <hal/vm.h>

extern void _end();

#define DEBUG_HEAP if (0)

/* Local Variables: */
/* comment-column:30 */
/* End: */

static kva_t heap_start = (kva_t) &_end; /**<! @brief starting VA of heap */
static kva_t heap_end = (kva_t) &_end; /**<! @brief current end of heap */

/**<! @brief Heap is backed to here. */
static kva_t heap_backed = (kva_t) &_end;

/** @brief Hard upper bound for heap lest we collide.
 *
 * If this is non-zero, heap has been initialized.
 */
static kva_t heap_hard_limit = 0;

void
heap_init(kva_t start, kva_t backedTo, kva_t limit)
{
  /* Initialization of heap_start, heap_backed, heap_end is handled
     in the machine-specific MapKernel code, which is called very
     early in mach_BootInit(). */

  heap_start = start;
  heap_end = start;
  heap_backed = align_up(backedTo, COYOTOS_PAGE_SIZE);
  heap_hard_limit = limit;

  DEBUG_HEAP 
    printf("heap_start, heap_end = 0x%08x, 0x%08x\n", heap_start, heap_end);
}

/** @brief Mutex protecting heap state.
 *
 * @bug should this be a spinlock? 
 */
static mutex_t heap_mutex;

/**
 * @bug This implementation of malloc() is a placeholder, since free()
 * clearly would not work correcly without maintaining some sort of
 * record of what was allocated where.
 */
static void *
malloc(size_t nBytes)
{
  HoldInfo hi = mutex_grab(&heap_mutex);

  assert(heap_hard_limit);

  DEBUG_HEAP
    printf("malloc: heap start=0x%08x end=0x%08x backed=0x%08x limit=0x%08x alloc=%d\n",
	   heap_start, heap_end, heap_backed, heap_hard_limit, nBytes);

  // The address we return must be aligned at a pointer boundary. On
  // all currently forseen implementations this is the worst alignment
  // that the kernel must deal with.
  kva_t va = align_up(heap_end, sizeof(kva_t));

  // Heap is now pre-sized. If this assert fails, you screwed up the
  // pre-size logic.
  assert(va + nBytes <= heap_hard_limit);

  heap_end = va + nBytes;

  DEBUG_HEAP
    printf("malloc: allocated %d bytes at 0x%08x\n", nBytes, va);

  mutex_release(hi);

  return (void *) va;
}

void *
calloc(size_t nBytes, size_t nElem)
{
  void *vp = malloc(nBytes * nElem);
  memset(vp, 0, nBytes * nElem);
  return vp;
}

void *
construct(size_t nBytes, size_t nElem, void (*initproc)(void *))
{
  void *vp = calloc(nBytes, nElem);

  unsigned char *ucp = vp;

  for (size_t i = 0; i < nElem; i++)
    if (initproc) initproc(ucp + (i * nBytes));

  return vp;
}
