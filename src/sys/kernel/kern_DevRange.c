/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Implementation of device ranges and device pages.
 */

#include <hal/transmap.h>
#include <hal/console.h>

#include <kerninc/assert.h>
#include <kerninc/printf.h>
#include <kerninc/util.h>
#include <kerninc/malloc.h>
#include <kerninc/DevRange.h>
#include <kerninc/ObjectHeader.h>
#include <kerninc/Sched.h>
#include <kerninc/ObjectHash.h>
#include <kerninc/FreeList.h>
#include <kerninc/PhysMem.h>

#include <idl/coyotos/Range.h>
#include <idl/coyotos/DevRangeCtl.h>
#include <idl/coyotos/Cap.h>

DevRangeMgr_s DevRangeMgr = {
  .range = NULL,
  .nDevRange = NUM_DEVRANGE,
  .nPagePerRange = PAGES_PER_DEVRANGE,
  .freePages = STATIC_AGELIST_INIT(DevRangeMgr.freePages),
  .activePages = STATIC_AGELIST_INIT(DevRangeMgr.activePages)
};

/** @brief Lock that covers all of the device range and device page
 * logic.
 *
 * Operations on these are sufficiently rare that we probably don't
 * need to do fine-grain locking.
 */
static mutex_t DevRangeLock = MUTEX_INIT;

size_t 
devrange_compute_required_heap()
{
  size_t sz = 0;

  sz += alloc_requires(DevRangeMgr.nDevRange, sizeof(DevRange));
  sz += alloc_requires(DevRangeMgr.nDevRange*DevRangeMgr.nPagePerRange, sizeof(Page));

  return sz;
}

/* Currently just a placeholder implementation */
void 
devrange_init()
{
  DevRangeMgr.range = CALLOC(DevRange, DevRangeMgr.nDevRange);

  for (size_t i = 0; i < DevRangeMgr.nDevRange; i++) {
    DevRangeMgr.range[i].page = CALLOC(Page, DevRangeMgr.nPagePerRange);
    DevRange *devRange = &DevRangeMgr.range[i];
    for (size_t pg = 0; pg < DevRangeMgr.nPagePerRange; pg++) {
      devRange->page[pg].mhdr.hdr.ty = ot_Page;
      devRange->page[pg].mhdr.hdr.current = 1;
      ageable_init(&devRange->page[pg].mhdr.hdr.age);
      agelist_addFront(&DevRangeMgr.freePages, &devRange->page[pg]);
    }
  }
}

uint64_t
devrange_setup(size_t ndx, kpa_t base, kpa_t bound, uint32_t l2g)
{
  (void) mutex_grab(&DevRangeLock);

  sched_commit_point();

  if (ndx >= DevRangeMgr.nDevRange)
    return RC_coyotos_Cap_RequestError;

  if (bound <= base)
    return RC_coyotos_Cap_RequestError;

  if (pmem_OverlapsAllocatedRegion(base, bound))
    return RC_coyotos_Cap_NoAccess;

  if (l2g >= COYOTOS_HW_ADDRESS_BITS || l2g < COYOTOS_PAGE_ADDR_BITS)
    return RC_coyotos_Cap_RequestError;

  kpa_t mask = (((kpa_t)1u) << l2g) - 1;

  if ((base & mask) || (bound & mask))
    return RC_coyotos_Cap_RequestError;
      
  DevRange *devRange = &DevRangeMgr.range[ndx];

  if (devRange->base == base && devRange->bound == bound
      && devRange->l2g == l2g)
    return RC_coyotos_Cap_OK;

  if (devRange->l2g != 0)
    return RC_coyotos_Cap_NoAccess;

  devRange->base = base;
  devRange->bound = bound;
  devRange->l2g = l2g;

  return RC_coyotos_Cap_OK;
}

uint64_t
devrange_destroy(size_t ndx)
{
  fatal("Not implemented\n");
}

Page *
devpage_lookup(oid_t oid, bool andCreate, HoldInfo *ohi)
{
  if (oid < coyotos_Range_devOidStart)
    return NULL;

  kpa_t pa = (oid - coyotos_Range_devOidStart) * COYOTOS_PAGE_SIZE;
 
  HoldInfo range_hi = mutex_grab(&DevRangeLock);

  DevRange *devRange = NULL;
  Page *thePage = NULL;
  HoldInfo pg_hi;

  // Find the containing range, if any.
  for (size_t ndx = 0; ndx < DevRangeMgr.nDevRange; ndx++) {
    if (DevRangeMgr.range[ndx].base <= pa && 
	pa < DevRangeMgr.range[ndx].bound) {
      devRange = &DevRangeMgr.range[ndx];
      break;
    }
  }

  if (!devRange)
    goto done;

  if (pa % (1u << devRange->l2g))
    goto done;			/* no matching page */

  Page *freePage = NULL;

  // Find the desired page, if any.
  for (size_t ndx = 0; ndx < DevRangeMgr.nPagePerRange; ndx++) {
    Page *pg = &devRange->page[ndx];
    if (pg->mhdr.hdr.age.list != &DevRangeMgr.activePages) {
      if (!freePage)
	freePage = pg;
      continue;
    }

    if (pg->mhdr.hdr.oid == oid) {
      thePage = pg;
      pg_hi = mutex_grab(&thePage->mhdr.hdr.lock);
      goto done;
    }
  }

  if (freePage && andCreate) {
    thePage = freePage;
    pg_hi = mutex_grab(&thePage->mhdr.hdr.lock);
    assert(thePage->mhdr.hdr.ty == ot_Page);
    assert(thePage->mhdr.hdr.allocCount == 0);
    assert(thePage->mhdr.hdr.current == 1);
    thePage->mhdr.hdr.oid = oid;
    thePage->pa = pa;
    thePage->l2g = devRange->l2g;

    agelist_remove(&DevRangeMgr.freePages, thePage);
    agelist_addFront(&DevRangeMgr.activePages, thePage);
  }

 done:
  if (thePage)
    if (ohi) *ohi = pg_hi;
  mutex_release(range_hi);
  return thePage;
}

void 
devpage_destroy(Page* pg)
{
  assert(mutex_isheld(&pg->mhdr.hdr.lock));

  if (pg->mhdr.hdr.age.list != &DevRangeMgr.activePages)
    return;

  HoldInfo hi = mutex_grab(&DevRangeLock);

  obhdr_invalidate(&pg->mhdr.hdr);

  agelist_remove(&DevRangeMgr.activePages, pg);
  agelist_addFront(&DevRangeMgr.freePages, pg);

  mutex_release(hi);
}
