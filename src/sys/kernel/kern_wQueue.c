/*
 * Copyright (C) 2024, William ML Leslie.
 *
 * This file is part of the wleslie Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Queue management
 *
 * Another cut at queue functionality.
 *
 * A process or continuation may be queued because:
 * - it is sending to an endpoint that has no target.
 * - It is sending to a process or recv buffer not ready to receive.
 * - It is using an object that is not prepared.
 * - It is waiting for time.
 *
 * Regardless of which queue it may be on, there are some operations we need to
 * support: append, popleft, remove.
 *
 * We should also be aware that we could be appending to a queue whose entries
 * are not all resident.  We will get to the "sleeping" mechanism at a later
 * point, but the idea is that if one of the senders is reclaimed, it should be
 * taken from back of the queue, but not later than anything that hasn't been
 * checkpointed.  In this way, queues can be reduced in a way that we can
 * reconstitute as necessary.
 *
 * We want to effectively move the last clean process on the queue into the
 * sleeping list, and then the one before that, etc.
 *
 * Runnable processes could also be reclaimed if necessary, with the ability to
 * prepare processes in the schedule as we work through the queue.  I think the
 * preference would be to deprepare processes attempting to send first,
 * however.
 *
 * Adding an endpoint with a list of senders also requires care, in that
 * tickets will be out of order.  The persisted form of the process should
 * specify the target process and ticket, so that it can be invalidated on load
 * if necessary.
 */

#include <kerninc/Queue.h>
#include <kerninc/LinkedObjectHeader.h>
#include <kerninc/Process.h>
#include <kerninc/Endpoint.h>
#include <sys/tree.h>

#define MAX(x, y) (x > y ? x : y)
#define CMP(x, y) (x > y ? 1 : -1)

int
endpoint_compare_ticket(struct Endpoint *x, struct Endpoint *y) {
  uint64_t x_ticket = MAX(x->first_ticket, x->min_ticket);
  uint64_t y_ticket = MAX(y->first_ticket, y->min_ticket);
  if (x_ticket != y_ticket) {
    return CMP(x_ticket, y_ticket);
  }
  // otherwise, fall back to object id.
  if (x->hdr.oid != y->hdr.oid) {
    return CMP(x->hdr.oid, y->hdr.oid);
  }
  return 0;
}

int
endpoint_compare_endpointID(struct Endpoint *x, struct Endpoint *y) {
  if (x->state.endpointID != y->state.endpointID) {
    return CMP(x->state.endpointID, y->state.endpointID);
  }
  return endpoint_compare_ticket(x, y);
}

RB_PROTOTYPE_STATIC(ep_seq, Endpoint, senders_seq, endpoint_compare_ticket);
RB_PROTOTYPE_STATIC(ep_id, Endpoint, senders_epid, endpoint_compare_endpointID);
RB_GENERATE_STATIC(ep_seq, Endpoint, senders_seq, endpoint_compare_ticket);
RB_GENERATE_STATIC(ep_id, Endpoint, senders_epid, endpoint_compare_endpointID);

/** @brief Is some of the queue not in memory? */
bool
q_isPartial(Queue_t *q) {
  return q->next_sleeping.type != ct_Null || q->tail.next != NULL;
}

/** @brief Is the queue completely empty? */
bool
q_isEmpty(Queue_t *q) {
  return q->head.next == NULL &&
    q->tail.next == NULL &&
    q->next_sleeping.type != ct_Null;
}

/** @brief Is the next item in the queue stale? */
bool
q_needsFill(Queue_t *q) {
  return q->head.next == NULL && q->next_sleeping.type != ct_Null;
}

/** @brief What is the next item in the queue that needs to be prepared? */
capability
q_getFirstPartial(Queue_t *q) {
  return q->next_sleeping;
}

static LinkedObjectHeader *
q_popfront(Queue_t *q) {
  LinkedObjectHeader *result = q->head.next;
  if (result != NULL) {
    q->head.next = result->link.next;
    if (q->head.next == NULL) {
      q->head.prev = NULL;
    }
  }
  return result;
}

static void
q_append(Queue_t *q, LinkedObjectHeader *item) {
  item->link.next = NULL;
  if (q_isPartial(q)) {
    item->link.prev = q->tail.prev;
    q->tail.prev = item;
    if (q->tail.next == NULL) {
      q->tail.next = item;
    }
  } else {
    item->link.prev = q->head.prev;
    q->head.prev = item;
    if (q->head.next == NULL) {
      q->head.next = item;
    }
  }
}

static void
q_prepend(Queue_t *q, LinkedObjectHeader *item) {
  item->link.prev = NULL;
  item->link.next = q->head.next;
  q->head.next = item;
}

static void
q_remove(Queue_t *q, LinkedObjectHeader *item) {
  LinkedObjectHeader *prev = item->link.prev;
  LinkedObjectHeader *next = item->link.next;
  if (prev != NULL) {
    prev->link.next = next;
  } else if (q->head.next == item) {
    q->head.next = next;
  } else if (q->tail.next == item) {
    q->tail.next = next;
  }
  if (next != NULL) {
    next->link.prev = prev;
  } else if (q->head.prev == item) {
    q->head.prev = prev;
  } else if (q->tail.prev == item) {
    q->tail.prev = prev;
  }
  item->link.prev = NULL;
  item->link.next = NULL;
}

// or... no.
/* static void */
/* q_move(Queue_t *source, Queue_t *into) { */
/*   if (q_isPartial(source)) { */
/*     bug("No support for moving unprepared objects"); */
/*   } */
/*   while (!q_isEmpty(source)) { */
/*     LinkedObjectHeader *item = q_popfront(source); */
/*     q_append(into, item); */
/*   } */
/* } */

LinkedObjectHeader *
queue_popfront(Queue_t *q) {
  SpinHoldInfo hi = spinlock_grab(&q->qLock);

  LinkedObjectHeader *result = q_popfront(q);
  if (result != NULL) {
    result->onQ = NULL;
  }
  spinlock_release(hi);
  return result;
}

// XXX should onQ just be a Queue or an ObjectHeader?
//
// I like the idea that it could name a process send queue somehow, but maybe
// that's just silly.

void
queue_append(Queue_t *q, LinkedObjectHeader *item) {
  SpinHoldInfo hi = spinlock_grab(&q->qLock);
  q_append(q, item);
  item->onQ = q;
  spinlock_release(hi);
}

void
queue_prepend(Queue_t *q, LinkedObjectHeader *item) {
  SpinHoldInfo hi = spinlock_grab(&q->qLock);
  q_prepend(q, item);
  item->onQ = q;
  spinlock_release(hi);
}

void
queue_remove(Queue_t *q, LinkedObjectHeader *item) {
  SpinHoldInfo hi = spinlock_grab(&q->qLock);
  q_remove(q, item);
  item->onQ = NULL;
  spinlock_release(hi);
}

// in all current usages (kern_(CPU|IRQ|Interval)), `into` is the
// CPU-local runnable queue and taking the target lock can't fail.
//
// this function does not handle non-resident waiters; it should go
// away before it is required to.
bool
queue_move(Queue_t *q, Queue_t *into) {
  SpinHoldInfo shi, ihi;

  // if we don't already hold the lock on the source queue, take it.
  if (!spinlock_isheld(&q->qLock)) {
    shi = spinlock_grab(&q->qLock);
  } else {
    shi.hi.lockPtr = NULL;
  }
  ihi = spinlock_grab(&into->qLock);

  while (1) {
    LinkedObjectHeader *item = q_popfront(q);
    // TODO: handle non-resident waiters.
    if (item == NULL) {
      spinlock_release(ihi);
      if (shi.hi.lockPtr != NULL) {
        spinlock_release(shi);
      }
      return true;
    }
    item->onQ = into;
    q_append(into, item);
  }
}

bool
queue_isEmpty(Queue_t *q) {
  SpinHoldInfo shi = spinlock_grab(&q->qLock);
  bool result = q_isEmpty(q);
  spinlock_release(shi);
  return result;
}

Endpoint *
ep_peek(Process *p) {
  return RB_MIN(ep_seq, &p->senders_seq);
}

Endpoint *
ep_peekByID(Process *p, uint64_t epid) {
  Endpoint needle = { 0 };
  needle.first_ticket = 0;
  needle.state.endpointID = epid;
  Endpoint *next = RB_NFIND(ep_id, &p->senders_epid, &needle);
  if (next == NULL) {
    next = RB_MAX(ep_id, &p->senders_epid);
  } else {
    next = RB_PREV(ep_id, &p->senders_epid, next);
  }
  if (next == NULL || next->state.endpointID != epid) {
    return NULL;
  }
  return next;
}

void
ep_remove(Process *p, Endpoint *ep) {
  RB_REMOVE(ep_seq, &p->senders_seq, ep);
  RB_REMOVE(ep_id, &p->senders_epid, ep);
}

/** @brief sort the endpoint heaps after a change of waiting senders. */
void
ep_shuffle(Process *p, Endpoint *ep) {
  SpinHoldInfo hi = spinlock_grab(&ep->queue.qLock);
  if (q_isEmpty(&ep->queue)) {
    ep->first_ticket = 0;
    ep->state.sending = 0;
    spinlock_release(hi);
    RB_REMOVE(ep_seq, &p->senders_seq, ep);
    RB_REMOVE(ep_id, &p->senders_epid, ep);
    return;
  }
  LinkedObjectHeader *first = ep->queue.head.next;
  if (first == NULL) {
    ep->first_ticket = ep->queue.ticket_sleeping;
  } else {
    ep->first_ticket = first->my_ticket;
  }
  spinlock_release(hi);

  Endpoint *next_seq = RB_NEXT(ep_seq, &p->senders_seq, ep);
  Endpoint *next_epid = RB_NEXT(ep_id, &p->senders_epid, ep);

  // if our ticket is out of order, reshuffle our seq.
  if (next_seq != NULL &&
      next_seq->first_ticket > ep->first_ticket) {
    RB_REMOVE(ep_seq, &p->senders_seq, ep);
    RB_INSERT(ep_seq, &p->senders_seq, ep);
  }
  // if the next epid has the same endpoint id, but a lower ticket, shuffle our
  // epid.
  if (next_epid != NULL &&
      next_epid->state.endpointID == ep->state.endpointID &&
      next_epid->first_ticket < ep->first_ticket) {
    RB_REMOVE(ep_id, &p->senders_epid, ep);
    RB_INSERT(ep_id, &p->senders_epid, ep);
  }
}

/** @brief sort the endpoint heaps after a change of waiting senders.
 *
 * Assume locks are already held on the process and endpoint.
 */
void
ep_shuffle_locked(Process *p, Endpoint *ep) {
  if (q_isEmpty(&ep->queue)) {
    ep->first_ticket = 0;
    ep->state.sending = 0;
    RB_REMOVE(ep_seq, &p->senders_seq, ep);
    RB_REMOVE(ep_id, &p->senders_epid, ep);
    return;
  }
  LinkedObjectHeader *first = ep->queue.head.next;
  if (first == NULL) {
    ep->first_ticket = ep->queue.ticket_sleeping;
  } else {
    ep->first_ticket = first->my_ticket;
  }
  if (ep->first_ticket < ep->min_ticket) {
    // if the senders were on the queue before the endpoint pointed at the
    // recipient, ignore the ticket value.
    ep->first_ticket = ep->min_ticket;
  }

  Endpoint *next_seq = RB_NEXT(ep_seq, &p->senders_seq, ep);
  Endpoint *next_epid = RB_NEXT(ep_id, &p->senders_epid, ep);

  // if our ticket is out of order, reshuffle our seq.
  if (next_seq != NULL &&
      next_seq->first_ticket > ep->first_ticket) {
    RB_REMOVE(ep_seq, &p->senders_seq, ep);
    RB_INSERT(ep_seq, &p->senders_seq, ep);
  }
  // if the next epid has the same endpoint id, but a lower ticket, shuffle our
  // epid.
  if (next_epid != NULL &&
      next_epid->state.endpointID == ep->state.endpointID &&
      next_epid->first_ticket < ep->first_ticket) {
    RB_REMOVE(ep_id, &p->senders_epid, ep);
    RB_INSERT(ep_id, &p->senders_epid, ep);
  }
}

/** @brief sort the endpoint id heap after a change of endpoint id.
 *
 * Assumption: the endpoint has enqueued senders, and is already organised by
 *             them.
 */
void
ep_shuffleID(Process *p, Endpoint *ep) {
  RB_REMOVE(ep_id, &p->senders_epid, ep);
  RB_INSERT(ep_id, &p->senders_epid, ep);
}

bool
epq_isEmpty(Process *p) {
  return RB_EMPTY(&p->senders_epid) && RB_EMPTY(&p->senders_seq);
}

int
epq_append(Process *recipient, Endpoint *ep, LinkedObjectHeader *sender) {
  SpinHoldInfo phi;
  SpinHoldInfo ehi;
  uint64_t ticket = 0;
  if (recipient != NULL) {
    phi = spinlock_grab(&recipient->stall_lock);
    ticket = recipient->next_ticket++; // XXX Atomic?
  }
  sender->my_ticket = ticket;

  // add the sender to ep->queue.
  ehi = spinlock_grab(&ep->queue.qLock);
  bool new_endpoint = q_isEmpty(&ep->queue);
  if (new_endpoint) {
    ep->first_ticket = ticket;
    ep->min_ticket = 0;
  }
  q_append(&ep->queue, sender);
  sender->onQ = &ep->queue;
  spinlock_release(ehi);

  if (recipient != NULL) {
    // if this endpoint didn't have senders before, add it to the two sender
    // heaps.
    if (new_endpoint) {
      RB_INSERT(ep_seq, &recipient->senders_seq, ep);
      RB_INSERT(ep_id, &recipient->senders_epid, ep);
      ep->state.sending = 1;
    }
    spinlock_release(phi);
  }
  // I don't recall why we needed a return value.
  return 0;
}

LinkedObjectHeader *
epq_pop(Process *recipient) {
  SpinHoldInfo phi = spinlock_grab(&recipient->stall_lock);
  SpinHoldInfo ehi;
  Endpoint *ep;
  LinkedObjectHeader *result;
  do {
    ep = ep_peek(recipient);
    // if there are no endpoints attempting to send, return nothing.
    if (ep == NULL) {
      spinlock_release(phi);
      return NULL;
    }
    ehi = spinlock_grab(&ep->queue.qLock);
    result = q_popfront(&ep->queue);
    // XXX: if the result is null, the next value might be persistent.
    // otherwise, reshuffle endpoints.
    if (result != NULL) {
      result->onQ = NULL;
      ep_shuffle_locked(recipient, ep);
    }
    spinlock_release(ehi);
  }
  while (ep != NULL && result == NULL);
  spinlock_release(phi);
  return result;
}

LinkedObjectHeader *
epq_pop_byID(Process *recipient, uint64_t epID) {
  SpinHoldInfo phi = spinlock_grab(&recipient->stall_lock);
  SpinHoldInfo ehi;
  Endpoint *ep = ep_peekByID(recipient, epID);
  // if there are no matching endpoints attempting to send, return nothing.
  if (ep == NULL) {
    spinlock_release(phi);
    return NULL;
  }
  ehi = spinlock_grab(&ep->queue.qLock);
  LinkedObjectHeader *result = q_popfront(&ep->queue);
  // if the result is null, the next value might be persistent.
  // XXX: if so, we should store the item we want to load somewhere.
  // otherwise, reshuffle endpoints.
  if (result != NULL) {
    result->onQ = NULL;
    ep_shuffle_locked(recipient, ep);
  }
  spinlock_release(ehi);
  spinlock_release(phi);
  return result;
}

void
epq_recipient_clear(Process *recipient, Endpoint *ep) {
  if (ep->state.sending == 0) {
    return;
  }
  SpinHoldInfo phi = spinlock_grab(&recipient->stall_lock);
  ep_remove(recipient, ep);
  spinlock_release(phi);
}

void
epq_recipient_clear_all(Process *recipient) {
  SpinHoldInfo phi = spinlock_grab(&recipient->stall_lock);
  do {
    Endpoint *ep = RB_MIN(ep_seq, &recipient->senders_seq);
    if (ep == NULL) {
      break;
    }
    ep_remove(recipient, ep);
  } while (1);
  spinlock_release(phi);
}

void
epq_recipient_set(Process *recipient, Endpoint *ep) {
  SpinHoldInfo phi = spinlock_grab(&recipient->stall_lock);
  ep->min_ticket = 0;
  if (!q_isEmpty(&ep->queue)) {
    ep->min_ticket = recipient->next_ticket++;
    RB_INSERT(ep_seq, &recipient->senders_seq, ep);
    RB_INSERT(ep_id, &recipient->senders_epid, ep);
    ep->state.sending = 1;
  }
  spinlock_release(phi);
}

void
runnable_schedule(LinkedObjectHeader *runnable) {
  if (runnable->readyQ != NULL) {
    queue_prepend(runnable->readyQ, runnable);
  } else {
    // use the caller's runnable queue
    queue_prepend(MY_CPU(readyQueue), runnable);
  }
}

void
runnable_reschedule(LinkedObjectHeader *runnable) {
  if (runnable->readyQ != NULL) {
    queue_append(runnable->readyQ, runnable);
  } else {
    // use the caller's runnable queue
    queue_append(MY_CPU(readyQueue), runnable);
  }
}

///////////////////////////////////////////////////////////////////////////////
//
// further things we need to do.
//
// and the type `Link` and macro STATIC_LINK_INIT.
//
// - find uses of the following functions and replace them:
//
// STALLQUEUE_INIT, DEFQUEUE, StallQueue
//
// stall queues are currently used for irq handlers; we may instead replace
// this with a single scheduler activation. (IRQ.c)
//
// stall queues are also used when loading an object that isn't prepared.  I
// think a better technique would be to put such linked objects into a waiting
// list.  processes can nominate objects they require to be alive before
// continuing, and the loader can go through this list collecting objects to be
// loaded and processes to be awoken. (kern_Capability)
//
// kern_Process does sq_WakeAll; this can be replaced with epQueuePopFirst or
// epQueuePopID as necessary.
//
// kern_Interval should probably be removed as we move this concept into userspace.
//
// - find uses of the following and replace them;
//
// rq_add(rq, p, at_front) -> queue_append(rq, p)
// rq_remove(rq, p) -> queue_remove(rq, p) // craft this function from epRemove
// rq_removeFront(rq) -> queue_pop(rq, hi)
//
//
// I guess the first step is to define the scheduling structure. some way to
// enable interrupt handlers to pre-empt some other running program; and for
// masking and other stuff to be restored on context switch.
//
// while we're here, my guess is that the current interrupt handling code can't
// actually mask MSI-generated interrupts; it can only mask the LVT.
//
// it looks like a good process for building this system out is to use
// LinkedObjectHeader in stall and run queues, and independently introduce the
// endpoint trees, and then 
//
// I've left with the header structure in a bit of a mess.  We also
// need to add the tree heads to the process object.
//
// Goodness.  cap_handlerBeingOverwritten is called on the current
// handler value when setHandler is called?
//
// depend_invalidate_slot says "someone may be sleeping on this
// handler, wake them up".  Thing is, it looks a lot like we wake up
// the handler.

