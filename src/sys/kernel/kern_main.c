/*
 * Copyright (C) 2005, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Main kernel entry point.
 */

/** @mainpage Coyotos Kernel Source Code
 *
 * This is the Coyotos kernel source code, as processed using
 * Doxygen.  With a bit of luck, it provides a browsable view of the
 * code.
 */
#include <inttypes.h>
#include <hal/irq.h>
#include <hal/console.h>
#include <kerninc/ccs.h>
#include <kerninc/capability.h>
#include <kerninc/ObjectHeader.h>
#include <kerninc/Process.h>
#include <kerninc/printf.h>
#include <kerninc/PhysMem.h>
#include <kerninc/Cache.h>
#include <kerninc/DevRange.h>
#include <kerninc/Sched.h>
#include <kerninc/assert.h>

#include <kerninc/MemWalk.h>

/** @brief Transfer module content into the object heap.
 *
 * Move object content into the object vectors. Once this is done we
 * release the transferred module images.
 *
 * @bug The test for exactly one module isn't correct. Not immediately
 * clear what the correct test ought to be. This is a design issue
 * rather than a code issue: what other modules might we want here,
 * and how should they be differentiated?
 */
static void
process_modules(void)
{
  size_t nPreload = 0;

  for (size_t i = 0; i < MAX_MODULES; i++) {
    if (modules[i].base == modules[i].end)
      continue;

    nPreload++;

    cache_load_module(&modules[i]);

    if (modules[i].needsRelease)
      pmem_AllocRegion(modules[i].base, modules[i].end, 
		       pmc_RAM, pmu_AVAIL, pmc_descrip(pmc_RAM));
  }

  if (nPreload == 0)
    fatal("No object preload modules!\n");
}

/** @brief Kernel entry point */
void
kernel_main(void)
{
  arch_init();

  assert(local_interrupts_enabled());

  cache_init();
  devrange_init();

  assert(local_interrupts_enabled());

  process_modules();

  cache_add_page_space(true);

  assert(local_interrupts_enabled());

  printf("Dispatching first process...\n");

  console_handoff_to_user();

  cpu_start_all_aps();

  sched_dispatch_something();

  fatal("Unexpected return from sched_dispatch_something\n");

  //  for(;;) {
  //    GNU_INLINE_ASM ("nop");
  //  }
}
