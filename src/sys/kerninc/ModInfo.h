#ifndef __KERNINC_MODINFO_H__
#define __KERNINC_MODINFO_H__
/*
 * Copyright (C) 2003, 2005, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Module information structure for preloaded CoyImage modules.
 */

#include <stdbool.h>
#include <hal/kerntypes.h>

typedef struct ModInfo {
  kpa_t base;
  kpa_t end;
  bool  needsRelease;
  bool  needsReloc;
  char  imgName[64];		/* if any */
} ModInfo;

/* Defined in kern_main.c */
extern ModInfo modules[MAX_MODULES];
extern uint32_t nModules;

/** @brief Called from the BSP layer to register mkimage modules. */
void module_register(kpa_t start, kpa_t end, const char *nm);

/** @brief Called from the BSP layer to relocate modules to top of
 * available memory. */
void module_relocate();

#endif /* __KERNINC_MODINFO_H__ */
