#ifndef __KERNINC_FREELIST_H__
#define __KERNINC_FREELIST_H__
/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file 
 * @brief Low-level free lists.
 *
 * Free lists are manipulated by non-blocking data structures, because
 * we need to be able to release things after the commit point, and
 * that implies that we must not spin.
 */

#include <stdbool.h>
#include <hal/atomic.h>

typedef struct FreeListElem FreeListElem;
struct FreeListElem {
  FreeListElem *next;
};

typedef struct FreeListHeader FreeListHeader;
struct FreeListHeader {
  ATOMIC_PTR(FreeListElem *) next;
};

#define FREELIST_INIT { { 0 } }

static inline FreeListElem *
freelist_alloc(FreeListHeader *flh)
{
  FreeListElem *next = APTR_READ(flh->next);

  for(;;) {
    FreeListElem *was = APTR_CAS(flh->next, next, next->next);
    if (was == next) {
      if (next) next->next = 0;
      return next;
    }
    next = was;
  }
}

static inline void
freelist_insert(FreeListHeader *flh, void *vp)
{
  FreeListElem *ob = vp;
  FreeListElem *first = APTR_READ(flh->next);

  for (;;) {

    ob->next = first;

    FreeListElem *was = APTR_CAS(flh->next, first, ob);
    if (was == first)
      return;

    first = was;
  }
}

static inline void
freelist_init(FreeListHeader *flh)
{
  APTR_SET(flh->next, 0);
}

#endif /* __KERNINC_FREELIST_H__ */
