#ifndef __KERNINC_PROCESS_H__
#define __KERNINC_PROCESS_H__
/*
 * Copyright (C) 2007, The EROS Group, LLC
 * Copyright (C) 2024, William ML Leslie
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file 
 * @brief Definition of process structure. */

#include <stddef.h>
#include <sys/tree.h>
#include <hal/vm.h>
#include <kerninc/ccs.h>
#include <kerninc/capability.h>
#include <kerninc/Queue.h>
#include <kerninc/ObjectHeader.h>
#include <kerninc/LinkedObjectHeader.h>
#include <kerninc/CPU.h>
#include <kerninc/Sched.h>
#include <kerninc/Interval.h>
#include <obstore/Process.h>
#include <idl/coyotos/Process.h>

/**
 * @brief Reasons that a process cannot run.
 *
 * Later issues have to do with required functional units. If you run
 * in to an architecture with a new type of functional unit, add an
 * appropriate entry, but try to use a suitably architecture-neutral
 * name. For example, on IA-32 the pi_VectorUnit refers to SSE2/SSE3
 * state.
 */
enum Issues {
  pi_Schedule     = 0x0001u,  /**< @brief Scheduling criteria are unknown. */
  pi_Faulted      = 0x0002u,  /**< @brief Process has non-zero fault code. */
  pi_SingleStep   = 0x0004u,  /**< @brief A single step has been requested. */
  pi_Preempted    = 0x0008u,  /**< @brief Process has been preempted */

  pi_NumericsUnit = 0x1000u,  /**< @brief Process needs the FPU */
  pi_VectorUnit   = 0x2000u,  /**< @brief Process needs the vector unit */
  pi_SysCallDone  = 0x4000u,  /**< @brief System call has completed.
			       *
			       * This "issue" indicates that the CPU
			       * is free to return using the system
			       * call return protocol, as opposed to
			       * the trap return protocol.
			       *
			       * Note the design assumes that
			       * returning via the trap return
			       * protocol is @em always valid,
			       * regardless of entry method. This is
			       * necessary because system calls may
			       * abort and need to be restarted, and
			       * on many hardware implementations said
			       * restart cannot be implemented via the
			       * hardware system call return mechanism.
			       */

};
typedef enum Issues Issues;

/** @brief Issues that should be set when a process is initially
    loaded. */
#define  pi_IssuesOnLoad (pi_Schedule)

/** @brief The process structure.
 *
 * Every running activity has an associated process structure. This
 * contains the register state of the process. In contrast to EROS,
 * there are no in-kernel processes in Coyotos.
 * 
 * Processes are reallocated in round-robin order, which may well prove
 * to be a dumb thing to do.
 *
 *
 * The substates of PRS_RUNNING are spelled out here:
 *
 *   wiki:KernelInternals/ProcessLocking
 *
 * @section Locking
 *
 * @note Some of this is wrong, because I failed to consider that
 * every process grabs its own lock on the way into the kernel if it
 * did not enter for reason of having taken an interrupt.
 *
 * @p queue_link and @p queue are protected by the lock of the queue
 * the process is on.  Note that a process can change queues, so care
 * must be taken in acquiring the queue lock while holding the process lock.
 *
 * The locking rules for other (non-atomic) fields have three cases:
 *
 * <ul>
 *  <li>
 *    <p>
 *      <b>Process not on any CPU.</b>
 *      <tt>(p!=MY_CPU(current))&amp;&amp;(p-&gt;onCPU==NULL)</tt>.
 *    </p>
 *    <p>
 *      <b>Rule:</b> all fields are guarded by @p
 *      hdr.lock.
 *      <b>Rule:</b> the @p onCPU field can only be upgraded to
 *         non-null with @p hdr.lock held.
 *    </p>
 *    <p>
 *      Note that in particular this includes the @p onCPU field, so
 *      the process cannot transition onto another CPU while you hold
 *      the @p hdr.lock.
 *    </p>
 *  </li>
 *  <li>
 *    <p>
 *      <b>Process is current process on current CPU, executing in
 *      kernel mode.</b>
 *      <tt>(p==MY_CPU(current))&amp;&amp;(p-&gt;onCPU==CUR_CPU)</tt>.
 *    </p>
 *    <p>
 *      <b>Rule:</b> the subfields of @p hdr
 *         are guarded by @p hdr.lock.<br>
 *      <b>Rule:</b> other fields may be manipulated without aquiring
 *         any lock.<br>
 *      <b>Rule:</b> onCPU may be nullified without aquiring @p hdr.lock.<br>
 *      <b>Rule:</b> aquiring a lock on MY_CPU(current) is harmless.
 *    </p>
 *  </li>
 *  <li>
 *    <p>
 *      <b>Process is current process on some other CPU.</b>
 *      <tt>(p!=MY_CPU(current))&amp;&amp;(p-&gt;onCPU!=NULL)</tt>.
 *    </p>
 *    <p>
 *      <b>Rule:</b> non-atomic fields are guarded by @p
 *      hdr.lock + proc_ensure_exclusive().
 *      <b>Rule:</b> onCPU may be read, but not written.<br>
 *    </p>
 *    <p>
 *      This case can only arise when you are doing a manipulation of
 *      a non-current process. Control of the target process must be
 *      aquired by first taking @p hdr.lock and then calling
 *      proc_ensure_exclusive() to bounce the process off of its CPU. 
 *    </p>
 *  </li>
 * </ul>
 *
 * Note that if @p onCPU is non-NULL and != MY_CPU(current), the
 * target process may be running in user mode on the other CPU. In
 * that case, the following restrictions apply:
 *
 * @li 
 *   @p fixregs, @p floatregs, @p softregs, @p faultCode, and @p
 *   faultInfo are completely undefined, and cannot be read or written.
 * @li
 *   Capability state is undefined (target may perform cap_load
 *   without locking).
 * @li
 *   The runState cannot be changed.
 *   
 * To clear @p onCPU when it is not on our CPU, or to manipulate the
 * fields of a target process when it is not on our CPU, a cross-call
 * to the CPU in question is required, with the Process's header's
 * lock held across the crosscall.  After onCPU is cleared, the
 * process must either change runState or be placed onto a ready queue
 * before the header lock is dropped for any reason.
 */

struct Process {
  /** @brief This process can be linked to a stall or ready queue.
   *
   * The contents of this field are protected by onQ->qLock, if onQ is
   * non-NULL.
   */
  struct LinkedObjectHeader lohdr;

  uintptr_t   /* alignment pad */ : 0;

  /** @brief Reasons that the process cannot run.
   *
   * The issues field is a bitmask that identifies any conditions that
   * preclude this process from making progress if dispatched. If the
   * issues field is zero, the process has no issues and is immediately
   * dispatchable. If the field is non-zero, some form of special
   * handling is required before dispatch is possible.
   *
   * Meanings of individual bit positions are defined above in the
   * Issues enumeration. 
   */
  Atomic32_t        issues;

  cpuid_t           lastCPU;

  /** @brief the CPU on which the process is currently running.
   *
   * May only be set to a non-NULL value with hdr.lock held.  After it is
   * set, may only be cleared by the CPU it points to. This field is
   * an atomic pointer because it can be cleared without hdr.lock
   * being held.
   */
  ATOMIC_PTR(struct CPU *) onCPU;

  /** @brief For a process that is sleeping: when to wake up. */
  Interval          wakeTime;

  /** @brief The top mapping structure for this process. */
  struct Mapping    *mapping;

  /** @brief Endpoints with stalled senders, by ticket order. */
  RB_HEAD(ep_seq, Endpoint) senders_seq;

  /** @brief Endpoints with stalled senders, by endpointID. */
  RB_HEAD(ep_id, Endpoint) senders_epid;

  /** @brief a lock to guard the senders trees. */
  spinlock_t stall_lock;

  /** @brief an incrementing value to sequence the senders. */
  ticket_t next_ticket;

  /** @brief Externalizable (persistent) process state.
   *
   * Note that there is an alignment issue here on some
   * processors. On Coldfire, for example, we had a situation at one
   * point where the 16-bit native alignment combined with the
   * then-current content of the ObjectHeader structure to cause the
   * @p state field to align at a 16-bit boundary. As with most other
   * architectures, the kernel re-entry SP pointed into the middle of
   * state and therefore happened to be aligned at a 16-bit
   * boundary. Which was fine, except that the re-entry hardware
   * forcibly sets the stack pointer to a 32-bit boundary on re-entry
   * to avoid some microstate complications. This resulted in a bad
   * reload, with predictably fatal consequences.
   *
   * The ExProcess structure itself is naturally aligned at all
   * points, and contains at least a few 64-bit quantities.
   */
  ExProcess         state PSTATE_ALIGN;
};
typedef struct Process Process;

//#ifdef COYOTOS_HAVE_FPU
///* FPU support: */
//DECLARE_PER_CPU(extern Process*,proc_fpuOwner);
//#endif

extern Process *proc_ProcessCache;

static inline void
proc_SetFault(Process *p, uint32_t code, uva_t faultInfo)
{
  p->state.faultCode = code;
  p->state.faultInfo = faultInfo;
  atomic_set_bits(&p->issues, pi_Faulted);
}

static inline void
proc_TakeFault(Process *p, uint32_t code, uva_t faultInfo) NORETURN;

static inline void
proc_TakeFault(Process *p, uint32_t code, uva_t faultInfo)
{
  proc_SetFault(p, code, faultInfo);
  sched_restart_transaction();
}

extern void proc_gc(Process *);

/** @brief Attempt to dispatch the passed process. 
 *
 * If the process constituents are not in memory, starts the work to
 * bring them in and abandon the transaction. If the process has
 * faulted, attempts to deliver the fault message to the keeper, else
 * enqueues faulted process on keeper and returns. Abandons current
 * transaction if any of this causes specified process to block.
 */
void proc_dispatch_current() NORETURN;

/** @brief Clear any architecture-dependent issues that would
 * preclude dispatching this process.
 *
 * Returns only if successful.
 */
void proc_clear_arch_issues(Process *p);

/** @brief Find a per-process mapping table that is appropriate for
 * executing on the current CPU.
 */
void proc_migrate_to_current_cpu();

/** @brief Initialize process registers to a reasonable (secure) state.
 */
void proc_arch_initregs(uva_t pc);

/** @brief Called from HAL when current process makes a system call.
 */
__hal void proc_syscall(void);

/** @brief Deliver a memory fault that accrues to process @p.
 *
 * There are three cases where this routine is called:
 *
 * -# During a normal page fault. In this case @p p matches
 *    @c current.
 * -# During input argument processing in invocations. This is
 *    logically just like the page fault case. @p p matches 
 *    @c current. 
 * -# During @em output argument processing during invocations. This
 *    is a cross-space case. @p p does @em not equal @c current.
 *
 * In all cases, @p p will eventually end up in the PRS_FAULTED
 * state, but the mechanism varies. See comments in the implementation
 * for details.
 */
struct MemWalkResults;

void proc_deliver_memory_fault(Process *p,
			       coyotos_Process_FC fc, uintptr_t addr, 
			       const struct MemWalkResults * const mwr);

/** @brief Ensure that it is safe to manipulate state of an already-locked
 * process.
 *
 * On a single-CPU system, this is a no-op.  On multi-CPU systems, it is
 * possible that a process is on-CPU even though we are holding its lock.
 *
 * If run-state is not PRS_RUNNING, it is not necessary to call this routine.
 *
 * If run-state *is* PRS_RUNNING, is not safe to:
 *   <ol><li>modify anything in the "state" member, or</li>
 *       <li>read any non-capability field of "state", except for runState</li>
 *   </ol>
 * without first calling this routine.
 */
void proc_ensure_exclusive(Process *p);

/** @brief Results of <tt>proc_findCapPageSlot</tt> and/or
 *  <tt>proc_findDataPage</tt>, naming a slot in a  
 *  CapPage and the restrictions on its use.
 */
typedef struct FoundPage {
  struct Page *pgHdr;	/**< @brief The CapPage targeted */
  size_t slot;		/**< @brief The slot number to use */
  uint8_t restr;	/**< @brief The restrictions on use of the slot */
} FoundPage;

/** @brief Locate the CapPage slot in address space of process @p p at
 * address @p addr.
 *
 * Returns a non-zero fault code on error, without touching @p results.
 * On success, returns zero, and fills @p results with information necessary
 * to use the slot.
 *
 * Precondition: Process @p p's lock must be held.
 *
 * Postcondition: 
 *    On zero return, results->capPage is locked, and 
 *                    results->restr is the cumulative restrictions of the 
 *                    path to results->capPage.
 */
extern coyotos_Process_FC
proc_findCapPageSlot(Process *p, uintptr_t addr, bool forWriting,
		     bool nonBlock,
		     struct FoundPage * /*OUT*/ results);

/** @brief Locate the Page in Process @p p's address space at
 * address @p addr.
 *
 * Returns a non-zero fault code on error, without touching @p results.
 * On success, returns zero, and fills @p results with information necessary
 * to use the slot.
 *
 * Precondition: Process @p p's lock must be held.
 *
 * Postcondition: 
 *    On zero return, results->pgHdr is locked, and 
 *                    results->restr is the cumulative restrictions of the 
 *                    path to results->capPage.
 */
coyotos_Process_FC
proc_findDataPage(Process *p, uintptr_t addr, bool forWriting,
		  bool nonBlock,
		  struct FoundPage * /*OUT*/ results);

/** @brief Resume execution of the current process.
 *
 * Returns to user land executing the current process. This is a
 * low-level assembly language entry point supplied by the
 * architecture-dependent code.
 *
 * This should always be called with local interrupts DISABLED.
 */
__hal void proc_resume(Process *p) NORETURN;

/* Debugging only */
__hal void proc_dump_current_savearea();
#endif /* __KERNINC_PROCESS_H__ */
