#ifndef __KERNINC_LINKEDOBJECT_H__
#define __KERNINC_LINKEDOBJECT_H__
/*
 * Copyright (C) 1998, 1999, 2005, The EROS Group, LLC.
 * Copyright (C) 2024, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Delete this, it's all in StallQueue.h now.
 */

// #include <kerninc/ObjectHeader.h>

struct LinkedObjectHeader;

/** @brief Linked list structure */
typedef struct ObjectLink {
  struct LinkedObjectHeader *next;
  struct LinkedObjectHeader *prev;
} ObjectLink;




#endif /* __KERNINC_LINKEDOBJECT_H__ */
