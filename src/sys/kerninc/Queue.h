#ifndef __KERNINC_QUEUE_H__
#define __KERNINC_QUEUE_H__
/*
 * Copyright (C) 2007, The EROS Group, LLC
 * Copyright (C) 2024, William ML Leslie
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Type definition and functions for stall queues.
 */

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <kerninc/mutex.h>
#include <kerninc/capability.h>
#include <kerninc/Sched.h>

struct LinkedObjectHeader;

/** @brief Linked list structure */
typedef struct ObjectLink {
  struct LinkedObjectHeader *next;
  struct LinkedObjectHeader *prev;
} ObjectLink;

typedef uint32_t ticket_t;
#define TICKET_INVALID (0xFFFFFFFFull)

/** @breif Queue that might contain deprepared objects.
 *
 * In an ideal world, we'd be able to know all of the objects waiting on a
 * queue, loop through them, and perform operations on them, but we also don't
 * want to tie up memory with processes that aren't going to be able to do
 * anything for some time.  The Queue_t has continuous lists at the start and
 * the end of the queue, but allows the middle of the queue to be reclaimed and
 * deprepared.
 *
 * This allows most queue operations to either succeed or accurately describe
 * the right condition for blocking.  Append and prepend never fail or block,
 * popfront never fails if !queue_needsFill.  The only exception is queue_move,
 * which I haven't quite figured out how to work with the checkpointer yet.
 *
 * The q_ functions are internal and perform no locking, these are for use by
 * the endpoint system.  The queue_ functions are for general use.
 *
 * The Queue struct is approximately 5 pointers + 2 capabilities in size -
 * about 72 bytes.
 */

typedef struct Queue {
  spinlock_t qLock;
  /* The first section of the queue, objects ready for `popfront`. */
  ObjectLink head;
  /* The ticket of the first sleeping item.  This allows an endpoint
     containing this queue to be sorted. */
  ticket_t ticket_sleeping;
  /* The first object not present in the `head` list. */
  capability next_sleeping;
  /* The last object not present in the `head` list or the `tail` list. */
  capability last_sleeping;
  /* if next_sleeping is not-null, append onto the queue should append to
   * tail.  these processes will be written out into the sleeping set. */
  ObjectLink tail;
} Queue_t;

/** @brief Remove the first linked object from the queue.
 *
 * Will return NULL if nothing can be returned right now.  Use
 * queue_isEmpty to find out if more items can be loaded from disk.
 */
struct LinkedObjectHeader *queue_popfront(Queue_t *q);

/** @brief Append the linked object to the end of the queue.
 *
 * Precondition: The object is not on any queue.
 *
 * The lock will be taken on the queue - don't call this with the lock held.
 */
void queue_append(Queue_t *q, struct LinkedObjectHeader *item);

/** @brief Prepend the linked object to the start of the queue.
 *
 * Precondition: The object is not on any queue.
 *
 * The lock will be taken on the queue - don't call this with the lock held.
 */
void queue_prepend(Queue_t *q, struct LinkedObjectHeader *item);

/** @brief Remove the linked object from the queue.
 *
 * Precondition: The object is on the specified queue.
 *
 * The lock will be taken on the queue - don't call this with the lock held.
 */
void queue_remove(Queue_t *q, struct LinkedObjectHeader *item);

/** @brief Move the items on the first queue to the second.
 *
 * The spinlock will be taken on both queues, first on the source
 * queue.  All callers must agree on this locking order.
 */
bool queue_move(Queue_t *q, Queue_t *into);

/** @brief Place the item on a queue to be run.
 *
 * Precondition: the object is locked.
 *
 * If the item specifies its own runnable queue, use that, otherwise
 * use the context queue.
 */
void runnable_schedule(struct LinkedObjectHeader *item);

void runnable_reschedule(struct LinkedObjectHeader *item);

/** @brief Does this queue include objects not resident in RAM?
 *
 * A partial queue may not be empty, and yet a peek on it might not
 * return anything.  Use this function to check whether the queue
 * contains items that aren't ready for use.
 */
bool queue_isPartial(struct Queue *q);

/** @brief Is the queue empty? */
bool queue_isEmpty(struct Queue *q);

/** @brief Is the next item on the queue deprepared? */
bool queue_needsFill(struct Queue *q);

/** @brief Fetch the first item on the queue that isn't resident */
capability queue_getFirstPartial(struct Queue *q);


bool epq_isEmpty(struct Process *p);

/** @brief enqueue sender for sending to the endpoint.
 *
 * Sender wants to send IPC to the endpoint, but no recipient is ready.  Enqueue
 * the sender on the endpoints' stall queue, and enqueue the endpoint on the
 * recipient's stall queue, if necessary.
 */
int epq_append(struct Process *recipient, struct Endpoint *ep,
               struct LinkedObjectHeader *sender);

/** @brief Retrieve the next sender.
 *
 * This function can be used when the receiver is in closed-wait to return the
 * process that can send to it and remove it from the queue.
 */
struct LinkedObjectHeader *epq_pop(struct Process *recipient);

/** @brief Retrieve a sender sending with a given endpoint id.
 *
 * This function can be used when the receiver is in closed-wait to return the
 * process that can send to it and remove it from the queue.
 */
struct LinkedObjectHeader *epq_pop_byID(struct Process *recipient,
                                        uint64_t epID);

/** @brief clear the recipient from the endpoint. */
void epq_recipient_clear(struct Process *recipient, struct Endpoint *ep);

/** @brief remove the recipient from the endpoint. */
void epq_recipient_set(struct Process *recipient, struct Endpoint *ep);

/** @brief clear the recipient from all endpoints. */
void epq_recipient_clear_all(struct Process *recipient);

/** @brief initialise a Queue_t. */
static inline void
queue_init(Queue_t *q) {
  q->head.next = NULL;
  q->head.prev = NULL;
  q->tail.next = NULL;
  q->tail.prev = NULL;
  q->ticket_sleeping = 0;
  cap_init(&q->next_sleeping);
  cap_init(&q->last_sleeping);
}

// typedef struct Queue StallQueue;

#define QUEUE_INIT(name) { SPINLOCK_INIT, { NULL, NULL } }
#define DEFQUEUE(name) \
  struct Queue name = QUEUE_INIT(name);

/** @brief Initialize an objects prev/next pointer. */
static inline void
oblink_init(ObjectLink *l)
{
  l->prev = NULL;
  l->next = NULL;
}

/** @brief Return true iff this item is not part of a list. */
static inline bool
oblink_isSingleton(ObjectLink *l)
{
  return l->next == NULL && l->prev == NULL;
}

#endif /* __KERNINC_STALLQUEUE_H__ */
