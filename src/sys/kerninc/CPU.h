#ifndef __KERNINC_CPU_H__
#define __KERNINC_CPU_H__
/*
 * Copyright (C) 2007, The EROS Group, LLC.
 * Copyright (C) 2024, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file 
 * @brief Machine-independent per-CPU information.
 */

#include <stddef.h>
#include <stdbool.h>
#include <hal/config.h>
#include <hal/atomic.h>
#include <hal/kerntypes.h>
#include <hal/cpu.h>
#include <kerninc/ccs.h>

/** @brief Process executing on current CPU has been preempted.
 *
 * This bit is set whenever the current CPU has changed its value of
 * MY_CPU(current) within the kernel. It is set:
 *  - In the Interval timer logic, to indicate that the process which
 *    entered the kernel should give way because its time has run out.
 *  - In sched_abandon_transaction(), to indicate that the process
 *    which entered the kernel has @em already given way.
 *
 * It is checked in proc_dispatch_current() as a sanity measure.
 *
 * This bit is set in the interval timer code to indicate that the
 * slice of the current process has ended.
 */
#define CPUFL_WAS_PREEMPTED 0x1

/** @brief Wakeup processing is required on current CPU.
 *
 * This bit is "owned" by the interval management logic. It is set
 * and cleared in the interval code, and it is checked just before
 * returning from traps/interrupts to determine if wakeup processing
 * is needed. 
 */
#define CPUFL_NEED_WAKEUP  0x2

#define MSG_BUFFER_LEN 128

struct Process;
struct Queue;

typedef struct CPU {
  /** @brief Mutex value for locks held by current process on this
   * CPU.
   *
   * A change to this field has the side effect of gang releasing all
   * spinlocks and mutexes currently held by this CPU. To understand
   * how, see mutex_do_trylock().
   */
  Atomic32_t procMutexValue;

  /** @brief Process that is presently running on this CPU (if any). */
  __mycpu struct Process *current;

  /** @brief Unique identifier for this CPU. */
  __immutable cpuid_t   id;

  /** @brief Bitmap of this CPU's available transmap entries. */
  __mycpu transmeta_t  TransMetaMap;

  /** @brief Bitmap of this CPU's available transmap entries that have
   * been released but not yet flushed from the TLB. */
  __mycpu transmeta_t  TransReleased;

  /** @brief Per-CPU kernel stack reload address. */
  __immutable kva_t     topOfStack;

  /** @brief If shouldDefer matches procMutexValue, this CPU has been
   * asked to get out of the way if it cannot aquire a mutex immediately.
   */
  Atomic32_t shouldDefer;

  /** @brief true iff this CPU is present. */
  __immutable bool       present;

  /** @brief true iff this CPU has been started.
   *
   * At the moment we have no means to shut CPUs down except during
   * powerdown. When that changes, this will probably need to become
   * atomic. */
 __immutable  bool       active;

  /** @brief Priority of current process on CPU */
  Atomic32_t   priority;

  /** @brief Per-CPU action flags for this CPU.
   * 
   * Zero on kernel entry. Bits set in various places if the
   * preemption timer goes off, and/or if we need to do wakeup
   * processing on the sleeping process queue.
   */
  Atomic32_t  flags;

  /** @brief Mapping context currently loaded on this CPU.
   *
   * Corner case: if the CPU has not yet been IPL'd, this is the map
   * that @em will be loaded onto that CPU during IPL. We need to mark
   * such a map loaded so that the map will not be reclaimed. */
  __mycpu struct Mapping *curMap;

  /** @brief Kernel message composition buffer for this CPU.
   *
   * This field exists primarily to work around the fact that many of
   * our kernel messages currently get assembled by a @em sequence of
   * calls to printf(). It would be cleaner all around if each
   * kernel-originated message used exactly one such call. At present,
   * the printf subsytem inserts a message boundary whenever a newline
   * followed by a NUL is seen. The kernel does not intentionally
   * issue multi-line messages, but application-level subsystems might
   * do so.
   */
  __mycpu char messageBuffer[MSG_BUFFER_LEN];
  /** @brief Next output position in message buffer.
   *
   * We @em rely in the fact that this initializes to zero because the
   * entire CPU structure is placed in the BSS region. */
  __mycpu size_t messageBufferPos;

#if 0
  /** @brief Allocated object recovery pointer.
   *
   * When an object is allocated, there is a (hopefully brief) window
   * of time when the object is not on any list. Ideally, we should
   * not yield during this period, but on some architectures that
   * constraint is inconvenient and it is not always obvious to the
   * programmer which functions may yield. There is only one such
   * object outstanding at a time, so our recovery strategy is to keep
   * track of the pointer here along with a callback that knows how to
   * re-free the object (back on to the appropriate free list).
   */
  __mycpu void *orphanedObject;

  /** @brief Procedure that knows how to put the orphanedObject onto
   * its appropriate free list.
   */
  void (*yieldAllocFixup)(void *);
#endif

  /** @brief The ready queue of the current schedule.
   *
   * This queue is used to draw ready processes from.  It is also used
   * to append processes to, if the target process does not have its
   * own ready queue.
   */
  __mycpu struct Queue *readyQueue;

  /** @brief Linked list of vector entries that we need to awaken.
   *
   * This needs to be manipulated with interrupts disabled.
   */
  __mycpu struct VectorInfo *wakeVectors;
} CPU;

/* Guaranteed <= MAX_NCPU, defined in hal/machine.h */
extern size_t cpu_ncpu;

/** @brief Vector of all possible CPU structures */
extern CPU cpu_vec[MAX_NCPU];

/** @brief Initialize the machine-independent CPU structure for a
 * given CPU.
 */
void cpu_construct(cpuid_t ndx);

/** @brief Wake up the processes that are blocked waiting for pending
 * interrupts on this CPU.
 */
void cpu_wake_vectors();

#if MAX_NCPU > 1
#define CUR_CPU (current_cpu())
#else
#define CUR_CPU (&cpu_vec[0])
#endif

#define MY_CPU(id) CUR_CPU->id

/** @brief Return the CPU structure pointer (entry in cpu vector) of
 * the currently executing processor.
 *
 * This is NOT the preferred way to do this, as it is fairly
 * expensive. The difference between this and current_cpu is that it
 * can be called by a processor whose stack is not yet set up.
 */
struct CPU* cpu_getMyCPU();

/** @brief IPL any attached processors */
__hal extern void cpu_init_all_aps(void);
__hal extern void cpu_start_all_aps(void);

#endif /* __KERNINC_CPU_H__ */
