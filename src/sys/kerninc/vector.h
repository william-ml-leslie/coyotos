#ifndef __KERNINC_VECTOR_H__
#define __KERNINC_VECTOR_H__
/*
 * Copyright (C) 2007, The EROS Group, LLC
 * Copyright (C) 2024, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Common interrupt distribution logic.
 *
 * While the low-level handling of interrupts is architecture
 * dependent, the high-level handling is more or less common, and the
 * information that needs to be tracked concerning interrupt vectors
 * is likewise more or less common.
 *
 * Coyotos uses a single vector to describe exceptions, traps, system
 * calls, and interrupts. A given target MUST use this vector for
 * interrupts, but it is free to handle traps and system calls through
 * other means if that is the preferred implementation on the
 * target. Typically, the VectorMap array indices will correspond to
 * hardware vector numbers that are either supplied by the hardware
 * directly or injected by the low-level trap interface.
 *
 * When Coyotos code refers to an IRQ, it is referring to a
 * sequentially numbered hardware pin number. When it refers to a
 * vector number, it is referring to a hardware-supplied index
 * number. A subset of entries in the VectorMap array will describe
 * IRQs. Because system calls may be interspersed with irq entries, it
 * is necessary to maintain both forward and backward mappings between
 * vector numbers and IRQ numbers "by hand".
 * 
 * Concrete examples:
 *
 * - IA-32 handles traps, interrupts, and SYSCALLS through the same
 *   hardware vector mechanism, and system calls need to be
 *   interspersed with interrupts in the hardware
 *   vector. On this platform, NUM_VECTOR=NUM_TRAP+NUM_IRQ
 * - Coldfire has a hardware-supplied system call mechanism that uses
 *   a separate entry point, and (from memory) much of its interrupt
 *   demultiplexing is done in software. On that platform We wil
 *   probably use NUM_VECTOR=NUM_IRQ, and demultiplex traps and system
 *   calls using machine-dependent structures and code.
 */

/** @page IntHandling Handling of Interrupts
 *
 * This page describes how interrupt handling is performed within the
 * Coyotos kernel.
 *
 * @section IntTheory Theory of Operation
 *
 * The interval timer interrupt is fully handled within the kernel. I
 * am @em considering virtualizing this interrupt for
 * application-level timer use. All other interrupts are handled by
 * applications.
 *
 * For application-handled interrupts, the role of the kernel is to
 * process interrupts asserted by the interrupt controller and deliver
 * these interrupts to the appropriate application. If no application
 * is currently waiting for the interrupt, execution proceeds as
 * follows:
 *
 * - The kernel makes note that the interrupt has arrived.
 * - The interrupt is masked at the controller to ensure that a single
 *   interrupt will not be multiply delivered.
 * - If the interrupt is edge triggered, it is acknowledged at the
 *   controller. 
 *
 * When an application requests to wait for an interrupt, a check is
 * made to determine whether that interrupt may have already arrived,
 * and the logic proceeds as follows:
 *
 * - If the interrupt is already pending, the application's request to
 *   wait returns immediately, and the interrupt is marked as no
 *   longer pending.
 * - If no interrupt is pending at the software layer, the hardware
 *   source is unmasked to allow interrupt recognition, and the
 *   requesting application is blocked.
 *
 * Various optimizations of this are possible. Notably: interrupts can
 * be deasserted, and there is no particularly good reason to fire off
 * a driver on a phantom interrupt.
 *
 * @section IntDisable Enable and Disable
 *
 * Within the Linux kernel, there are functions @p disable_irq() and @p
 * enable_irq(), and associated @p nosync variants, that support
 * mutual exclusion between the upper and lower halves of
 * drivers. When an upper half must examine or manipulate data without
 * being molested by the corresponding lower half, it uses these
 * functions to ensure that the lower half is not currently running
 * and cannot interfere (the @p nosync versions do not check whether
 * the lower half is presently running).
 *
 * I provisionally believe that these are necessarily used as
 * bracketing calls, and necessarily from the same logical thread of
 * control. Hypothetically that thread of control might transit from
 * one CPU to another, but in the Linux kernel this does not occur.
 *
 * Since we want to make use of Linux drivers, this raises a question
 * for Coyotos: should we support this functionality in the kernel or
 * not?
 *
 * I provisionally believe that the right answer is @em no. If this
 * mechanism is provided in the kernel, then the kernel becomes
 * entangled in reference counting the disables, and an error by a
 * driver could lead to a dangling non-zero reference count. It
 * therefore seems better (to me, at least) for this issue to be
 * handled in the driver compatibility cradle. If the cradle goes, it
 * goes as a unit, taking the entire reference count with it.
 */

#include <hal/irq.h>
#include <kerninc/Process.h>
#include <kerninc/Queue.h>

struct VectorInfo;

/**@brief Signature of a vector handler function. */
typedef void (*VecFn)(struct VectorInfo *vec, struct Process *,
		      fixregs_t *saveArea);

/** @brief Method dispatch for interrupt controller chips */
struct IrqController {
  irq_t baseIRQ;	   /**< @brief First IRQ on this controller */
  irq_t nIRQ;		   /**< @brief Number of IRQ sources */
  kva_t va;			/**< @brief For memory-mapped controllers. */
  void (*setup)(struct VectorInfo *vi);
  bool (*isPending)(struct VectorInfo *vi);
  void (*unmask)(struct VectorInfo *vi);
  void (*mask)(struct VectorInfo *vi);
  void (*ack)(struct VectorInfo *vi);
};
typedef struct IrqController IrqController;

/** @brief Vector types.
 *
 * Values for VectorInfo.type field.
 */
enum VecType {
  vt_Unbound,		/**< @brief Not yet bound.  */
  vt_HardTrap,		/**< @brief Hardware trap or exception.  */
  vt_SysCall,		/**< @brief System call (software trap).  */
  vt_Interrupt,		/**< @brief Interrupt source.  */
};
typedef enum VecType VecType;

#define VEC_MODE_FROMBUS 0
#define VEC_MODE_EDGE    1
#define VEC_MODE_LEVEL   2

#define VEC_LEVEL_FROMBUS  0
#define VEC_LEVEL_ACTHIGH  1
#define VEC_LEVEL_ACTLOW   2

enum VecStatus {
  vec_st_uninit,
  vec_st_live,
  vec_st_fired,
  vec_st_delivered
};
typedef enum VecStatus VecStatus;

/** @brief Per-vector information.
 *
 * This structure stores the machine-dependent mapping from vectors to
 * handlers, and also from vectors to interrupt lines. Note that
 * interrupts 
 *
 * The stall queue associated with a given interrupt pin is actually
 * stored in the VectorInfo structure.
 *
 * A given vector can be in one of four states:
 *
 * -# Uninitialized. Nothing is known about this vector. It is not
 *    claimed by either the kernel or a user-mode driver.
 * -# Live. Interrupts/events on this vector will be delivered to the
 *    CPU by the interrupt controller.
 * -# Fired. An interrupt/event has been detected, but has not yet
 *    been delivered to the appropriate user-mode handler. The
 *    interrupt is presently masked at the hardware layer. If the
 *    interrupt was level-triggered, an acknowledge has been issued to
 *    the controller.
 * -# Delivered. An interrupt/event has been delivered to the
 *    driver. The driver has not yet indicated interest in the next
 *    interrupt/event on this vector.
 */
struct VectorInfo {
  VecFn    fn;			/**< @brief Handler function. */
  uint64_t count;		/**< @brief Number of occurrences. */
  uint8_t  type;		/**< @brief See VecType. */

  /* Descriptive information about this vector entry, set at
     initialization time: */
  uint8_t  user : 1;		/**< @brief User accessable */
  uint8_t  mode : 2;		/**< @brief Trigger mode */
  uint8_t  level : 2;		/**< @brief Active hi/lo */

  /* Current status of this vector: */
  uint8_t  status : 2;		/**< @brief Current vector state.  */

  uint32_t  irq;		/**< @brief Global interrupt pin number. */
  IrqController* ctrlr;		/**< @brief Controller chip */
  Queue_t stallQ;

  struct VectorInfo *next;
};
typedef struct VectorInfo VectorInfo;

/** @brief per-vector information. */
extern struct VectorInfo VectorMap[NUM_VECTOR];

/** @brief Reverse map from IRQs to vector entries. */
extern VectorInfo *IrqVector[NUM_IRQ];

/** @brief Initialize the vector table and the corresponding IDT
 * entries.
 */
__hal void vector_init(void);

typedef struct VectorHoldInfo {
  SpinHoldInfo shi;
  flags_t oldFlags;
} VectorHoldInfo;

/** @brief Lock a vector entry for manipulation, including guarding
 * against interrupts.
 */
static inline VectorHoldInfo vector_grab(VectorInfo *v)
{
  VectorHoldInfo vhi;
  vhi.oldFlags = locally_disable_interrupts();
  vhi.shi = spinlock_grab(&v->stallQ.qLock);

  return vhi;
}

/** @brief Release a vector entry, allowing further interrupts or
 * activity on this vector.
 */
static inline void vector_release(VectorHoldInfo vhi)
{
  spinlock_release(vhi.shi);
  locally_enable_interrupts(vhi.oldFlags);
}

void vector_set_status(VectorInfo *v, VecStatus vs);

#if 0

/** @brief Enable specified interupt pin. */
void irq_EnableVector(irq_t irq);
/** @brief Disable specified interupt pin. */
void irq_DisableVector(irq_t irq);

/** @brief Return true IFF vector is currently enabled. */
bool irq_isEnabled(irq_t irq);
#endif

/** @brief Bind an interrupt to a device.
 *
 * Note that this is generally @em not used to bind the interrupt to a
 * particular end device. There are one or two devices in the kernel
 * for which that is true, but the more usual case is that this gets
 * invoked indirectly from user-land bus device drivers to bind the
 * interrupt source to the bus controller. Further demultiplexing to
 * the actual device will happen in user-mode code.
 *
 * In consequence, there is currently no provision in the kernel for
 * @em undoing this operation. */
void irq_Bind(irq_t irq, uint32_t mode, uint32_t level, VecFn fn);

/** @brief Given an IRQ, return the appropriate vector entry. */
VectorInfo *irq_MapInterrupt(irq_t irq);

/** @brief Handler to use prior to interrupt binding, as a sanity
 * provision. */
__hal void vh_UnboundIRQ(VectorInfo *vec, Process *inProc, 
			 fixregs_t *saveArea);

/** @brief Handler to use when an interrupt vector is bound. */
__hal void vh_BoundIRQ(VectorInfo *vec, Process *inProc, 
		       fixregs_t *saveArea);

#endif /* __KERNINC_VECTOR_H__ */
