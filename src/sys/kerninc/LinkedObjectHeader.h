#ifndef __KERNINC_LINKEDOBJECTHEADER_H__
#define __KERNINC_LINKEDOBJECTHEADER_H__

/*
 * Copyright (C) 2024, William ML Leslie.
 *
 * This file is part of the wleslie-os Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief Header that embeds a primary link.
 *
 * Objects that can naturally form part of a queue - a Process or a
 * Continuation - can use a LinkedObjectHeader as their header
 * structure, meaning that it's not necessary to decode the object's
 * type in order to traverse the list.
 *
 */

#include <kerninc/Queue.h>
#include <kerninc/ObjectHeader.h>

typedef struct LinkedObjectHeader {
  ObjectHeader hdr;
  ObjectLink link;

  ticket_t my_ticket;

  /** @brief Non-zero when we are in an extended IPC transaction with
   * a peer.
   *
   * WL TODO: We should use this to detect when a change to the endpoint
   * makes the send fail, but the receiver is still blocked waiting
   * for us to send.
   *
   * Invariant: ((ipcPeer == 0) || (p->ipcPeer->ipcPeer == p) */
  struct LinkedObjectHeader *  ipcPeer;


  /** @brief Ready queue to go on when we wake up.
   *
   * A runnable can have different scheduling clases. Depending on the
   * scheduling class, it will go on different ready queues when it is
   * ready to run. We cache a pointer to the appropriate ready queue
   * in the process structure.
   */
  struct Queue *readyQ;

  /** @brief the stall (or ready) queue we are presently on.
   *
   * If NULL, setting this field is protected by the Runnable's header's lock.
   * If non-NULL, setting or clearing this field is protected by onQ->qLock.
   */
  struct Queue *onQ;
} LinkedObjectHeader;


#endif /* __KERNINC_LINKEDOBJECTHEADER_H__ */
