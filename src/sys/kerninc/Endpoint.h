#ifndef __KERNINC_ENDPOINT_H__
#define __KERNINC_ENDPOINT_H__
/*
 * Copyright (C) 2006, The EROS Group, LLC.
 * Copyright (C) 2024, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file 
 * @brief Definition of Wrapper structure. */

#include <hal/kerntypes.h>
#include <kerninc/ObjectHeader.h>
#include <kerninc/Queue.h>
#include <obstore/Endpoint.h>
#include <sys/tree.h>

/** @brief Cached Endpoint structure.
 */
struct Endpoint {
  ObjectHeader      hdr;

  // as senders attempt to send to some target, they take a
  // strictly-increasing numerical ticket and are enqueued onto the
  // endpoint's stall queue.

  // we order the endpoints sending to some target by the value of the
  // first ticket in the endpoint's stall queue

  // if an endpoint contains queued senders and has an active target,
  // it should appear within the following trees.  the senders_seq
  // tree tracks the endpoints sorted by the chronological order of
  // the send requests of the next process, which is used for open
  // receive, and the epid tree sorts endpoints first by their
  // endpoint id, for use in closed receive.

  /** @brief heap entry holding this endpoints position by first ticket.
   *
   * That is, these endpoints are sorted by the chronological order of
   * the first sender in the stall queue.  This entry holds our
   * position in the heap.
   */
  RB_ENTRY(Endpoint) senders_seq;

  /** @brief heap entry holding this endpoints position by endpoint id.
   *
   * That is, these endpoints are sorted first by their endpoint id,
   * and then by the chronological order of the first sender in the
   * stall queue.  This entry holds our position in the heap.
   */
  RB_ENTRY(Endpoint) senders_epid;

  /** @brief the ticket held by the first entry in the stall queue. */
  ticket_t first_ticket;

  /** @brief the minimum valid ticket in queue for our given target.
   *
   * If this endpoint has senders on the stall queue, and it is then
   * pointed at a new target process, the ticket values of the
   * existing senders on the stall queue don't accurately represent
   * their position.  In particular, they could be numerically lower
   * than the target's ticket value at the time this endpoint was set
   * to point at it.
   *
   * To avoid weird priority inversion, we treat all of the senders we
   * have enqueued on this endpoint as if they all share the ticket
   * claimed when the endpoint was pointed at the new target.  We
   * track this value in min_ticket, and ensure that any senders
   * enqueued on this endpoint are treated as sending _after_ any
   * senders already enqueued on the target.
   *
   * Note that our ticket values could instead be _too high_, allowing
   * later senders to be handled earlier than any on our queue.  I
   * don't think this is quite the same problem at first glance, but
   * admit I could be wrong.
   */
  ticket_t min_ticket;

  /** @brief Queue of senders stalled on the endpoint.
   *
   * TODO: external state for this stall queue.
   */
  struct Queue queue;

  ExEndpoint        state;
};
typedef struct Endpoint Endpoint;

extern void endpoint_gc(Endpoint *);

#endif /* __KERNINC_ENDPOINT_H__ */
