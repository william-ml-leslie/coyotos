/* -*- mode: c -*- */

/*
 * Copyright (C) 2007, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Bits of x86 page table entry.
 *
 * I have intentionally left out the PAT bit, because it moves
 * depending on whether you are doing 2Mbyte pages or 4K pages. Turns
 * out the bit is irregular in the legacy mode too, and I never
 * noticed because I've never seen any good reason to use it on a
 * normal processor.
 *
 * Note that the G bit and the D bit have *never* been meaningful at
 * any level other than leaf entries, in spite of what the diagrams
 * would have you believe. The correct policy for these bits is only
 * to set them in leaf entries. The historical practice in EROS was
 * always to set the D bit on a writable page in order to suppress the
 * TLB write-back. For the moment, we continue that practice in
 * Coyotos, though I am no longer sure that we should
 *
 * Unfortunately, the A bit is updated by the TLB hardware to indicate
 * that the page table or page beneath has been referenced. Even
 * worse, the A bit is not implemented in regular fashion at all
 * levels under PAE. In EROS I simply preset the A bit at all
 * levels. In Coyotos I think that I will only do this bit at the leaf
 * level and eat the update costs at all other levels, since it won't
 * be visible for benchmarking purposes and it shouldn't impact
 * steady-state behavior much.
 *
 * The PCD, PWT bits are also available in CR3 in both normal and PAE
 * modes.
 */

/* Local Variables: */
/* comment-column:24 */
/* End: */
package IA32;

describe PTE 32 {
  bit   V       0;	/* present (a.k.a. valid) */
  bit   W       1;	/* writable */
  bit   USER    2;	/* user-accessable */
  bit   PWT     3;	/* page write through */
  bit   PCD     4;	/* page cache disable */
  bit   ACC     5;	/* accessed */
  bit   DRTY    6;	/* dirty */
  bit   PGSZ    7;	/* large page (PDE only, only if CR4.PSE) */
  bit   PAT     7;	/* page attribute table */
  bit   GLBL    8;	/* global page (PDE, PTE, only if CR4.PGE) */
  field SW      9 11;	/* software defined */
  bit   PAT4M   12;	/* page attribute table, 4M pages */

  field FRAME   12 31;	/* page frame number */
  field FRAME4M 22 31;	/* page frame number, 4M page */
}
