/*
 * Copyright (C) 2007, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <idl/coyotos/Builder.h>
#include <idl/coyotos/KernLog.h>

#include <coyotos/runtime.h>
#include <coyotos/kprintf.h>

#include <mki/testConstructor.h>

#include <string.h>

#define CR_LOG testConstructor_REG_KERNLOG

int
main(int argc, char *argv[])
{
  uint64_t type = 0;

  if (!coyotos_Constructor_create(testConstructor_REG_METACON,
				  testConstructor_REG_BANK,
				  testConstructor_REG_SCHED,
				  testConstructor_REG_RUNTIME,
				  testConstructor_REG_TMP))
    kprintf(CR_LOG, "create failed\n");
  else if (!coyotos_Cap_getType(testConstructor_REG_TMP, &type))
    kprintf(CR_LOG, "getType failed\n");

  else if (type != IKT_coyotos_Builder)
    kprintf(CR_LOG, "getType bad result\n");

  else
    kprintf(CR_LOG, "INFO: constructor IKT successful\n");
  
  kprintf(CR_LOG, "PASS: constructor test passes\n");

  *(uint64_t *)0 = 0;
  return 0;
}

