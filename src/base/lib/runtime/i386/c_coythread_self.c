/*
 * Copyright (C) 2022 William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stddef.h>
#include "coyotos/coythread.h"

// note that this may return a null pointer - in which case, the
// thread system has not been initialised.
//
// we punt on initialising the thread system in libc-coyotos, we would
// need access to allocate cappages and map them into our address
// space.  feel free to link libpthread which can do this for you.
coythread_t *coythread_self() {
  register coythread_t *self;
  __asm__ ("movl %%gs,%0" : "=r" (self));
  return self;
}

