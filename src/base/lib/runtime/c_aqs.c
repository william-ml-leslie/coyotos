/*
 * Copyright (C) 2022, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "coyotos/coythread.h"
#include "coyotos/coythread_config.h"
// #include "coyotos/sync.h"
#include "coyotos/machine/atomic.h"
#include "coyotos/aqs.h"

// Some implementation notes about AQS append.
//
// Given the invariant that any thread holding the lock is also on the
// queue, we first check the tail of the queue.  If we're the only
// item in the queue, that's great - we can STORE to the owner field.
// See AQS pop for the corresponding assignments.
//
// By the time you call aqs_append, you must have checked that you
// don't already hold the lock.  This doesn't need to be done with
// acquire semantics as a stale read of the owner field will never
// return self.
//
// The algorithm here is based (err, loosely) on one for linked list
// insert/delete by John D Valois in `Lock Free Data Structures`, but
// without a dummy 'head' element and without reference counting.  It
// is generally a bad idea to mess with a published and peer reviewed
// algorithm, but all the same arguments seem to apply to this
// implementation as they do to the original.  It is worth noting
// that, lacking a `SafeRead` function, we check the `waiting_on`
// field to confirm that we haven't added ourself to a Cons that has
// already been removed.

int aqs_append(aqs_queue_t *head, coythread_t *self) {
  atomic_write_ptr((_ATOMICPTR_T *) &self->waiting_on, head);
  do {
    self->wait_next = NULL;

    // coythread_t *next = atomic_read_ptr((_ATOMICPTR_T *) &&head->next);
    coythread_t *head_next = NULL;

    // the invariant we want to hold here is that the holder of the lock is
    // always on the queue starting at next; so that's what we'll try first.
    if (compare_and_swap_ptr((_ATOMICPTR_T *)&head->next, &head_next, self)) {
      // ok, we're the first one on the queue; we can take ownership.
      atomic_write_ptr((_ATOMICPTR_T *) &head->owner, self);
      return AQS_TAKEN;
    }

    coythread_t *prev = head_next;
    while (prev != NULL && prev != self) {
      coythread_t *current = prev;
      prev = NULL;
      // try to set current.wait_next = self.  if that fails, load
      // wait_next into prev.
      if (compare_and_swap_ptr((_ATOMICPTR_T *)&current->wait_next, &prev, self)) {
        // update the `next` pointer if it hasn't changed since we
        // read it.  if we fail to do so, confirm that `current` is
        // still waiting_on this lock.
        if (compare_and_swap_ptr((_ATOMICPTR_T *)&head->next, &head_next, self) ||
            atomic_read_ptr((_ATOMICPTR_T *) &current->waiting_on) == head) {
          return AQS_PARKED;
        }
        // if any of the above has failed, we need to start again from
        // the top.
        break;
      }
    }
  } while (1);
}

int aqs_try_x(aqs_queue_t *head, coythread_t *self) {
  self->wait_next = NULL;
  self->waiting_on = head;

  coythread_t *existing = NULL;
  if (compare_and_swap_ptr((_ATOMICPTR_T *)&head->next, &existing, self)) {
    atomic_write_ptr((_ATOMICPTR_T *) &head->owner, self);
    return AQS_TAKEN;
  }
  return AQS_FAILED;
}

coythread_t* aqs_pop(aqs_queue_t *head) {
  coythread_t *me = atomic_read_ptr((_ATOMICPTR_T *) &head->owner);
  if (!me) {
    // should we set an error?
    return NULL;
  }

  // if we are on a machine with load-linked/store-conditional, we should
  // instead do something like SC(&head->owner, LL(&me->wait_next))

  // clearing `waiting_on` will cause any outstanding attempts to queue
  // to retry until we smash head->next.
  me->waiting_on = NULL;

  // pop the next waiting item.
  coythread_t *result = __atomic_exchange_n(&me->wait_next, NULL,
                                            __ATOMIC_ACQ_REL);

  // if we are still at `next`, clobber that with the result.  it's
  // fine to fail here.  doing this before setting the owner preserves
  // our invariant that any valid owner must be on the queue.
  coythread_t *next = me;
  compare_and_swap_ptr((_ATOMICPTR_T *)&head->next, &next, result);
  
  if (result == NULL) {
    // now that we've clobbered `head->next` with possibly NULL, a
    // thread could have already set the `owner` variable, so we must
    // cmpxchg there too:
    next = me;
    compare_and_swap_ptr((_ATOMICPTR_T *)&head->owner, &next, NULL);
  } else {
    atomic_write_ptr((_ATOMICPTR_T *) &head->owner, result);
  }

  return result;
}

coythread_t* aqs_owner(aqs_queue_t *head) {
  return atomic_read_ptr((_ATOMICPTR_T *) &head->owner);
}
