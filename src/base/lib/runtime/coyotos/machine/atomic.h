#ifndef __COYOTOS_MACHINE_ATOMIC_H__
#define __COYOTOS_MACHINE_ATOMIC_H__
/** @file
 * @brief Automatically generated architecture wrapper header.
 */

#include <coyotos/machine/target.h>

#if (COYOTOS_ARCH == COYOTOS_ARCH_i386) || defined(COYOTOS_UNIVERSAL_CROSS)
#include "../i386/atomic.h"
#endif

#endif /* __COYOTOS_MACHINE_ATOMIC_H__ */
