#ifndef I386_COYOTOS_ATOMIC_H
#define I386_COYOTOS_ATOMIC_H
/*
 * Copyright (C) 2008, The EROS Group, LLC.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Structure declarations for atomic words and their
 * manipulators. 
*/

typedef struct {
  uint32_t  w;
} _ATOMIC32_T;

typedef struct {
  void *w;
} _ATOMICPTR_T;

/** @brief Atomic compare and swap.
 *
 * If current word value is @p oldval, replace with @p
 * newval. Regardless, return value of target word prior to
 * operation. */
static inline uint32_t 
compare_and_swap(_ATOMIC32_T *a, uint32_t oldval, uint32_t newval)
{
  uint32_t result;

  __asm__ __volatile__("lock cmpxchgl %1,%2"
		       : "=a" (result)
		       : "r" (newval), "m" (*((uint32_t *)(a))), "0" (oldval)
		       : "memory", "cc");
  return result;

}

/** @brief Atomic compare and swap for pointers */
static inline void *
compare_and_swap_ptr(_ATOMICPTR_T *ap, void *oldval, void *newval)
{
  void * result;

  __asm__ __volatile__("lock cmpxchgl %1,%2"
		       : "=a" (result)
		       : "r" (newval), "m" (*((uintptr_t *)(ap))), "0" (oldval)
		       : "memory", "cc");
  return result;

}

/** @brief Atomic word read. */
static inline uint32_t 
atomic_read(_ATOMIC32_T *a)
{
  return a->w;
}

/** @brief Atomic word write. */
static inline void
atomic_write(_ATOMIC32_T *a, uint32_t u)
{
  a->w = u;
}

/** @brief Atomic word read. */
static inline void * 
atomic_read_ptr(_ATOMICPTR_T *a)
{
  return a->w;
}

/** @brief Atomic word write. */
static inline void
atomic_write_ptr(_ATOMICPTR_T *a, void *u)
{
  a->w = u;
}

#endif /* I386_COYOTOS_ATOMIC_H */
