/*
 * Copyright (C) 2022, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// this structure describes each "thread", which is a coyotos process.
//
// platforms are expected to provide coythread_self() as a means for
// tracking which process maps to which coythread_t value.

#include <sys/types.h>
#include <coyotos/coytypes.h>

typedef struct coythread_t {
  int thread_id;
  caddr_t stack_start;
  caddr_t stack_end;
  caploc_t process;
  caploc_t notify;
  void *tls;
  void * volatile waiting_on;
  int thread_interrupted;
  // struct coythread_t *wait_prev;
  struct coythread_t * volatile wait_next;
  uint32_t error;
} coythread_t;

coythread_t *coythread_self(void);

