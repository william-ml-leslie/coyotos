/*
 * Copyright (C) 2022, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/** @brief Synchronisation primative: Abstract Queued Synchroniser
 *
 * libc-coyotos implements a simple AQS as a primitive to build locks and 
 * other synchronisation systems upon.  It's not optimial as a locking
 * primitive, specifically it requires two atomic compare-and-exchange
 * operations when taking the lock.  Nevertheless, it implements entirely
 * in userspace a locking strategy built on the AppNotice facility.
 *
 * The invariant held here is that the thread that holds the lock is always
 * on the queue.
 */

#define CACHE_LINE_SIZE 64

#define AQS_TAKEN 1
#define AQS_PARKED 2
#define AQS_FAILED 3

#define AQS_INITIALISER {}

// note that 'owner' is the owning thread, and that 'next' is the
// place you enqueue.  this means that when looping through the queue
// in order, you need to start from owner->wait_next.

typedef struct aqs_queue_t {
  coythread_t * volatile owner;
  union {
    struct details {
      // the remainder of this union can be used for various things.
      uint64_t level; // when used as a lock or semaphore.
      uint32_t flags; // generic configuration.
      union {
        void *ptr;
        uintptr_t val;
      } userdata; // when used as a logic/once variable.
    } details;
    unsigned char _cache_line[CACHE_LINE_SIZE - sizeof(coythread_t *)];
  };
  // this is the tail of the wait queue - cons here to be alerted when you
  // can take the lock.
  coythread_t * volatile next;
} aqs_queue_t;

int aqs_append(aqs_queue_t *head, coythread_t *self);

int aqs_try_x(aqs_queue_t *head, coythread_t *self);

coythread_t* aqs_pop(aqs_queue_t *head);

coythread_t* aqs_owner(aqs_queue_t *head);
