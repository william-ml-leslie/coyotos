#ifndef __COYOTOS_KPRINTF_H__
#define __COYOTOS_KPRINTF_H__

/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file 
 * @brief Kernel Log printf routine */

#include <stdarg.h>
#include <coyotos/capidl.h>
#include <coyotos/coytypes.h>

/** @brief Write pre-digested printf string to a KernLog
    capability. */
bool IDL_ENV_kvprintf(IDL_Environment *_env, caploc_t kernlog,
		      const char *fmt, va_list ap);

/** @brief Write string to a KernLog capability using specified IDL
 * environment. */
bool IDL_ENV_kprintf(IDL_Environment *_env, caploc_t kernlog,
		     const char *fmt, ...);

/** @brief Write string to a KernLog capability. */
bool kprintf(caploc_t kernlog, const char *fmt, ...);
bool kvprintf(caploc_t kernlog, const char *fmt, va_list listp);

/** @brief Write string to a buffer, without triggering any dependency
 * on stdio. */
size_t ksnprintf(char *buf, size_t len, const char *fmt, ...);
size_t kvsnprintf(char *buf, size_t len, const char *fmt, va_list listp);

#endif /* __COYOTOS_KPRINTF_H__ */
