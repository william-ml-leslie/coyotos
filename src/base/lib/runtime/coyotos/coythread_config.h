/*
 * Copyright (C) 2022, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// this header contains configuration constants used by libpthread.

#define COYTHREAD_INVALID_THREAD ((coythread_t *) 1)

#define COYTHREAD_SUCCESS 0
#define COYTHREAD_FAILURE 0xBEEF

#define COYTHREAD_INTERRUPTED 1
#define COYTHREAD_TERMINATING 2
#define COYTHREAD_LOCK_DESTROYED 4

#define COYOTOS_LOCK_RECURSIVE 0x100000

// flags for mutexes, not all of these are implemented yet.

#define COYOTOS_LOCK_SCHED_MASK 0xf
#define COYOTOS_LOCK_SCOPE_MASK 0xf0
#define COYOTOS_LOCK_JOIN_MASK 0xf00
#define COYOTOS_LOCK_INHERIT_MASK 0xf000
#define COYOTOS_LOCK_LIFECYCLE_MASK 0xf0000
#define COYOTOS_LOCK_TYPE_MASK 0xf00000

#define COYOTOS_LOCK_SCHED_FIFO     1
#define COYOTOS_LOCK_SCHED_RR       2
#define COYOTOS_LOCK_SCHED_OTHER    0
#define COYOTOS_LOCK_SCHED_BATCH    3
#define COYOTOS_LOCK_SCHED_IDLE     4
#define COYOTOS_LOCK_SCHED_DEADLINE 5

#define COYOTOS_LOCK_SCOPE_SYSTEM   0

#define COYOTOS_LOCK_DETACHED       0x100
#define COYOTOS_LOCK_JOINABLE       0

#define COYOTOS_LOCK_INHERIT_SCHED  0
#define COYOTOS_LOCK_EXPLICIT_SCHED 0x1000

#define COYOTOS_LOCK_LIFECYCLE_INVALID 0
#define COYOTOS_LOCK_LIFECYCLE_VALID   0x10000
#define COYOTOS_LOCK_LIFECYCLE_DYING   0x20000

#define NOTIFY_DESTROYED 1
#define NOTIFY_LOCK      2
