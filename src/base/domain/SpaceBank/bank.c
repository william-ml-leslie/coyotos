/*
 * Copyright (C) 2007, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief SpaceBank bank manipulation functions
 */

/*
 * Big Theory Statement on the Space Bank
 * --------------------------------------
 *
 * 1.  Background
 *
 * The SpaceBank is a translator between the kernel:
 *
 *   coyotos.Range:
 *     * range.get(type, OID) -> cap 
 *         Create a capability to object {type, OID}
 *
 *     * range.rescind(cap)
 *         Destroy all outstanding capabilities to the object
 *         referenced by cap, and resets the object to the initial
 *         zeroed state.
 *
 *     * range.identify(cap) -> type, OID
 *         Given a cap, retrieve the underlying object's type and OID
 *
 * And SpaceBank users:
 *
 *   coyotos.SpaceBank:
 *     * bank.createChild() -> newBank
 *         Create a new child bank
 *
 *     * bank.alloc(type, type, type) -> cap, cap, cap
 *         Allocate up to three objects, which are guarenteed to be in
 *         the initial zeroed state, and to not have any other outstanding
 *         capabilities which reference them.
 *
 *     * bank.free(count, cap, cap, cap)
 *         Free up to three objects, if they are owned by the invoked bank
 *
 *     * bank.destroy()
 *         Destroy the bank and all of its children and free all storage
 *         allocated from them.
 *
 *     * bank.remove()
 *         Destroy the bank and all of its children and reassign all
 *         storage to the bank's parent.
 *
 * This interface lets users not worry about the details of how objects
 * are stored on disk.  The SpaceBank is a fairly standard object allocator.
 * Any SpaceBank design needs to track four basic things:
 *
 *    1.  The contiguous extents of OIDs available for use
 *    2.  Which objects are available for allocation
 *    3.  Which bank each allocated object is associated with
 *    4.  The tree of banks, their limits and current usage
 *
 * The main difference between different SpaceBank designs is the overhead
 * in both processing and memory that the datastructures tracking this
 * information take.
 *
 * 2.  The Embedded SpaceBank
 *
 * Unlike the EROS SpaceBank, this design is for embedded systems.  The
 * main aim is simplicity and reliability.  Since the backing store is
 * just memory, no locality of reference issues need to be considered.
 *
 * The total number of available objects of each type is determined by the 
 * kernel before the SpaceBank starts running.  This allows us to have a
 * design which pre-allocates all needed storage during initialization,
 * and afterwards only manipulate existing objects.
 *
 * 2.1.  The basic objects of SpaceBank
 *
 * 2.1.1. The Object
 *
 *   An object represents a single object with a particular OID and type,
 *   and contains linkages and a Bank pointer which allow it to be
 *   either part of a Bank (as an outstanding allocation), or on
 *   its type's freelist (as an available free object).
 *
 *   The object does not directly contain its type or OID; it only
 *   contains a pointer to its containing extent.  The extent's @p
 *   obType field determine's the object's type, and the object's
 *   position in the extent's @p array added to the extent's @p
 *   baseOID determines its OID.
 *
 * 2.1.2. The Extent
 *
 *   An extent represents a contiguous range of OID values of a
 *   particular type.  The extent structures are pre-allocated in the
 *   data segment, which puts an upper bound on the total number of
 *   extents which can be supported.
 *
 *   Each extent is characterized by an @p obType, @p baseOID, and @p count,
 *   which determines the type and OID range the extent covers.  The
 *   extent also has an @p array, which contains <tt>Object</tt> structures
 *   for each OID the extent covers.  
 *
 *   For Endpoint extents, there is a parallel @p bankArray, which holds
 *   a Bank structure for each object.
 *
 * 2.1.3. The Bank
 *
 *   A bank is represented externally as a single Endpoint, whose EPID is
 *   its OID + 1.  The Protected Payload of the Entry capabilities to
 *   the bank is a bitset of the restrictions on that capability to the bank.
 *
 *   An array of Bank structures is allocated for every Endpoint extent,
 *   so no dynamic allocation is needed to create a new bank.
 *
 *   Banks form a tree, with the root being the PrimeBank.  The PrimeBank
 *   can never be destroyed, and its limits represent the amount of available
 *   objects in the system.
 *
 *   The bank has a circularly linked list of objects, whose head is
 *   pointed to by oList.  It also tracks the limits and usage of the
 *   bank for each object type.  The usage includes all allocations from
 *   sub-banks.
 *
 * 3.  Initialization
 *
 *   Because the Bank and Object arrays for the extents are dynamically sized,
 *   and the SpaceBank controlls all object allocation, the initialization of
 *   SpaceBank is a bit complex.
 *
 * 3.1.  Allocation subsystem initialization
 *
 *   The first thing the spacebank does is call alloc_init(), which
 *   sets up the allocation subsystem.  The main effect is to install
 *   a new address space root, with an l2v of 28, to give us space for our
 *   heap.  It also uses the Range key to map Page 0 (which contains the
 *   system image header) at a known location, referenced by @p image_header.
 *
 * 3.2.  Extent Setup
 *
 *   Next, the Extent array is set up, using coyotos.Range.nextBackedSubrange()
 *   to get the available extents from the kernel.  The Object and Bank arrays
 *   are left as NULL for now, since they require allocation.
 *
 *   Next, the preallocated field of all of the extents is set to the
 *   number of objects of the extent's type in the system image.
 *
 * 3.3.  Object and Bank array allocation
 *
 *   We then call allocate_objects_and_banks(), which allocates the Object
 *   and Bank arrays for all of the Extents.  The allocator uses require_Page()
 *   to allocate the necessary storage and require_GPT() to allocate the
 *   necessary address space metadata to map it.  Those routines use the
 *   "bootstrap" fields in the Page and GPT Extents to allocate objects
 *   before the Object arrays are set up.
 *
 * 3.4.  Loading the image's Object and Bank information
 *
 *   The mkimage image file contains information about the bank tree used
 *   to create the image, and a table of which bank allocated every object
 *   in the tree.  These are stored as pages, which are mapped using
 *   map_pages().
 *
 *   Since we are now done with the heap, alloc_finish() is called to disable
 *   further heap allocation.
 *
 *   setup_banks() and setup_allocations() load the information tables and
 *   set up the Bank and Objects to match them.
 *
 * 3.5.  Account for bootstrap allocations
 *
 *   We then call setup_bootstrap_allocations(), which assigns the Pages and
 *   GPTs we allocated during initialization to the PrimeBank.
 *
 * 3.6.  Free the image map (not yet implemented)
 *
 *   Now that everything is complete, the Pages we mapped from the image
 *   should be rescinded and returned to the Freelists.  This is not yet 
 *   implemented.  For bonus points, the GPTs used to map them should
 *   be freed as well. (Being careful not to unmap the Object/Bank arrays)
 *
 *   Page 0 could also be freed.
 */
#include "SpaceBank.h"
#include "heap.h"
#include "assert.h"
#include "debug.h"
#include <coyotos/kprintf.h>
#include <obstore/CoyImgHdr.h>

#define MAX_EXTENTS 20

Extent extents[MAX_EXTENTS];
size_t nExtents = 0;

Extent *extentByType[coyotos_Range_obType_otNUM_TYPES];

Object *freelistByType[coyotos_Range_obType_otNUM_TYPES];

Bank *primeBank;

oid_t prealloc_base[coyotos_Range_obType_otNUM_TYPES];

enum reserveType {
  RSV_FREE = 0,
  RSV_ALLOC = 1
};

static inline Object *
bank_getEndpoint(Bank *bank)
{
  Extent *ext = bank->extent;
  return &ext->array[bank - ext->bankArray];
}

static inline bool
bank_reserveSpace(Bank *bank, coyotos_Range_obType ty, size_t amount)
{
  Bank *cur;

  for (cur = bank; cur != 0; cur = cur->parent) {
    if (cur->usage[ty] + amount > cur->limits[ty])
      break;
    cur->usage[ty] += amount;
  }

  if (cur == 0)
    return true;

  /* failed; undo changes */
  while (bank != cur) {
    assert(bank->usage[ty] > 0);

    bank->usage[ty] -= amount;
    bank = bank->parent;
  }
  return false;
}

static inline void
bank_unreserveSpace(Bank *bank, coyotos_Range_obType ty, size_t amount)
{
  Bank *cur;

  for (cur = bank; cur != 0; cur = cur->parent) {
    assert(cur->usage[ty] > 0);
    cur->usage[ty] -= amount;
  }
}

static inline void
object_setAllocatedBank(Object *object, Bank *bank)
{
  coyotos_Range_obType ty = object->extent->obType;

  Object **listHead = (bank != 0)? &bank->oList : &freelistByType[ty];

  if (object->next != NULL) {
    Object **oldHead = (object->bank != 0)? &object->bank->oList :
      &freelistByType[ty];

    object->next->prev = object->prev;
    object->prev->next = object->next;
    if (*oldHead == object)
      *oldHead = (object->next == object) ? NULL : object->next;
    assert(*oldHead == NULL || (*oldHead)->bank == object->bank);
  }

  if (*listHead == 0) {
    object->next = object;
    object->prev = object;
    *listHead = object;
  } else {
    Object *head = *listHead;
    object->next = head;
    object->prev = head->prev;
    head->prev->next = object;
    head->prev = object;
  }

  DEBUG(allocdetail) {
    oid_t oid = object_getOid(object);
    kprintf(CR_KERNLOG, "Extent 0x%p, arr 0x%p, base 0x%llx sizeof(Object)=%d\n",
	    object->extent,
	    object->extent->array,
	    object->extent->baseOID,
	    sizeof(*object));
    kprintf(CR_KERNLOG, "Allocate 0x%p ty=%d oid=0x%llx to bank 0x%p\n",
	    object, ty, oid, bank);
  }
  object->bank = bank;
}

void
object_getCap(Object *obj, caploc_t out)
{
  coyotos_Range_obType type = object_getType(obj);
  oid_t oid = object_getOid(obj);

  MUST_SUCCEED(coyotos_Range_getCap(CR_RANGE, oid, type, out));
}

/** @brief Free an object and tell the kernel to rescind it */
void
object_rescindAndFree(Object *obj)
{
  assert(obj);
  assert(obj->bank);
  oid_t oid = object_getOid(obj);
  coyotos_Range_obType ty = object_getType(obj);

  MUST_SUCCEED(coyotos_Range_getCap(CR_RANGE, oid, ty, CR_TMP1));
  MUST_SUCCEED(coyotos_Range_rescind(CR_RANGE, CR_TMP1));

  // rescind obj and free it.
  bank_unreserveSpace(obj->bank, ty, 1);
  object_setAllocatedBank(obj, NULL);
}

static inline void
require_object(coyotos_Range_obType ty, caploc_t out)
{
  Extent *ext = extentByType[ty];

  assert(ext->bootstrapped >= 0);
  assert(ext->count > (ext->preallocated + ext->bootstrapped));

  oid_t oid = ext->baseOID + ext->preallocated + ext->bootstrapped;
  ext->bootstrapped++;

  MUST_SUCCEED(coyotos_Range_getCap(CR_RANGE, oid, ty, out));
}

void
require_Page(caploc_t out)
{
  require_object(coyotos_Range_obType_otPage, out);
}

void
require_GPT(caploc_t out)
{
  require_object(coyotos_Range_obType_otGPT, out);
}

void
get_pagecap(caploc_t out, oid_t oid)
{
  DEBUG(allocdetail)
    kprintf(CR_KERNLOG, "Trying to allocate pg oid=0x%llx\n", oid);
  MUST_SUCCEED(coyotos_Range_getCap(CR_RANGE,
				    oid,
				    coyotos_Range_obType_otPage,
				    out));
}

bool
bank_setLimits(Bank *bank, const coyotos_SpaceBank_limits *newLims)
{
  size_t idx;
  for (idx = 0; idx < coyotos_Range_obType_otNUM_TYPES; idx++)
    if (bank->usage[idx] > newLims->byType[idx])
      return false;

  for (idx = 0; idx < coyotos_Range_obType_otNUM_TYPES; idx++)
    bank->limits[idx] = newLims->byType[idx];

  return true;
}

void
bank_getUsage(Bank *bank, coyotos_SpaceBank_limits *out)
{
  size_t idx;
  for (idx = 0; idx < coyotos_Range_obType_otNUM_TYPES; idx++)
    out->byType[idx] = bank->usage[idx];
}

void
bank_getLimits(Bank *bank, coyotos_SpaceBank_limits *out)
{
  size_t idx;
  for (idx = 0; idx < coyotos_Range_obType_otNUM_TYPES; idx++)
    out->byType[idx] = bank->limits[idx];
}

void
bank_getEffLimits(Bank *bank, coyotos_SpaceBank_limits *out)
{
  Bank *cur;
  size_t idx;

  // Fill the out array with the number of available objects in bank.
  for (idx = 0; idx < coyotos_Range_obType_otNUM_TYPES; idx++)
    out->byType[idx] = -1ULL; // unlimited.

  // Find the most constrained values by walking up the parent chain.
  for (cur = bank->parent; cur != 0; cur = cur->parent) {
    for (idx = 0; idx < coyotos_Range_obType_otNUM_TYPES; idx++) {
      unsigned long long avail = cur->limits[idx] - cur->usage[idx];
      if (avail < out->byType[idx])
	out->byType[idx] = avail;
    }
  }

  // Add in the child bank's usage to get the effective limit, as opposed
  // to the # of objects available.
  for (idx = 0; idx < coyotos_Range_obType_otNUM_TYPES; idx++)
    out->byType[idx] += bank->usage[idx];
}

bool
bank_create(Bank *parent, caploc_t out)
{
  Object *endpt = bank_alloc(parent, coyotos_Range_obType_otEndpoint, out);
  if (endpt == 0)
    return false;

  Extent *ext = endpt->extent;
  Bank *bank = &ext->bankArray[endpt - ext->array];

  assert(bank->parent == 0 && bank != primeBank);

  // link it in to the bank hierarchy.
  bank->parent = parent;
  bank->nextSibling = parent->firstChild;
  parent->firstChild = bank;

  // set up the endpoint, and make the initial Entry cap.
  MUST_SUCCEED(coyotos_Endpoint_setRecipient(out, CR_SELF));
  // we offset the endpointID by one, since 0 is our reply endpoint.
  MUST_SUCCEED(coyotos_Endpoint_setEndpointID(out, object_getOid(endpt) + 1));
  // a protected payload of 0 gives all permissions
  MUST_SUCCEED(coyotos_Endpoint_makeEntryCap(out, 0, out));

  return true;
}

void
bank_destroy(Bank *bank, bool destroyObjects)
{
  Bank *parent = bank->parent;

  // don't destroy the PrimeBank
  assert(bank->parent != 0);

  while (bank != parent) {
    /* go down to a leaf child */
    while (bank->firstChild)
      bank = bank->firstChild;

    // unlink the bank from its siblings and parent bank.
    Bank **ptr;
    for (ptr = &bank->parent->firstChild;
	 *ptr != bank;
	 ptr = &(*ptr)->nextSibling)
      assert((*ptr) != 0);

    *ptr = bank->nextSibling;
    bank->nextSibling = 0;

    // Rescind the bank's endpoint, to make it so no one can use it.
    Object *endpoint = bank_getEndpoint(bank);
    assert(endpoint);
    assert(endpoint->bank);
    object_rescindAndFree(endpoint);

    if (destroyObjects) {
      // Rescind and free all objects in the bank
      Object *obj;
      while ((obj = bank->oList) != 0) {
	assert (obj->bank);
	object_rescindAndFree(obj);
      }

    } else {
      // move the objects in the bank into the parent
      // <b>of the destroyed bank</b>.

      // Note that all allocations have already been accounted for in the
      // parent, so no bookkeeping needs to happen.
      Object *obj;
      while ((obj = bank->oList) != 0)
	object_setAllocatedBank(obj, parent);
    }

    Bank *next = bank->parent;

    // clean up the bank
    bank->parent = 0;
    assert(bank->oList == 0);

    coyotos_Range_obType type;
    for (type = 0; type < coyotos_Range_obType_otNUM_TYPES; type++) {
      bank->limits[type] = -1ULL;
      bank->usage[type] = 0;
    }

    // set up for next loop
    bank = next;
  }
}

void
load_initial_extent(coyotos_Range_obType type)
{
  assert(nExtents < MAX_EXTENTS);
  Extent *ext = &extents[nExtents++];

  oid_t base = 0;
  oid_t bound = 0;

  MUST_SUCCEED(coyotos_Range_nextBackedSubrange(CR_RANGE, 0, type,
						&base, &bound));

  assert((size_t)(bound - base) == (bound - base));

  ext->baseOID = base;
  ext->count = bound - base;
  ext->obType = type;
  ext->nextExtent = extentByType[type];
  extentByType[type] = ext;
}

/** @brief Allocate the Object and Bank structures for the existing extents. */
void
allocate_objects_and_banks(void)
{
  size_t idx;
  size_t objidx;

  for (idx = 0; idx < nExtents; idx++) {
    Extent *ext = &extents[idx];
    Object *array = calloc(sizeof (*ext->array), ext->count);
    assert(array != 0);
    ext->array = array;
    for (objidx = 0; objidx < ext->count; objidx++) {
      array[objidx].extent = ext;
      // add to freelist
      object_setAllocatedBank(&array[objidx], NULL);
    }
    if (ext->obType != coyotos_Range_obType_otEndpoint)
      ext->bankArray = NULL;
    else {
      Bank *bankArray = calloc(sizeof (*ext->bankArray), ext->count);
      assert(bankArray != 0);
      ext->bankArray = bankArray;
      for (objidx = 0; objidx < ext->count; objidx++) {
	bankArray[objidx].extent = ext;
	int ty;
	for (ty = 0; ty < coyotos_Range_obType_otNUM_TYPES; ty++)
	  bankArray[objidx].limits[ty] = -1ULL;  /* unlimited */
      }
    }
  }
}

/** @brief locate Object structure for @p type and @p oid */
static Object *
lookup_object(coyotos_Range_obType ty, oid_t oid)
{
  assert(ty < coyotos_Range_obType_otNUM_TYPES);

  Extent *ext;

  DEBUG(allocdetail)
    kprintf(CR_KERNLOG, "Look up ty=%d, oid=0x%llx\n", 
	    ty, oid);

  for (ext = extentByType[ty]; ext != NULL; ext = ext->nextExtent) {
    assert(ext->obType == ty);
    if (oid >= ext->baseOID && (oid - ext->baseOID) < ext->count)
      return &ext->array[oid - ext->baseOID];
  }
  return (0);
}

/** @brief Find the Bank structure for oid @p oid */
static Bank *
lookup_bank(oid_t oid)
{
  coyotos_Range_obType ty = coyotos_Range_obType_otEndpoint;

  Extent *ext;

  for (ext = extentByType[ty]; ext != NULL; ext = ext->nextExtent) {
    assert(ext->obType == ty);
    assert(ext->bankArray != 0);
    if (oid >= ext->baseOID && (oid - ext->baseOID) < ext->count)
      return (&ext->bankArray[oid - ext->baseOID]);
  }
  return (0);
}

Bank *
bank_fromEPID(oid_t oid)
{
  // de-bias the OID
  Bank *bank = lookup_bank(oid - 1);
  // if bank does not exist or is not allocated, fail
  if (bank == 0 || (bank->parent == 0 && bank != primeBank))
    return 0;

  return bank;
}
/** @brief Setup the initial preallocation of the initial extent, as well
 * as the bootstrapping allocation
 */
static void
setup_prealloc(coyotos_Range_obType ty, size_t count)
{
  Extent *ext = extentByType[ty];
  assert(ext->baseOID == 0);
  assert(ext->count > count);

  ext->preallocated = count;
  ext->bootstrapped = 0;
}

Object *
bank_do_alloc(Bank *bank, coyotos_Range_obType ty, caploc_t out)
{
  bool result = bank_reserveSpace(bank, ty, 1);

  if (!result)
    return 0;

  Object *obj = freelistByType[ty];
  object_setAllocatedBank(obj, bank);
  return obj;
}

Object *
bank_alloc(Bank *bank, coyotos_Range_obType ty, caploc_t out)
{
  Object *obj = bank_do_alloc(bank, ty, out);
  if (obj) {
    oid_t oid = object_getOid(obj);
    MUST_SUCCEED(coyotos_Range_getCap(CR_RANGE, oid, ty, out));
  }
  return obj;
}

Object *
bank_alloc_proc(Bank *bank, caploc_t brand, caploc_t out)
{
  Object *obj = bank_do_alloc(bank, coyotos_Range_obType_otProcess, out);
  if (obj) {
    oid_t oid = object_getOid(obj);
    MUST_SUCCEED(coyotos_Range_getProcess(CR_RANGE, oid, brand, out));
  }
  return obj;
}

void
bank_getEntry(Bank *bank, coyotos_SpaceBank_restrictions restr, caploc_t out)
{
  Object *endpt = bank_getEndpoint(bank);
  MUST_SUCCEED(coyotos_Range_getCap(CR_RANGE,
				    object_getOid(endpt),
				    object_getType(endpt),
				    out));

  MUST_SUCCEED(coyotos_Endpoint_makeEntryCap(out, restr, out));

}

void
object_unalloc(Object *obj)
{
  Bank *bank = obj->bank;
  assert(bank);

  bank_unreserveSpace(bank, object_getType(obj), 1);
  object_setAllocatedBank(obj, NULL);
}

Object *
object_identify(caploc_t cap)
{
  coyotos_Range_obType ty = coyotos_Range_obType_otNUM_TYPES;
  oid_t oid;

  if (!coyotos_Range_identify(CR_RANGE, cap, &ty, &oid))
    return 0;
  if (ty == coyotos_Range_obType_otInvalid)
    return 0;

  DEBUG(allocdetail)
    kprintf(CR_KERNLOG, "Identify object as ty=%d oid=0x%llx\n",
	    ty, oid);

  assert(ty >= 0 && ty < coyotos_Range_obType_otNUM_TYPES);

  return lookup_object(ty, oid);
}

Object *
bank_alloc_contiguous(Bank *bank, caploc_t out, size_t l2g) {
  size_t n_pages = 1 << (l2g - COYOTOS_PAGE_ADDR_BITS);
  bool result = bank_reserveSpace(bank, coyotos_Range_obType_otPage, n_pages);

  if (!result)
    return NULL;

  Extent *ext = extentByType[coyotos_Range_obType_otPage];

  size_t start_page = -1;
  size_t pages_found = 0;
  for (size_t page = 0; page < ext->count; page++) {
    if (ext->array[page].bank == NULL) {
      if (pages_found > 0) {
        pages_found++;
        if (pages_found >= n_pages) {
          break;
        }
      } else {
        start_page = page;
        pages_found = 1;
      }
    } else {
      pages_found = 0;
    }
  }

  if (pages_found < n_pages) {
    return NULL;
  }

  for (size_t page = start_page; page < start_page + n_pages; page++) {
    object_setAllocatedBank(&ext->array[page], bank);
  }

  Object *obj = &ext->array[start_page];

  oid_t oid = object_getOid(obj);
  MUST_SUCCEED(coyotos_Range_getCap(CR_RANGE, oid, coyotos_Range_obType_otPage, out));
  MUST_SUCCEED(coyotos_Range_resizePage(CR_RANGE, out, make_guard(0, l2g)));
  return obj;
}

/** @brief Set up the banks from the Bank array supplied from mkimage,
 * Located at @p banks and @p count
 *
 * Also sets up their Endpoints correctly.
 */
void
setup_banks(const CoyImgBank *banks, size_t count)
{
  size_t idx;

  DEBUG(init) kprintf(CR_KERNLOG, "Call to setup_banks\n");

  for (idx = 0; idx < count; idx++) {
    oid_t oid = banks[idx].oid;
    bankid_t parentId = banks[idx].parent;

    /*
     * The parent must have already been set up.  This guarantees that the
     * bank tree is well-formed. (i.e. a tree rooted at primeBank)
     */
    if (parentId > 0)
      assert3(parentId, <, (bankid_t)idx);

    oid_t parentOid = banks[parentId].oid;

    Bank *bank = lookup_bank(oid);
    Bank *parent = lookup_bank(parentOid);

    // they both must exist
    assert3p(bank, !=, (void *) 0);
    assert3p(parent, !=, (void *) 0);

    // link the bank into its parent chain
    if (bank == parent) {
      assert(primeBank == 0);
      primeBank = bank;
      bank->parent = 0;
    } else {
      bank->parent = parent;
      bank->nextSibling = parent->firstChild;
      parent->firstChild = bank;
    }

    // Get the endpoint, point it at us, and set its endpoint ID appropriately.
    MUST_SUCCEED(coyotos_Range_getCap(CR_RANGE,
				      oid,
				      coyotos_Range_obType_otEndpoint,
				      CR_TMP1));

    MUST_SUCCEED(coyotos_Endpoint_setRecipient(CR_TMP1, CR_SELF));
    // we offset the endpoint ID by 1, since 0 is our reply endpoint.
    MUST_SUCCEED(coyotos_Endpoint_setEndpointID(CR_TMP1, oid + 1));
  }

  DEBUG(init) kprintf(CR_KERNLOG, "Bank setup loop complete\n");

  // There must have been a primeBank somewhere.
  assert(primeBank != 0);

  // set up prime bank's limits to match the range counts.
  size_t ty;
  for (ty = 0; ty < coyotos_Range_obType_otNUM_TYPES; ty++) {
    primeBank->limits[ty] = 0;
    Extent *ext;
    for (ext = extentByType[ty]; ext != 0; ext = ext->nextExtent)
      primeBank->limits[ty] += ext->count;
  }

  DEBUG(init) kprintf(CR_KERNLOG, "Exit setup_banks\n");
}

/** @brief Actually do the initial allocations described in the CoyImgAlloc
 * array @p alloc of size @p count, which was created by mkimage.
 */
static void
setup_allocations(CoyImgAlloc *alloc, size_t count)
{
  size_t idx;

  for (idx = 0; idx < count; idx++) {
    oid_t oid = alloc[idx].oid;
    bankid_t bankid = alloc[idx].bank;
    coyotos_Range_obType ty = alloc[idx].fType;

    Bank *bank = lookup_bank(bankid);
    Object *obj = lookup_object(ty, oid);

    MUST_SUCCEED(bank_reserveSpace(bank, ty, 1));

    object_setAllocatedBank(obj, bank);
  }
}

/** @brief Actually do the bootstrap allocations, allocating them to
 * the Prime Bank.
 */
static void
setup_bootstrap_allocations(void)
{
  size_t idx;

  for (idx = 0; idx < nExtents; idx++) {
    Extent *ext = &extents[idx];

    size_t objIdx;
    assert(ext->bootstrapped != -1ul);
    for (objIdx = 0; objIdx < ext->bootstrapped; objIdx++) {
      Object *obj = &ext->array[ext->preallocated + objIdx];

      assert(obj->bank == NULL);
      // treat it as allocated by the primeBank
      MUST_SUCCEED(bank_reserveSpace(primeBank, object_getType(obj), 1));
      object_setAllocatedBank(obj, primeBank);
    }
    ext->bootstrapped = -1ul;
  }
}

void
initialize(void)
{
  coyotos_Range_obType ty;

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: initializing\n");

  heap_init();

  /* Set up a read-only Page zero mapping so that we can access the
     CoyImgHdr. */
  const struct CoyImgHdr *image_header = mmap_constant_pages(0, 1);

  for (ty = 0; ty < coyotos_Range_obType_otNUM_TYPES; ty++)
    load_initial_extent(ty);

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: initial extents loaded\n");

  setup_prealloc(coyotos_Range_obType_otPage, image_header->nPage);
  setup_prealloc(coyotos_Range_obType_otCapPage, image_header->nCapPage);
  setup_prealloc(coyotos_Range_obType_otGPT, image_header->nGPT);
  setup_prealloc(coyotos_Range_obType_otEndpoint, image_header->nEndpoint);
  setup_prealloc(coyotos_Range_obType_otProcess, image_header->nProc);

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: prealloc_setup\n");

  // Now that we've reserved the initially allocated objects, we can start
  // allocating storage and mapping tables.
  allocate_objects_and_banks();

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: objects and banks allocated\n");

  DEBUG(init)
    kprintf(CR_KERNLOG, "Mapping [0x%llx,0x%llx]\n", 
	    image_header->bankVecOID, image_header->endVecOID);

  const void *metadataMap = 
    mmap_pages(image_header->bankVecOID, image_header->endVecOID);

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: map_pages done\n");

  heap_freeze();

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: alloc finished\n");
  
  // from now on, no allocate_bytes() or map_pages() may occur.

  setup_banks((const CoyImgBank *)metadataMap, image_header->nBank);

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: banks set up\n");

  uintptr_t allocVecBase = (uintptr_t)metadataMap +
    COYOTOS_PAGE_SIZE * (image_header->allocVecOID - image_header->bankVecOID);

  setup_allocations((CoyImgAlloc *)allocVecBase, image_header->nAlloc);

  DEBUG(init) kprintf(CR_KERNLOG, "SpaceBank: allocations set up\n");

  // Mark the pages and GPTs we allocated bootstrapping this operation as
  // allocated from the Prime Bank.
  setup_bootstrap_allocations();

  // at this point, our ranges, banks, and freelists are fully consistent.

  /// @bug at some point, we should rescind and free the metadata pages, and
  /// possibly the GPTs which point to them.
}

