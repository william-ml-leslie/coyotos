/*
 * Copyright (C) 2007, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief SpaceBank debugging support file
 */

#include "SpaceBank.h"
#include <stdlib.h>
#include <string.h>
#include <coyotos/kprintf.h>

void
__assert3_fail(const char *file, int lineno, const char *desc, uint64_t val1, uint64_t val2)
{
  kprintf(CR_KERNLOG, 
	  "SpaceBank: assertion failed: %s:%d: %lld %s %lld\n\n",
	  file,
	  lineno,
	  val1,
	  desc,
	  val2);

  exit(1);
}

void
__assert_fail(const char *file, int lineno, const char *desc)
{
  kprintf(CR_KERNLOG, 
	  "SpaceBank: assertion failed: %s:%d: %s\n\n",
	  file,
	  lineno,
	  desc);

  exit(1);
}

