/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* Implements the following interfaces:
 *   coyotos.TermIoStream
 *   coyotos.driver.IrqCallback
 */

#include <coyotos/capidl.h>
#include <coyotos/syscall.h>
#include <coyotos/runtime.h>
#include <coyotos/reply_create.h>

#include <idl/coyotos/SpaceBank.h>
#include <idl/coyotos/Endpoint.h>
#include <idl/coyotos/AddressSpace.h>
#include <idl/coyotos/Constructor.h>

/* Utility quasi-syntax */
#define unless(x) if (!(x))

#include "mki/coyotos.driver.TTY.h"
#include "termios.h"

#include <idl/coyotos/TermIoStream.server.h>
#include <idl/coyotos/driver/IrqCallback.server.h>

typedef union {
  _IDL_IFUNION_coyotos_TermIoStream
      coyotos_TermIoStream;
  _IDL_IFUNION_coyotos_driver_IrqCallback
      coyotos_driver_IrqCallback;
  InvParameterBlock_t pb;
  InvExceptionParameterBlock_t except;
  uintptr_t icw;
} _IDL_GRAND_SERVER_UNION;

#define CR_LOG		 coyotos_driver_TTY_APP_LOG
#define CR_Helper	 coyotos_driver_TTY_APP_Helper
#define CR_CallBack	 coyotos_driver_TTY_APP_CallBack
#define CR_wChannel	 coyotos_driver_TTY_APP_wChannel
#define CR_rChannel	 coyotos_driver_TTY_APP_rChannel
#define CR_TMP0		 coyotos_driver_TTY_APP_TMP0

/* You should supply a function that selects an interface
 * type based on the incoming endpoint ID and protected
 * payload */
static inline uint64_t
choose_if(uint64_t epID, uint32_t pp)
{
  if (pp == 0)
    return IKT_coyotos_TermIoStream;
  else
    return IKT_coyotos_driver_IrqCallback;
}

/* The IDL_SERVER_Environment structure type is something
 * that you should extend to hold any "extra" information
 * you need to carry around in your handlers. CapIDL code
 * will pass this pointer along, but knows absolutely
 * nothing about the contents of the structure.
 *
 * The following structure definition is provided as a
 * starting point. It is generally a good idea to pass the
 * received protected payload and endpoint ID to handler
 * functions so that they know what object was invoked and
 * with what permissions.
 */
typedef struct IDL_SERVER_Environment {
  uint32_t pp;
  uint64_t epID;
} IDL_SERVER_Environment;

/* Handlers for coyotos.driver.IrqCallback */
IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_driver_IrqCallback_onInterrupt(
  coyotos_IrqCtl_irq_t irq,
  struct IDL_SERVER_Environment *_env)
{
  /* Fill in with action */
  return RC_coyotos_Cap_RequestError;
}

/* Handlers for coyotos.TermIoStream */
IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_TermIoStream_makeraw(
  struct IDL_SERVER_Environment *_env)
{
  termios_make_raw();

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_TermIoStream_makecooked(
  struct IDL_SERVER_Environment *_env)
{
  termios_make_cooked();

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_TermIoStream_setattr(
  coyotos_TermIoStream_termios t,
  struct IDL_SERVER_Environment *_env)
{
  termios_setattr(&t);

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_TermIoStream_getattr(
  coyotos_TermIoStream_termios *_retVal,
  struct IDL_SERVER_Environment *_env)
{
  termios_getattr(_retVal);

  return RC_coyotos_Cap_OK;
}

/* Handlers for coyotos.IoStream */
IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_IoStream_doRead(
  uint32_t length,
  coyotos_IoStream_chString *s,
  struct IDL_SERVER_Environment *_env)
{
  /* Fill in with action */
  return RC_coyotos_Cap_RequestError;
}

IDL_SERVER_CLEANUP_PREDECL void
CLEANUP_coyotos_IoStream_doRead(
  coyotos_IoStream_chString s,
  struct IDL_SERVER_Environment *_env)
{
  /* fill in with cleanups */
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_IoStream_getReadChannel(
  caploc_t _retVal,
  struct IDL_SERVER_Environment *_env)
{
  cap_copy(_retVal, CR_rChannel);

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_IoStream_getWriteChannel(
  caploc_t _retVal,
  struct IDL_SERVER_Environment *_env)
{
  cap_copy(_retVal, CR_wChannel);

  return RC_coyotos_Cap_OK;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_IoStream_doWrite(
  coyotos_IoStream_chString s,
  uint32_t *_retVal,
  struct IDL_SERVER_Environment *_env)
{
  /* Fill in with action */
  return RC_coyotos_Cap_RequestError;
}

/* Handlers for coyotos.Cap */
IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_Cap_destroy(
  struct IDL_SERVER_Environment *_env)
{
  /*
   * ** UNCOMMENT and update if destroy can be refused **
   * if (destroy not allowed)
   *   return RC_coyotos_Cap_NoAccess;
   */
  unless (coyotos_SpaceBank_destroyBankAndReturn(CR_SPACEBANK,
                                                 CR_RETURN,
                                                 RC_coyotos_Cap_OK)) {
    return IDL_exceptCode;
  }
  /* Not reached */
  return RC_coyotos_Cap_RequestError;
}

IDL_SERVER_HANDLER_PREDECL uint64_t
HANDLE_coyotos_Cap_getType(
  uint64_t *_retVal,
  struct IDL_SERVER_Environment *_env)
{
  *_retVal = choose_if(_env->epID, _env->pp);
  return RC_coyotos_Cap_OK;
}

/* ProcessRequests implements the main Event Loop */
void
ProcessRequests(struct IDL_SERVER_Environment *_env)
{
  _IDL_GRAND_SERVER_UNION gsu;
  
  gsu.icw = 0;
  gsu.pb.sndPtr = 0;
  gsu.pb.sndLen = 0;
  
  for(;;) {
    gsu.icw &= (IPW0_LDW_MASK|IPW0_LSC_MASK
        |IPW0_SG|IPW0_SP|IPW0_SC|IPW0_EX);
    gsu.icw |= IPW0_MAKE_NR(sc_InvokeCap)|IPW0_RP|IPW0_AC
        |IPW0_MAKE_LRC(3)|IPW0_NB|IPW0_CO;
    
    gsu.pb.u.invCap = CR_RETURN;
    gsu.pb.rcvCap[0] = CR_RETURN;
    gsu.pb.rcvCap[1] = CR_ARG0;
    gsu.pb.rcvCap[2] = CR_ARG1;
    gsu.pb.rcvCap[3] = CR_ARG2;
    gsu.pb.rcvBound = (sizeof(gsu) - sizeof(gsu.pb));
    gsu.pb.rcvPtr = ((char *)(&gsu)) + sizeof(gsu.pb);
    
    invoke_capability(&gsu.pb);
    
    _env->pp = gsu.pb.u.pp;
    _env->epID = gsu.pb.epID;
    
    /* Re-establish defaults. Note we rely on the handler proc
     * to decide how MANY of these caps will be sent by setting ICW.SC
     * and ICW.lsc fields properly.
     */
    gsu.pb.sndCap[0] = CR_REPLY0;
    gsu.pb.sndCap[1] = CR_REPLY1;
    gsu.pb.sndCap[2] = CR_REPLY2;
    gsu.pb.sndCap[3] = CR_REPLY3;
    
    /* We rely on the (de)marshaling procedures to set sndLen to zero
     * if no string is to be sent. We cannot zero it preemptively here
     * because sndLen is an IN parameter telling how many bytes we got.
     * Set sndPtr to zero so that we will fault if this is mishandled.
     */
    gsu.pb.sndPtr = 0;
    
    if ((gsu.icw & IPW0_SC) == 0) {
      /* Protocol violation -- reply slot unpopulated. */
      gsu.icw = 0;
      gsu.pb.sndLen = 0;
      continue;
    }
    
    switch(choose_if(gsu.pb.epID, gsu.pb.u.pp)) {
    case IKT_coyotos_TermIoStream:
      _IDL_IFDISPATCH_coyotos_TermIoStream(&gsu.coyotos_TermIoStream, _env);
      break;
    case IKT_coyotos_driver_IrqCallback:
      _IDL_IFDISPATCH_coyotos_driver_IrqCallback(&gsu.coyotos_driver_IrqCallback, _env);
      break;
    default:
      {
        gsu.except.icw =
          IPW0_MAKE_LDW((sizeof(gsu.except)/sizeof(uintptr_t))-1)
          |IPW0_EX|IPW0_SP;
        gsu.except.exceptionCode = RC_coyotos_Cap_UnknownRequest;
        gsu.pb.sndLen = 0;
        break;
      }
    }
  }
}

static inline bool
exit_gracelessly(errcode_t errCode)
{
  return
    coyotos_SpaceBank_destroyBankAndReturn(CR_SPACEBANK,
                                           CR_RETURN,
                                           errCode);
}

/// There is a moderately bad problem here, which is that the current
/// design of TTY doesn't deal with interrupt negotiation. That's okay
/// for a provisional implementation, but we will need in due course
/// to rebuilt the initialization logic.
bool
initialize()
{
  unless(
	 coyotos_AddressSpace_getSlot(CR_TOOLS, 
				      coyotos_driver_TTY_TOOL_HelperCon,
				      CR_Helper) &&

	 coyotos_AddressSpace_getSlot(CR_TOOLS, 
				      coyotos_driver_TTY_TOOL_LOG,
				      CR_LOG) &&

	 coyotos_Constructor_simpleCreate(CR_Helper, CR_NULL, CR_NULL, CR_Helper) &&

         /* Set up our call-back capability */
         coyotos_Endpoint_makeEntryCap(CR_INITEPT,
				       coyotos_driver_TTY_PP_CallBack,
                                       CR_CallBack) &&

         /* Set up our entry capability */
         coyotos_Endpoint_makeEntryCap(CR_INITEPT,
				       coyotos_driver_TTY_PP_ReadWrite,
                                       CR_REPLY0)
         )
    exit_gracelessly(IDL_exceptCode);
  
  /* Send our entry cap to our caller */
  REPLY_create(CR_RETURN, CR_REPLY0);
  
  return true;
}

int
main(int argc, char *argv[])
{
  struct IDL_SERVER_Environment ise;
  
  if (!initialize())
    return 0;
  
  ProcessRequests(&ise);
  
  return 0;
}
