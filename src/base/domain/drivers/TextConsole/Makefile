#
# Copyright (C) 2007, The EROS Group, LLC.
#
# This file is part of the Coyotos Operating System.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#

default: package
COYOTOS_SRC=../../../..
CROSS_BUILD=yes

CFLAGS+=-g -O

INC=-I. -I$(COYOTOS_ROOT)/usr/include -I$(BUILDDIR) -I../../../sys
SOURCES=$(wildcard *.c)
OBJECTS=$(patsubst %.c,$(BUILDDIR)/%.o,$(wildcard *.c))
TARGETS=$(BUILDDIR)/TextConsole
# note that we have to specify libcoyotos-small before libidl,
# otherwise it doesn't have a dependency on GPT.o
LIBS=-lcoyotos-small -lidl-sys -lidl-base

include $(COYOTOS_SRC)/build/make/makerules.mk

ENUM_MODULES=coyotos.driver.TextConsole coyotos.TargetInfo
ENUM_HDRS=$(ENUM_MODULES:%=$(BUILDDIR)/mki/%.h)

# Note on make: Using secondary expansion, it is possible in abstract
# to do a minimal rebuild of enum headers, but it hardly seems worth it.

# To generate enum headers from MKI files:
$(ENUM_HDRS): $(MKIMAGE) | $(BUILDDIR)/mki
	$(RUN_MKIMAGE) --MD -H $(BUILDDIR)/mki $(ENUM_MODULES)

$(OBJECTS): $(ENUM_HDRS)

install all: $(TARGETS)

install: all
	$(INSTALL) -d $(COYOTOS_ROOT)/usr/domain/coyotos/driver
	$(INSTALL) -m 0755 $(TARGETS) $(COYOTOS_ROOT)/usr/domain/coyotos/driver

$(BUILDDIR)/TextConsole: $(OBJECTS)
	$(GCC) -small-space $(GPLUSFLAGS) $(OBJECTS) $(LIBS) $(STDLIBDIRS) -o $@

-include $(BUILDDIR)/.*.m $(BUILDDIR)/mki/.*.m
