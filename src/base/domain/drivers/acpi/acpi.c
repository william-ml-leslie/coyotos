/*
 * Copyright (C) 2007, The EROS Group, LLC.
 * Copyright (C) 2021, William ML Leslie.
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 * @brief A program for starting the system drivers from ACPI.
 *
 * It detects what hardware is supported, and starts drivers to handle
 * hardware described by ACPI, most notably, any sort of PCI bus.
 */

#include <stdint.h>
#include <string.h>

#include <idl/coyotos/AddressSpace.h>
#include <idl/coyotos/Process.h>
#include <coyotos/kprintf.h>
#include <coyotos/runtime.h>
#include <mki/coyotos.driver.ACPI.h>

#include <acpi.h>

// some fixed mappings in our address space

#define PMAP_START 0x10000000
#define BDA (char *) PMAP_START
#define BIOS_ROM (char *) (PMAP_START + 0x10 * COYOTOS_PAGE_SIZE)

/**
 * @brief Compute min(a, b) in a type-safe, single-evaluation form.  
 *
 * This must be a macro because it's type-indifferent.
 */
#define min(a, b) \
  ({ \
    __typeof(a) tmpa = (a); \
    __typeof(b) tmpb = (b); \
    (((tmpa) < (tmpb)) ? (tmpa) : (tmpb)); \
  })

RSDP_v3 *rsdp;



void *
ptov(uintptr_t pa) {
  if (pa < 0x1000) {
    return BDA + pa;
  }
  if (pa >= 0xe0000 && pa < 0xf0000) {
    return BIOS_ROM + pa - 0xe0000;
  }
  return pmap_ptov(pa);
}

static uint8_t
acpi_cksum(uintptr_t pa, size_t len)
{
  uint8_t sum = 0;

  while (len) {
    uintptr_t offset = pa & COYOTOS_PAGE_ADDR_MASK;
    pa -= offset;

    char *va = ptov(pa);
    if (va == NULL) {
      return 1;
    }
    char *ptr = va + offset;
    size_t nBytes = min(len, COYOTOS_PAGE_SIZE - offset);

    for (size_t i = 0; i < nBytes; i++)
      sum += ptr[i];

    len -= nBytes;
  }

  return sum;
}

static RSDP_v3*
acpi_scan_region(uintptr_t ptr, uintptr_t bound)
{
  while (ptr < bound) {
    uintptr_t rsdp_pa = ptr;
    RSDP_v3 *rsdp_va = (RSDP_v3 *)ptov(ptr);
    if (rsdp_va == NULL) {
      return NULL;
    }
    ptr += 16;

    if (rsdp_pa > 0xe0000 && rsdp_pa < 0x100000) {
      // kprintf(CR_KERNLOG, "pa: %P (%P)", rsdp_pa, rsdp_va);
    }

    if (memcmp((char *) rsdp_va->signature, "RSD PTR ", 8) != 0)
      continue;

    /* Test the v1.0 checksum */
    if (acpi_cksum(rsdp_pa, 20))
      continue;

    /* Test the v3.0 checksum */
    if (rsdp_va->revision >= 2 && acpi_cksum(rsdp_pa, rsdp_va->length))
      continue;

    /* Checksum checks out. Test if the rsdt pointer actually points
     * as an RSDT structure by testing that signature. */
    char *rsdt_sig = ptov(rsdp_va->rsdt);
    if (rsdt_sig != NULL && memcmp(rsdt_sig, "RSDT", 4) != 0)
      continue;

    /* Found it (we think): */
    return rsdp_va;
  }

  return 0;			/* not found */
}

typedef struct RSDT {
  SDT_hdr header;
  uint32_t tables[32];
} RSDT;

bool initialise_rsdt(uintptr_t rsdt_pa) {
  RSDT* rsdt = (RSDT*) ptov(rsdt_pa);
  kprintf(CR_KERNLOG, "RSDT length: %d\n", rsdt->header.length);
  if (acpi_cksum(rsdt_pa, rsdt->header.length)) {
    kprintf(CR_KERNLOG, "RSDT is not valid.\n", rsdt->header.length);
    return false;
  }
  
  uint8_t name[5] = {0, 0, 0, 0, 0};
  size_t entries = (rsdt->header.length - sizeof(SDT_hdr)) / 4;
  for (size_t tabn = 0; tabn < entries; tabn++) {
    uint32_t *table_index = (uint32_t*) ptov(rsdt_pa + sizeof(SDT_hdr) +
                                             tabn * sizeof(uint32_t));
    SDT_hdr* table = (SDT_hdr*) ptov(*table_index);
    for (int i = 0; i < 4; i++) {
      char c = table->signature[i];
      if ((c >= ' ' && c <= '~')) {
        name[i] = c;
      }  else {
        name[i] = '?';
      }
    }
    kprintf(CR_KERNLOG, "ACPI: Found %s at %lx\n", name, *table_index);
  }
  return true;
}

bool
initialise() {
  // fetch the EBDA.
  uint16_t* ebda_descriptor = (uint16_t *)(BDA + 0x40e);
  uintptr_t ebda_pa = (*ebda_descriptor) << 4;
  kprintf(CR_KERNLOG, "Reading ebda at %P\n", ebda_pa);

  rsdp = acpi_scan_region(ebda_pa, ebda_pa + 0x400);
  if (!rsdp) {
    kprintf(CR_KERNLOG, "Did not find an RSDT in EBDA, searching BIOS ROM.\n");
    rsdp = acpi_scan_region(0xe0000, 0xfffff);
  }
  if (!rsdp) {
    kprintf(CR_KERNLOG, "Could not find an RSDT, giving up.\n");
    return false;
  }
  if (rsdp->revision < 3) {
    kprintf(CR_KERNLOG, "Found an old RSDP. RSDT: %lx revision: %d\n",
            rsdp->rsdt, rsdp->revision);
    initialise_rsdt(rsdp->rsdt);
  } else {
    kprintf(CR_KERNLOG, "Found an RSDP. RSDT: %lx XSDT: %llx",
            rsdp->rsdt, rsdp->xsdt);
  }
  return true;
}

int
main() {
  initialise();
  return 0;
}
