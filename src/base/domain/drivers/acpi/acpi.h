/*
 * Copyright (C) 2007, The EROS Group, LLC.
 * Copyright (C) 2021, William ML Leslie
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Userspace ACPI support.
 */

typedef struct RSDP_v3 {
  char     signature[8];
  uint8_t  cksum;
  char     oemid[6];
  uint8_t  revision;
  uint32_t rsdt;		/* physical address of RSDT */
  uint32_t length;		/* of entire RSDT */

  /* Fields below appeared starting in v3.0 */
  uint64_t xsdt;		/* Physical address of XSDT */
  uint8_t  ex_cksum;		/* Extended checksum */
  char     reserved[3];
} RSDP_v3;

typedef struct SDT_hdr {
  char     signature[4];
  uint32_t length;
  uint8_t  revision;
  uint8_t  cksum;
  char     oemid[6];
  char     oemTableID[8];
  uint32_t oemRevision;
  uint32_t creatorID;
  uint32_t creatorRevision;
} SDT_hdr;

#define CR_RANGE	coyotos_driver_ACPI_APP_RANGE
#define CR_KERNLOG	coyotos_driver_ACPI_APP_KERNLOG
#define CR_PMAP		coyotos_driver_ACPI_APP_PMAP

void * pmap_ptov(uintptr_t pa);
