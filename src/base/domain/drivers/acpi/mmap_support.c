/*
 * Copyright (C) 2007, The EROS Group, LLC
 * Copyright (C) 2021, William ML Leslie
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/** @file
 *
 * @brief Support for custom address mappings.
 *
 * These routines make it possible to find ranges of memory to map one
 * or more pages into and do the relevant GPT manipulation to acheive
 * it.
 *
 * There are a few ways to enable this.  Small address space processes
 * have mutable access to their GPT and can simply insert whatever
 * pages they want.
 *
 * Larger programs normally use ElfSpace, and these could ask the
 * ElfSpace program to map pages for them by fishing out the handler
 * address and sending it messages.  This might require some
 * rearchitecting as the interface exported on that endpoint is only
 * the MemoryHandler interface at this point.
 *
 * For this process - a singleton that has no need for ElfSpace - we
 * instead do our own runtime setup, a`la the small runtime.
 *
 * Recall the large address mapping:
 *
 * 0x00000xxx : captemp
 * 0x07ffxxxx : stack
 * 0x08xxxxxx : elf image
 */

#include <stdint.h>

#include <coyotos/runtime.h>
#include <coyotos/kprintf.h>
#include <coyotos/captemp.h>

#include <idl/coyotos/AddressSpace.h>
#include <idl/coyotos/GPT.h>
#include <idl/coyotos/Page.h>
#include <idl/coyotos/KernLog.h>
#include <idl/coyotos/Null.h>
#include <idl/coyotos/SpaceBank.h>
#include <idl/coyotos/Process.h>

#include <mki/coyotos.driver.ACPI.h>

#include "acpi.h"


#define PMAP_START (0x10000000 + 0x20 * COYOTOS_PAGE_SIZE)
// 0x10000000
// +  0x20000
// 0x10020000

typedef struct mapping_t {
  uintptr_t pa;
  /* void *va; */
} mapping_t;

#define MAP_VA(map_index, offset) \
  (PMAP_START + (map_index) * COYOTOS_PAGE_SIZE + (offset))

// the mki builds a mapping that supports a MAPPING_COUNT of 0xffff
//
// if ACPI tables get bigger, we can comfortably increase this,
// although the linear scan over the table might not remain effective.
#define MAPPING_COUNT 0xe0

mapping_t mappings[MAPPING_COUNT];

size_t allocated_offset = 0;


bool
fetch_gpt(caploc_t gpt, uint8_t slot, coyotos_Memory_l2value_t l2v,
          caploc_t result, uint32_t guard_match) {
  //uint64_t address = slot << l2v;
  //kprintf(CR_KERNLOG, "Fetching GPT: %llx, %lx", address, guard_match << (l2v + 4));

  if (!coyotos_AddressSpace_getSlot(gpt, slot, result)) {
    kprintf(CR_KERNLOG, "Could not read from GPT slot\n");
    return false;
  }

  coyotos_Cap_AllegedType aty = 0;
  if (!coyotos_Cap_getType(result, &aty)) {
    kprintf(CR_KERNLOG, "Could not get type of object in GPT\n");
    return false;
  }

  if (aty == IKT_coyotos_GPT) {
    guard_t existing;
    if (!coyotos_Memory_getGuard(result, &existing)) {
      kprintf(CR_KERNLOG, "Found a GPT at %x %d, but no guard\n", slot, l2v);
    } else {
      //kprintf(CR_KERNLOG, "Found a GPT at %x %d (mask %llx match %llx)", slot, l2v,
      //        guard_mask(existing), guard_matchValue(existing));
    }
    return true;
  }

  if (aty != IKT_coyotos_Null) {
    kprintf(CR_KERNLOG, "Got unexpected type while looking through GPT: %d\n", aty);
    return false;
  }

  if (!coyotos_SpaceBank_alloc(CR_SPACEBANK,
                               coyotos_Range_obType_otGPT,
                               coyotos_Range_obType_otInvalid,
                               coyotos_Range_obType_otInvalid,
                               result,
                               CR_NULL,
                               CR_NULL)) {
    kprintf(CR_KERNLOG, "Could not allocate a GPT\n");
    return false;
  }

  coyotos_Memory_l2value_t unusedl2v = 0;
  if (!coyotos_GPT_setl2v(result, l2v, &unusedl2v)) {
    kprintf(CR_KERNLOG, "Could not set l2v value\n", l2v);
    (void) coyotos_SpaceBank_free(CR_SPACEBANK, 1, result, CR_NULL, CR_NULL);
    return false;
  }

  guard_t the_guard = make_guard(guard_match, l2v + 4);

  //if (!coyotos_AddressSpace_setSlot(gpt, slot, result)) {
  if (!coyotos_AddressSpace_guardedSetSlot(gpt, slot, result, the_guard)) {
    kprintf(CR_KERNLOG, "Could not install new GPT into slot: %d\n", slot);
    (void) coyotos_SpaceBank_free(CR_SPACEBANK, 1, result, CR_NULL, CR_NULL);
    return false;
  }

  // kprintf(CR_KERNLOG, "Allocated a GPT at %x %d (mask %llx match %llx)", slot, l2v,
  //         guard_mask(the_guard), guard_matchValue(the_guard));
  return true;
}

void *
pmap_pa(uintptr_t physical) {
  // initially we have 0x10000000 -> customGPT
  // 0x100x0000 -> gpt_bios
  // 0x1000x000 -> gpt_bios_low
  // 0x1001x000 -> gpt_bios_high

  if (allocated_offset > 0xffff) {
    kprintf(CR_KERNLOG, "Ran out of pmap space.\n");
    return NULL;
  }

  // kprintf(CR_KERNLOG, "Fetching physical page for %P", physical);

  caploc_t gpt = captemp_alloc();
  caploc_t gptx = captemp_alloc();

  int memory_offset = allocated_offset + 0x20;

  // 0x1abcd000
  // 0x10020000 0x10 0x100 0x1002 0x10020
  if (fetch_gpt(CR_PMAP, memory_offset >> 12, 20, gptx, memory_offset >> 16) &&
      fetch_gpt(gptx, (memory_offset >> 8) & 0xf, 16, gpt, (memory_offset >> 12) & 0xf) &&
      fetch_gpt(gpt, (memory_offset >> 4) & 0xf, 12, gptx, (memory_offset >> 8) & 0xf)) {

    // let's install the page into the selected GPT.
    uint64_t oid = coyotos_Range_devOidStart + physical / COYOTOS_PAGE_SIZE;
    caploc_t page = captemp_alloc();
    if (!coyotos_Range_getCap(CR_RANGE, oid, coyotos_Range_obType_otPage,
                              page)) {
      kprintf(CR_KERNLOG, "Could not load page at %P", physical);
      captemp_release(page);
      goto fail;
    }

    int our_slot = memory_offset & 0xf;
    // kprintf(CR_KERNLOG, "slot: %d", our_slot);
    //if (!coyotos_AddressSpace_setSlot(gptx, allocated_offset & 0xf, page)) {
    if (!coyotos_AddressSpace_guardedSetSlot(gptx, our_slot, page,
                                             make_guard(0, 12))) {
      kprintf(CR_KERNLOG, "Could not map page %P at %d", physical, our_slot);
      captemp_release(page);
      goto fail;
    }

    // done.
    captemp_release(page);
    captemp_release(gptx);
    captemp_release(gpt);
    mappings[allocated_offset].pa = physical & ~COYOTOS_PAGE_ADDR_MASK;
    // kprintf(CR_KERNLOG, "Mapped page %P at %P", physical & ~COYOTOS_PAGE_ADDR_MASK,
    //         ((allocated_offset << COYOTOS_PAGE_ADDR_BITS) + PMAP_START));

    return (void *) ((allocated_offset++ << COYOTOS_PAGE_ADDR_BITS) + PMAP_START);
  }

fail:
  captemp_release(gptx);
  captemp_release(gpt);
  return NULL;
}

void *
pmap_ptov(uintptr_t pa) {
  uintptr_t base = (pa & ~COYOTOS_PAGE_ADDR_MASK);
  for (uint64_t i = 0; i < allocated_offset; i++) {
    if (mappings[i].pa == base) {
      return (void *) MAP_VA((uintptr_t) i, pa & COYOTOS_PAGE_ADDR_MASK);
    }
  }

  void *mapped = pmap_pa(base);
  if (mapped) {
    return mapped + (pa & COYOTOS_PAGE_ADDR_MASK);
  }
  return NULL;
}

