/*
 * Copyright (C) 2008, The EROS Group, LLC
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

module coyotos.driver.TTY {
  import util = coyotos.Util;
  import rt = coyotos.RunTime;
  import ctor = coyotos.Constructor;
  import target = coyotos.TargetInfo;
  import Image = coyotos.Image;
  import helper = coyotos.driver.IrqHelper;

  // Following just so that we have a space bank:
  import sb = coyotos.SpaceBank;

  /* Our protected payload values */
  header enum PP {
    ReadWrite = 0,
    ReadOnly = 1,
    CallBack = 2
  };  
  
  header capreg APP {
    LOG = rt.REG.APP0,
    Helper,
    CallBack,
    wChannel,
    rChannel,
    TMP0
  };

  // For later.
  header enum TOOL {
    HelperCon = rt.TOOL.APP0,
    LOG
  };

  def bank = new Bank(PrimeBank);

  // Issue: it isn't immediately clear that this can be a small
  // process, so the following may need to be updated at some point:
  def image = 
    Image.load_small("TTY", bank, "coyotos/driver/TTY");

  def tools = ctor.fresh_tools(bank);
  tools[TOOL.HelperCon] = helper.constructor;
  tools[TOOL.LOG] = KernLog();	

  export def constructor = 
    ctor.make("TTY", bank, image, tools, NullCap());
}
