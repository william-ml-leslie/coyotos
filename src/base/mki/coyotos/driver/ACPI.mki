/*
 * Copyright (C) 2021, William ML Leslie
 *
 * This file is part of the Coyotos Operating System.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

module coyotos.driver.ACPI {
  import Util = coyotos.Util;
  import bp = coyotos.BootProcess;
  import rt = coyotos.RunTime;
  import TargetInfo = coyotos.TargetInfo;

  def bank = new Bank(PrimeBank);
  def image = loadimage(bank, "acpi");

  def make_gpt(bank, l2v, l2g, match) {
    def gpt = guard(new GPT(bank), match, l2g);
    gpt.l2v = l2v;
    return gpt;
  }

  // setting up a custom space.
  // 0x00000000 - 0x00000fff : captemp
  // 0x07ffe000 - 0x07fff000 : stack
  // 0x08000000 - ? : application
  // 0x10000000 - 0x20000000 : custom mapping space
  image.space = insert_subspace(bank, image.space, noexec(new CapPage(bank)), 0);
  image.space = insert_subspace(bank,
                                image.space,
                                noexec(new Page(bank)),
                                0x7ffe000);
                                //TargetInfo.large_stack_pointer - TargetInfo.page_size);

  def space = Util.make_gpt(bank, 28, 32);
  def customGPT = make_gpt(bank, 24, 28, 0x0);
  def customGPT1 = Util.make_gpt(bank, 20, 24);
  def gpt_bios = Util.make_gpt(bank, 16, 20);
  def gpt_bios_low = Util.make_gpt(bank, 12, 16);
  def gpt_bios_high = make_gpt(bank, 12, 16, 0x0);

  // map page 0 (the BIOS Data Area) at 0x10000000
  gpt_bios_low.cap[0] = readonly(PhysPageCap(0x0));

  // map 16 pages of high BIOS ROM from 0xe0000 at 0x10010000
  gpt_bios_high.cap[0] = readonly(PhysPageCap(0xe0));
  gpt_bios_high.cap[1] = readonly(guard(PhysPageCap(0xe1), 0, 12));
  gpt_bios_high.cap[2] = readonly(guard(PhysPageCap(0xe2), 0, 12));
  gpt_bios_high.cap[3] = readonly(guard(PhysPageCap(0xe3), 0, 12));
  gpt_bios_high.cap[4] = readonly(guard(PhysPageCap(0xe4), 0, 12));
  gpt_bios_high.cap[5] = readonly(guard(PhysPageCap(0xe5), 0, 12));
  gpt_bios_high.cap[6] = readonly(guard(PhysPageCap(0xe6), 0, 12));
  gpt_bios_high.cap[7] = readonly(guard(PhysPageCap(0xe7), 0, 12));
  gpt_bios_high.cap[8] = readonly(guard(PhysPageCap(0xe8), 0, 12));
  gpt_bios_high.cap[9] = readonly(guard(PhysPageCap(0xe9), 0, 12));
  gpt_bios_high.cap[10] = readonly(guard(PhysPageCap(0xea), 0, 12));
  gpt_bios_high.cap[11] = readonly(guard(PhysPageCap(0xeb), 0, 12));
  gpt_bios_high.cap[12] = readonly(guard(PhysPageCap(0xec), 0, 12));
  gpt_bios_high.cap[13] = readonly(guard(PhysPageCap(0xed), 0, 12));
  gpt_bios_high.cap[14] = readonly(guard(PhysPageCap(0xee), 0, 12));
  gpt_bios_high.cap[15] = readonly(guard(PhysPageCap(0xef), 0, 12));

  // space[1] customGPT[0] customGPT1[0] gpt_bios[1] gpt_bios_high[0]
  gpt_bios.cap[0] = gpt_bios_low; // 0x1000x000
  gpt_bios.cap[1] = gpt_bios_high; //0x1001x000

  // insert the bios mapping into the custom section
  customGPT1.cap[0] = gpt_bios;  // 0x100xxxxx
  customGPT.cap[0] = customGPT1; // 0x10xxxxxx

  // insert the custom section into the address space
  space.cap[0] = image.space; // 0x0xxxxxxx
  space.cap[1] = customGPT; //   0x1xxxxxxx

  image.space = space;

  def acpi = bp.make("ACPI", bank, image, NullCap(), NullCap());

  header capreg APP {
    RANGE = rt.REG.APP0,
    KERNLOG,
    PMAP,
    SPACEBANK
  };

  acpi.capReg[APP.RANGE] = Range();
  acpi.capReg[APP.KERNLOG] = KernLog();
  acpi.capReg[APP.PMAP] = customGPT;

  // TODO: export endpoints to comunicate with the ACPI server.
  //
  // At the moment, we treat the ACPI server as responsible for
  // starting the PCI driver, but we probably also want to support
  // copyout of some tables, as well as expose the features of the
  // FADT, such as power state management.
}
